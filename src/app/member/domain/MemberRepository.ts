import { Observable } from 'rxjs'
import { Member } from './entities/member'
import { MemberForModification } from './entities/memberForModification'

export interface MemberRepository {

    getMemberInformation(): Observable<Member>

    update(memberForModification: MemberForModification): Observable<void>

    unsubscribe(): Observable<void>

}
