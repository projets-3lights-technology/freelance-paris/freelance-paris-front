import { Member } from './member'

export class MemberBuilder {

    protected _id: string = ''
    protected _firstName: string = ''
    protected _lastName: string = ''
    protected _email: string = ''
    protected _avatar: string = ''

    withId(value: string): MemberBuilder {
        this._id = value
        return this
    }

    withFirstName(value: string): MemberBuilder {
        this._firstName = value
        return this
    }

    withLastName(value: string): MemberBuilder {
        this._lastName = value
        return this
    }

    withEmail(value: string): MemberBuilder {
        this._email = value
        return this
    }

    withAvatar(value: string): MemberBuilder {
        this._avatar = value
        return this
    }

    build(): Member {
        return {
            id       : this._id,
            firstName: this._firstName,
            lastName : this._lastName,
            email    : this._email,
            avatar   : this._avatar
        }
    }

}
