import { MemberForModification } from './memberForModification'

export class MemberForModificationBuilder {

    protected _id: string = ''
    protected _email: string = ''

    withId(value: string): MemberForModificationBuilder {
        this._id = value
        return this
    }

    withEmail(value: string): MemberForModificationBuilder {
        this._email = value
        return this
    }

    build(): MemberForModification {
        return new MemberForModification(
            this._id,
            this._email
        )
    }

}
