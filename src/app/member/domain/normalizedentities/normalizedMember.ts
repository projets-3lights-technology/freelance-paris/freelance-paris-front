import { normalize, schema } from 'normalizr'
import { Member } from '../entities/member'
import { MemberInformationStateById } from '../MemberState.redux'

export interface NormalizedMemberInformation {
    entities: {
        member: MemberInformationStateById
    },
    result: string
}

const memberInformationSchema = new schema.Entity('member')

export const normalizeMemberInformation = (data: Member): NormalizedMemberInformation =>
    normalize(data, memberInformationSchema)
