export interface MemberInformationById {
    id: string
    firstName: string
    lastName: string
    email: string
    avatar: string
}

export interface MemberInformationStateById {
    [id: string]: MemberInformationById
}

export interface MemberInformationState {
    byId: MemberInformationStateById
    allIds: string
    isLoading: boolean
    error: any
}

export interface MemberUnderModificationState {
    email: string
}

export interface MemberUnderModificationErrorState {
    email: string
}

export interface MemberModificationState {
    memberUnderModification: MemberUnderModificationState
    memberUnderModificationError: MemberUnderModificationErrorState
    isLoading: boolean
    error: any
}
