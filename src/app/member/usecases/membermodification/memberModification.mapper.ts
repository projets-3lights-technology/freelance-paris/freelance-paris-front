import { Member } from '../../domain/entities/member'
import { MemberBuilder } from '../../domain/entities/member.builder'
import { MemberForModification } from '../../domain/entities/memberForModification'
import { MemberForModificationBuilder } from '../../domain/entities/memberForModification.builder'
import { MemberInformationById, MemberUnderModificationState } from '../../domain/MemberState.redux'

export const MapToMemberForModification = (memberId: string) => (memberForModification: MemberUnderModificationState): MemberForModification =>
    new MemberForModificationBuilder()
        .withId(memberId)
        .withEmail(memberForModification.email)
        .build()

export const MapToMember = (memberForModification: MemberForModification, member: MemberInformationById): Member =>
    new MemberBuilder()
        .withId(member.id)
        .withFirstName(member.firstName)
        .withLastName(member.lastName)
        .withEmail(memberForModification.email)
        .withAvatar(member.avatar)
        .build()
