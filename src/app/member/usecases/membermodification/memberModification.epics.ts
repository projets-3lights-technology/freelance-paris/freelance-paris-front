import { ActionsObservable, combineEpics, ofType, StateObservable } from 'redux-observable'
import { concat, Observable, of } from 'rxjs'
import { catchError, map, switchMap } from 'rxjs/operators'
import { AppState } from '../../../../configuration/redux/rootState.redux'
import { MemberForModification } from '../../domain/entities/memberForModification'
import { MemberRepository } from '../../domain/MemberRepository'
import * as memberInformationActions from '../memberinformation/memberInformation.actions'
import * as memberModificationActions from './memberModification.actions'
import { MapToMember, MapToMemberForModification } from './memberModification.mapper'
import { MemberModificationValidator$ } from './memberModification.validator'

const updateMember = (memberForModification: MemberForModification, state$: StateObservable<AppState>) => concat(
    of(memberInformationActions.Actions.memberInformationFetched(MapToMember(
        memberForModification,
        state$.value.domain.memberInformation.byId[state$.value.domain.memberInformation.allIds]
    ))),
    of(memberModificationActions.Actions.memberUpdated())
)

const memberUpdator = (action$: ActionsObservable<memberModificationActions.Actions>,
                       state$: StateObservable<AppState>,
                       { memberRepository }: { memberRepository: MemberRepository })
    : Observable<any> =>
    action$.pipe(
        ofType<memberModificationActions.Actions,
            ReturnType<typeof memberModificationActions.Actions.updateMember>>(memberModificationActions.UPDATE_MEMBER),
        map(() => state$.value.domain.memberModification.memberUnderModification),
        switchMap(memberUnderModification => MemberModificationValidator$(memberUnderModification)
            .pipe(
                map(MapToMemberForModification(state$.value.domain.memberInformation.allIds)),
                switchMap(memberForModification => memberRepository.update(memberForModification)
                    .pipe(
                        switchMap(() => updateMember(memberForModification, state$)),
                        catchError(error => of(memberModificationActions.Actions.memberModificationFailed(error)))
                    )
                ),
                catchError(error => of(memberModificationActions.Actions.memberModificationValidationFailed(error)))
            )
        )
    )

export const memberModificationEpics = combineEpics<memberModificationActions.Actions, memberModificationActions.Actions, AppState>(
    memberUpdator
)
