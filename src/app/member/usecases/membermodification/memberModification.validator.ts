import { anyPass, evolve, isEmpty, not, pick, propSatisfies } from 'ramda'
import { Observable, of, pipe, throwError } from 'rxjs'
import { MemberUnderModificationErrorState, MemberUnderModificationState } from '../../domain/MemberState.redux'
import { MemberModificationFields } from './memberModification.actions'

const emailValidation = (field: string): string => {
    if (isEmpty(field))
        return 'required'
    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(field))
        return 'invalid'
    return ''
}
export const defenseAgainstBadInformation = pipe<MemberUnderModificationState, MemberUnderModificationErrorState, MemberUnderModificationErrorState>(
    pick(['email']),
    evolve({
        email: emailValidation
    })
)

const hasAnError = pipe(isEmpty, not)
const fieldIsBad = (field: MemberModificationFields) => propSatisfies(hasAnError, field)
export const hasOneBadField: (form: MemberUnderModificationErrorState) => boolean = anyPass([
    fieldIsBad('email')
])

export const MemberModificationValidator$ = (memberUnderModification: MemberUnderModificationState): Observable<MemberUnderModificationState> => {
    const errors: MemberUnderModificationErrorState = defenseAgainstBadInformation(memberUnderModification)

    return hasOneBadField(errors)
        ? throwError(errors)
        : of(memberUnderModification)
}
