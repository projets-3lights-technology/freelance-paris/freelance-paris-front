import { ActionsUnion } from '../../../../configuration/redux/helpers/actionsUnion.redux'
import { createAction } from '../../../../configuration/redux/helpers/createAction.redux'
import { MemberUnderModificationErrorState } from '../../domain/MemberState.redux'

export type MemberModificationFields = 'email'

/* commands */
export const CHANGE_MEMBER_INFORMATION = 'CHANGE_MEMBER_INFORMATION'
export const RESET_MEMBER_MODIFICATION = 'RESET_MEMBER_MODIFICATION'
export const UPDATE_MEMBER = 'UPDATE_MEMBER'

/* Events */
export const MEMBER_MODIFICATION_VALIDATION_FAILED = 'MEMBER_MODIFICATION_VALIDATION_FAILED'
export const MEMBER_UPDATED = 'MEMBER_UPDATED'
export const MEMBER_MODIFICATION_FAILED = 'MEMBER_MODIFICATION_FAILED'

export const Actions = {
    changeMemberInformation: (field: MemberModificationFields, value: string) => createAction(CHANGE_MEMBER_INFORMATION, {
        field,
        value
    }),
    resetMemberModification: () => createAction(RESET_MEMBER_MODIFICATION),
    updateMember           : () => createAction(UPDATE_MEMBER),

    memberModificationValidationFailed: (error: MemberUnderModificationErrorState) => createAction(MEMBER_MODIFICATION_VALIDATION_FAILED, error),
    memberUpdated                     : () => createAction(MEMBER_UPDATED),
    memberModificationFailed          : (error: any) => createAction(MEMBER_MODIFICATION_FAILED, error)
}

export type Actions = ActionsUnion<typeof Actions>
