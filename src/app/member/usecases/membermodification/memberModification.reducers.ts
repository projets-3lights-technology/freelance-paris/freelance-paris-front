import {
    MemberModificationState,
    MemberUnderModificationErrorState,
    MemberUnderModificationState
} from '../../domain/MemberState.redux'
import * as memberModificationActions from './memberModification.actions'
import { defenseAgainstBadInformation, hasOneBadField } from './memberModification.validator'

const initialMissionCreationState: MemberModificationState = {
    memberUnderModification     : {
        email: ''
    },
    memberUnderModificationError: {
        email: ''
    },
    isLoading                   : false,
    error                       : ''
}

const handleMemberModificationActions = (state: MemberModificationState = initialMissionCreationState,
                                         action: memberModificationActions.Actions): MemberModificationState => {
    switch (action.type) {
        case memberModificationActions.CHANGE_MEMBER_INFORMATION:
            const newMemberUnderModification = {
                ...state.memberUnderModification,
                [action.payload.field]: action.payload.value
            }
            return {
                ...state,
                memberUnderModification: newMemberUnderModification,
                memberUnderModificationError: defenseAgainstBadInformation(newMemberUnderModification)
            }

        case memberModificationActions.UPDATE_MEMBER:
            return {
                ...state,
                isLoading: true
            }

        case memberModificationActions.RESET_MEMBER_MODIFICATION:
        case memberModificationActions.MEMBER_UPDATED:
            return initialMissionCreationState

        case memberModificationActions.MEMBER_MODIFICATION_VALIDATION_FAILED:
            return {
                ...state,
                memberUnderModificationError: action.payload,
                isLoading                   : false
            }

        case memberModificationActions.MEMBER_MODIFICATION_FAILED:
            return {
                ...state,
                isLoading: false,
                error    : action.payload
            }

        default:
            return state
    }
}

export default handleMemberModificationActions

export const _getMemberModificationIsLoading = (state: MemberModificationState): boolean => state.isLoading
export const _getMemberUnderModification = (state: MemberModificationState): MemberUnderModificationState =>
    state.memberUnderModification
export const _getMemberUnderModificationError = (state: MemberModificationState): MemberUnderModificationErrorState =>
    state.memberUnderModificationError
export const _getMemberModificationHasError = (state: MemberModificationState): boolean =>
    hasOneBadField(state.memberUnderModificationError)
