import { defaultTo, path } from 'ramda'
import { combineReducers } from 'redux'
import { Optional } from 'typescript-optional'
import {
    MemberInformationById,
    MemberInformationState,
    MemberInformationStateById
} from '../../domain/MemberState.redux'
import { normalizeMemberInformation } from '../../domain/normalizedentities/normalizedMember'
import * as memberInformationActions from './memberInformation.actions'

const error = (state: any = '',
               action: memberInformationActions.Actions): any => {
    switch (action.type) {
        case memberInformationActions.MEMBER_INFORMATION_FETCHING_FAILED:
            return action.payload

        case memberInformationActions.FETCH_MEMBER_INFORMATION:
        case memberInformationActions.MEMBER_INFORMATION_FETCHED:
            return ''

        default:
            return state
    }
}

const isLoading = (state: boolean = false,
                   action: memberInformationActions.Actions): boolean => {
    switch (action.type) {
        case memberInformationActions.FETCH_MEMBER_INFORMATION:
            return true

        case memberInformationActions.MEMBER_INFORMATION_FETCHED:
        case memberInformationActions.MEMBER_INFORMATION_FETCHING_FAILED:
            return false

        default:
            return state
    }
}

const byId = (state: MemberInformationStateById = {},
              action: memberInformationActions.Actions): MemberInformationStateById => {
    switch (action.type) {
        case memberInformationActions.MEMBER_INFORMATION_FETCHED:
            const normalizedMemberInformation = normalizeMemberInformation(action.payload)
            return defaultTo({})(path(['entities', 'member'], normalizedMemberInformation))

        case memberInformationActions.FETCH_MEMBER_INFORMATION:
        case memberInformationActions.MEMBER_INFORMATION_FETCHING_FAILED:
            return {}

        default:
            return state
    }
}

const allIds = (state: string = '',
                action: memberInformationActions.Actions): string => {
    switch (action.type) {
        case memberInformationActions.MEMBER_INFORMATION_FETCHED:
            const normalizedMemberInformation = normalizeMemberInformation(action.payload)
            return normalizedMemberInformation.result

        case memberInformationActions.FETCH_MEMBER_INFORMATION:
        case memberInformationActions.MEMBER_INFORMATION_FETCHING_FAILED:
            return ''

        default:
            return state
    }
}

export default combineReducers<MemberInformationState>({
    byId,
    allIds,
    isLoading,
    error
})

export const _getMemberInformationIsLoading = (state: MemberInformationState): boolean => state.isLoading
export const _getMemberInformation = (state: MemberInformationState): Optional<MemberInformationById> => Optional.ofNullable(state.byId[state.allIds])
