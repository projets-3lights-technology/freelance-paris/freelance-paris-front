import { ActionsUnion } from '../../../../configuration/redux/helpers/actionsUnion.redux'
import { createAction } from '../../../../configuration/redux/helpers/createAction.redux'
import { Member } from '../../domain/entities/member'

/* Commands */
export const FETCH_MEMBER_INFORMATION = 'FETCH_MEMBER_INFORMATION'

/* Events */
export const MEMBER_INFORMATION_FETCHED = 'MEMBER_INFORMATION_FETCHED'
export const MEMBER_INFORMATION_FETCHING_FAILED = 'MEMBER_INFORMATION_FETCHING_FAILED'

export const Actions = {
    fetchMemberInformation: () => createAction(FETCH_MEMBER_INFORMATION),

    memberInformationFetched       : (member: Member) => createAction(MEMBER_INFORMATION_FETCHED, member),
    memberInformationFetchingFailed: (error: any) => createAction(MEMBER_INFORMATION_FETCHING_FAILED, error)
}

export type Actions = ActionsUnion<typeof Actions>
