import { ActionsObservable, combineEpics, ofType, StateObservable } from 'redux-observable'
import { Observable, of } from 'rxjs'
import { catchError, map, switchMap } from 'rxjs/operators'
import { AppState } from '../../../../configuration/redux/rootState.redux'
import { MemberRepository } from '../../domain/MemberRepository'
import * as memberInformationActions from './memberInformation.actions'

const memberInformationRetriever = (action$: ActionsObservable<memberInformationActions.Actions>,
                                    state$: StateObservable<AppState>,
                                    { memberRepository }: { memberRepository: MemberRepository })
    : Observable<memberInformationActions.Actions> =>
    action$.pipe(
        ofType<memberInformationActions.Actions,
            ReturnType<typeof memberInformationActions.Actions.fetchMemberInformation>>(memberInformationActions.FETCH_MEMBER_INFORMATION),
        switchMap(() => memberRepository.getMemberInformation()),
        map(memberInformation => memberInformationActions.Actions.memberInformationFetched(memberInformation)),
        catchError(error => of(memberInformationActions.Actions.memberInformationFetchingFailed(error)))
    )

export const memberInformationEpics = combineEpics<memberInformationActions.Actions, memberInformationActions.Actions, AppState>(
    memberInformationRetriever
)
