import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { AppConfiguration } from '../../../../../configuration/environment/AppConfiguration'
import { AuthorizedHttpClient } from '../../../../common/adapters/gateways/real/AuthorizedHttpClient'
import { Member } from '../../../domain/entities/member'
import { MemberForModification } from '../../../domain/entities/memberForModification'
import { MemberRepository } from '../../../domain/MemberRepository'
import { MemberMeDTO } from './DTO/MemberMeDTO'
import { MemberUpdateDTO } from './DTO/MemberUpdateDTO'
import { MapToMemberInformation } from './mappers/member.mapper'

export class RESTMemberRepository implements MemberRepository {

    private readonly memberEndpoint = this.appConfiguration.get('FREELANCE_API_URL') + 'me'

    constructor(private authorizedHttpClient: AuthorizedHttpClient,
                private appConfiguration: AppConfiguration) {
    }

    getMemberInformation(): Observable<Member> {
        return this.authorizedHttpClient
            .get<MemberMeDTO>(this.memberEndpoint)
            .pipe(
                map<MemberMeDTO, Member>(MapToMemberInformation)
            )
    }

    update(memberForModification: MemberForModification): Observable<void> {
        const body: MemberUpdateDTO = {
            value: memberForModification.email
        }
        return this.authorizedHttpClient
            .patch<MemberUpdateDTO>(this.memberEndpoint + '/mail', body)
    }

    unsubscribe(): Observable<void> {
        return this.authorizedHttpClient
            .delete(this.memberEndpoint)
    }

}
