import { Member } from '../../../../domain/entities/member'
import { MemberBuilder } from '../../../../domain/entities/member.builder'
import { MemberMeDTO } from '../DTO/MemberMeDTO'

export const MapToMemberInformation = (member: MemberMeDTO): Member =>
    new MemberBuilder()
        .withId(member.id)
        .withFirstName(member.first_name)
        .withLastName(member.last_name)
        .withEmail(member.email)
        .withAvatar(member.avatar)
        .build()
