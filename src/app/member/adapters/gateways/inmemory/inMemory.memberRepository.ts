import { Observable, of, Subject } from 'rxjs'
import { Member } from '../../../domain/entities/member'
import { MemberBuilder } from '../../../domain/entities/member.builder'
import { MemberForModification } from '../../../domain/entities/memberForModification'
import { MemberRepository } from '../../../domain/MemberRepository'

export class InMemoryMemberRepository implements MemberRepository {

    private _memberInformation$: Subject<Member> = new Subject()
    private _memberModification$: Subject<void> = new Subject()
    private _unsubscribeMember$: Subject<void> = new Subject()
    private memberForModification: MemberForModification = null
    private unsubscribeMember: boolean = false

    constructor(private member: Member = null) {
    }

    getMemberInformation(): Observable<Member> {
        if (this.member)
            return of(this.member)
        return this._memberInformation$
    }

    update(memberForModification: MemberForModification): Observable<void> {
        this.memberForModification = memberForModification

        if (!this.member)
            return this._memberModification$

        const newMember: Member = new MemberBuilder()
            .withId(this.member.id)
            .withFirstName(this.member.firstName)
            .withLastName(this.member.lastName)
            .withEmail(memberForModification.email)
            .withAvatar(this.member.avatar)
            .build()
        this._memberInformation$.next(newMember)

        return of(undefined)
    }

    unsubscribe(): Observable<void> {
        this.unsubscribeMember = true
        return of(undefined)
    }

    get updateWith(): MemberForModification {
        return this.memberForModification
    }

    get isUnsubscribe(): boolean {
        return this.unsubscribeMember
    }

    populateMemberInformation = (member: Member) => this._memberInformation$.next(member)
    triggerMemberModification = (): void => this._memberModification$.next()
    triggerUnsubscribeMember = (): void => this._unsubscribeMember$.next()

    memberInformationOnError = (error: string) => this._memberInformation$.error(error)
    updateMemberOnError = (error: any): void => this._memberModification$.error(error)
}
