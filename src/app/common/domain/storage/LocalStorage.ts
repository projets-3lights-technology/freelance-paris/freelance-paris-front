import { Observable } from 'rxjs'
import { Token } from '../entities/token'

export interface LocalStorage {

    saveToken(token: Token): Observable<void>

    getToken(): Observable<Token>

    removeToken(): Observable<void>

}
