import { addSeconds, format, isBefore, subHours } from 'date-fns/fp'
import { compose } from 'ramda'

export class Token {

    static readonly GUEST = new Token('guest', 0)

    constructor(private _accessToken: string,
                private expiresIn: number,
                private _expiresAt: string = '') {
    }

    static create(accessToken: string,
                  _expiresAt: string): Token {
        return new Token(accessToken, 0, _expiresAt)
    }

    get accessToken(): string {
        return this._accessToken
    }

    get expiresAt(): string {
        const expiresAt = compose<Date, Date, Date, string>(
            format('yyyy-MM-dd\'T\'HH:mm:ss.SSSxxx'),
            addSeconds(this.expiresIn),
            subHours(1)
        )

        return this._expiresAt === ''
            ? expiresAt(new Date())
            : this._expiresAt
    }

    get isExpired(): boolean {
        return isBefore(new Date())(new Date(this.expiresAt))
    }

}
