import { I18nKeys } from './translations/i18.keys'

export type TranslationState = {
    [key in I18nKeys]: string
}

export interface TranslationsState {
    [key: string]: TranslationState
}

export interface InternationalizationState {
    lang: string
    translations: TranslationsState
}
