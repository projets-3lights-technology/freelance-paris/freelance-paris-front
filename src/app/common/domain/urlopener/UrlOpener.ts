import { Observable } from 'rxjs'

export interface UrlOpener {

    openUrl(url: string): Observable<void>

}
