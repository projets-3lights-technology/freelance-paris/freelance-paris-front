export interface MissionListingState {
    id: string
    isMissionActionsOpen: boolean
}

export interface MissionDetailsState {
    id: string
    isMissionContactOpen: boolean
}

export interface UserNavigationState {
    currentPage: string
    missionListing: MissionListingState
    missionDetails: MissionDetailsState
    isInformationTemplateOpen: boolean
}
