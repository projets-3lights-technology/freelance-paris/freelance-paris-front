export interface AnalyticsOption {
    [key: string]: string
}

export interface AnalyticsLogger {

    logScreen(name: string): void

    logEvent(name: string, options?: AnalyticsOption): void

}
