export enum AppScreenName {
    LOGIN = 'login',
    SIGN_UP = 'sign_up',
    MAIN_PAGE = 'main_page',
    MISSIONS = 'missions',
    MISSION_DETAILS = 'mission_details',
    MISSION_DETAILS_CONTACT = 'mission_details_contact',
    MISSION_CREATION = 'mission_creation',
    MEMBER_PROFILE = 'member_profile'
}
