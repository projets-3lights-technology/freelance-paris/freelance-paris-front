export interface GatekeeperState {
    token: {
        access_token: string
        expires_at: string
    }
}
