import { isAfter } from 'date-fns/fp'
import { complement, isNil } from 'ramda'
import { Token } from '../../domain/entities/token'
import { GatekeeperState } from '../../domain/GatekeeperState.redux'
import * as gatekeeperActions from './gatekeeper.actions'

const initialGatekeeperState: GatekeeperState = {
    token: {
        access_token: '',
        expires_at  : ''
    }
}

const handleGatekeeperActions = (state: GatekeeperState = initialGatekeeperState,
                                 action: gatekeeperActions.Actions): GatekeeperState => {
    switch (action.type) {
        case gatekeeperActions.TOKEN_SAVED:
        case gatekeeperActions.TOKEN_LOADED:
            return complement(isNil)(action.payload)
                ? {
                    ...state,
                    token: {
                        access_token: action.payload.accessToken,
                        expires_at  : action.payload.expiresAt
                    }
                }
                : state

        default:
            return state
    }
}

export default handleGatekeeperActions

export const _canAutoConnect = (state: GatekeeperState): boolean => isAfter(new Date())(new Date(state.token.expires_at))
export const _isAuthenticated = (state: GatekeeperState): boolean => state.token.access_token !== Token.GUEST.accessToken
