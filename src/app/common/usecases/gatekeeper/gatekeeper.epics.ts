import { ActionsObservable, combineEpics, ofType, StateObservable } from 'redux-observable'
import { concat, Observable, of } from 'rxjs'
import { map, switchMap } from 'rxjs/operators'
import { AppState } from '../../../../configuration/redux/rootState.redux'
import { MemberRepository } from '../../../member/domain/MemberRepository'
import * as memberInformationActions from '../../../member/usecases/memberinformation/memberInformation.actions'
import { Token } from '../../domain/entities/token'
import { LocalStorage } from '../../domain/storage/LocalStorage'
import * as navigationActions from '../navigation/navigation.actions'
import * as gatekeeperActions from './gatekeeper.actions'

const logMember = (token: Token) => concat(
    of(gatekeeperActions.Actions.tokenSaved(token)),
    of(memberInformationActions.Actions.fetchMemberInformation()),
    of(navigationActions.Actions.navigateToMainPage())
)

const autoLogMember = (token: Token) => token.isExpired
    ? concat(
        of(gatekeeperActions.Actions.tokenSaved(token)),
        of(navigationActions.Actions.startApplication())
    )
    : logMember(token)

const logInMember = (action$: ActionsObservable<gatekeeperActions.Actions>,
                     state$: StateObservable<AppState>,
                     {
                         memberRepository,
                         localStorage
                     }: {
                         memberRepository: MemberRepository,
                         localStorage: LocalStorage
                     })
    : Observable<gatekeeperActions.Actions | memberInformationActions.Actions | navigationActions.Actions> =>
    action$.pipe(
        ofType<gatekeeperActions.Actions,
            ReturnType<typeof gatekeeperActions.Actions.logIn>>(gatekeeperActions.LOGIN_MEMBER),
        switchMap(action => localStorage.saveToken(action.payload)
            .pipe(
                switchMap(() => logMember(action.payload))
            )
        )
    )

const logInGuest = (action$: ActionsObservable<gatekeeperActions.Actions>,
                    state$: StateObservable<AppState>,
                    {
                        memberRepository,
                        localStorage
                    }: {
                        memberRepository: MemberRepository,
                        localStorage: LocalStorage
                    })
    : Observable<gatekeeperActions.Actions | memberInformationActions.Actions | navigationActions.Actions> =>
    action$.pipe(
        ofType<gatekeeperActions.Actions,
            ReturnType<typeof gatekeeperActions.Actions.logInGuest>>(gatekeeperActions.LOGIN_GUEST),
        switchMap(() => localStorage.saveToken(Token.GUEST)
            .pipe(
                switchMap(() => logMember(Token.GUEST))
            )
        )
    )

const autoLogInMember = (action$: ActionsObservable<gatekeeperActions.Actions>,
                         state$: StateObservable<AppState>,
                         {
                             memberRepository,
                             localStorage
                         }: {
                             memberRepository: MemberRepository,
                             localStorage: LocalStorage
                         })
    : Observable<gatekeeperActions.Actions | memberInformationActions.Actions | navigationActions.Actions> =>
    action$.pipe(
        ofType<gatekeeperActions.Actions,
            ReturnType<typeof gatekeeperActions.Actions.autoLogin>>(gatekeeperActions.AUTO_LOGIN_MEMBER),
        switchMap(() => localStorage.getToken()),
        switchMap(token => autoLogMember(token))
    )

const logOutMember = (action$: ActionsObservable<gatekeeperActions.Actions>,
                      state$: StateObservable<AppState>,
                      { localStorage }: { localStorage: LocalStorage })
    : Observable<navigationActions.Actions> =>
    action$.pipe(
        ofType<gatekeeperActions.Actions,
            ReturnType<typeof gatekeeperActions.Actions.logOut>>(gatekeeperActions.LOGOUT_MEMBER),
        switchMap(() => localStorage.removeToken()),
        map(() => navigationActions.Actions.quitApplication())
    )

const unsubscribeMember = (action$: ActionsObservable<gatekeeperActions.Actions>,
                           state$: StateObservable<AppState>,
                           {
                               memberRepository,
                               localStorage
                           }: {
                               memberRepository: MemberRepository,
                               localStorage: LocalStorage
                           })
    : Observable<navigationActions.Actions> =>
    action$.pipe(
        ofType<gatekeeperActions.Actions,
            ReturnType<typeof gatekeeperActions.Actions.unsubscribe>>(gatekeeperActions.UNSUBSCRIBE_MEMBER),
        switchMap(() => memberRepository.unsubscribe()),
        switchMap(() => localStorage.removeToken()),
        map(() => navigationActions.Actions.quitApplication())
    )

export const gatekeeperEpics = combineEpics<gatekeeperActions.Actions, any, AppState>(
    logInMember,
    logInGuest,
    autoLogInMember,
    logOutMember,
    unsubscribeMember
)
