import { ActionsUnion } from '../../../../configuration/redux/helpers/actionsUnion.redux'
import { createAction } from '../../../../configuration/redux/helpers/createAction.redux'
import { Token } from '../../domain/entities/token'

/* commands */
export const LOGIN_MEMBER = 'LOGIN_MEMBER'
export const LOGIN_GUEST = 'LOGIN_GUEST'
export const AUTO_LOGIN_MEMBER = 'AUTO_LOGIN_MEMBER'
export const LOGOUT_MEMBER = 'LOGOUT_MEMBER'
export const UNSUBSCRIBE_MEMBER = 'UNSUBSCRIBE_MEMBER'

/* Events */
export const TOKEN_SAVED = 'TOKEN_SAVED'
export const TOKEN_LOADED = 'TOKEN_LOADED'

export const Actions = {
    logIn      : (token: Token) => createAction(LOGIN_MEMBER, token),
    logInGuest : () => createAction(LOGIN_GUEST),
    autoLogin  : () => createAction(AUTO_LOGIN_MEMBER),
    logOut     : () => createAction(LOGOUT_MEMBER),
    unsubscribe: () => createAction(UNSUBSCRIBE_MEMBER),

    tokenSaved : (token: Token) => createAction(TOKEN_SAVED, token),
    tokenLoaded: (token: Token) => createAction(TOKEN_LOADED, token)
}

export type Actions = ActionsUnion<typeof Actions>
