import { AppPageName } from '../../../../../configuration/withNavigation/appPageName.navigation'
import { UserNavigationState } from '../../../domain/UserNavigationState.redux'
import * as navigationActions from '../navigation.actions'

export const handleSideMenuAreaNavigation = (state: UserNavigationState,
                                             action: navigationActions.Actions): UserNavigationState => {
    switch (action.name) {
        case navigationActions.OPEN_SIDE_MENU:
        case navigationActions.QUIT_SIDE_MENU:
        default:
            return state

        case navigationActions.OPEN_MISSION_CREATION:
            return {
                ...state,
                currentPage: AppPageName.MISSION_CREATION
            }

        case navigationActions.OPEN_MEMBER_PROFILE:
            return {
                ...state,
                currentPage: AppPageName.MEMBER_PROFILE
            }

        case navigationActions.OPEN_INFORMATION_TEMPLATE:
            return {
                ...state,
                isInformationTemplateOpen: true
            }

        case navigationActions.QUIT_INFORMATION_TEMPLATE:
            return {
                ...state,
                isInformationTemplateOpen: false
            }

        case navigationActions.QUIT_MISSION_CREATION:
        case navigationActions.QUIT_MEMBER_PROFILE:
            return {
                ...state,
                currentPage: AppPageName.MISSIONS
            }

        case navigationActions.QUIT_APPLICATION:
            return {
                ...state,
                currentPage: AppPageName.LOGIN
            }
    }
}
