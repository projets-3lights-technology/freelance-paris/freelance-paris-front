import { anyPass, propEq } from 'ramda'
import { AppPageName } from '../../../../configuration/withNavigation/appPageName.navigation'
import { UserNavigationState } from '../../domain/UserNavigationState.redux'
import { handleDisconnectedAreaNavigation } from './disconnectedarea/disconnectedArea.reducers'
import { handleMissionsAreaNavigation } from './missionsarea/missionsArea.reducers'
import * as navigationActions from './navigation.actions'
import { handleSideMenuAreaNavigation } from './sidemenuarea/sideMenuArea.reducers'

export const initialNavigationState: UserNavigationState = {
    currentPage              : AppPageName.LOGIN,
    missionListing           : {
        id                  : '',
        isMissionActionsOpen: false
    },
    missionDetails           : {
        id                  : '',
        isMissionContactOpen: false
    },
    isInformationTemplateOpen: false
}

const handleNavigationActions = (state: UserNavigationState = initialNavigationState,
                                 action: navigationActions.Actions): UserNavigationState => {
    switch (action.name) {
        case navigationActions.START_APPLICATION:
        case navigationActions.NAVIGATE_TO_MAIN_PAGE:
            return handleDisconnectedAreaNavigation(state, action)

        case navigationActions.NAVIGATE_TO_MISSIONS:
        case navigationActions.OPEN_MISSION_ACTIONS:
        case navigationActions.QUIT_MISSION_ACTIONS:
        case navigationActions.OPEN_MISSION_DETAILS:
        case navigationActions.QUIT_MISSION_DETAILS:
        case navigationActions.OPEN_MISSION_DETAILS_CONTACT:
        case navigationActions.QUIT_MISSION_DETAILS_CONTACT:
            return handleMissionsAreaNavigation(state, action)

        case navigationActions.OPEN_SIDE_MENU:
        case navigationActions.QUIT_SIDE_MENU:
        case navigationActions.OPEN_MISSION_CREATION:
        case navigationActions.OPEN_INFORMATION_TEMPLATE:
        case navigationActions.QUIT_INFORMATION_TEMPLATE:
        case navigationActions.QUIT_MISSION_CREATION:
        case navigationActions.OPEN_MEMBER_PROFILE:
        case navigationActions.QUIT_MEMBER_PROFILE:
        case navigationActions.QUIT_APPLICATION:
            return handleSideMenuAreaNavigation(state, action)

        default:
            return state
    }
}

export default handleNavigationActions

export const _getCurrentPage = (state: UserNavigationState): string => state.currentPage
export const _getMissionDetailsId = (state: UserNavigationState): string => state.missionDetails.id
export const _isOnMissions = (state: UserNavigationState): boolean => anyPass([
    propEq('currentPage')(AppPageName.MISSIONS),
    propEq('currentPage')(AppPageName.MISSION_DETAILS)
])(state)

export const _isMissionActionsOpen = (id: string) => (state: UserNavigationState): boolean => state.missionListing.id === id
export const _isMissionContactOpen = (state: UserNavigationState): boolean => state.missionDetails.isMissionContactOpen
export const _isInformationTemplateOpen = (state: UserNavigationState): boolean => state.isInformationTemplateOpen
