import { path } from 'ramda'
import { AppPageName } from '../../../../../configuration/withNavigation/appPageName.navigation'
import {
    MissionDetailsState,
    MissionListingState,
    UserNavigationState
} from '../../../domain/UserNavigationState.redux'
import * as navigationActions from '../navigation.actions'

const handleCurrentPage = (state: string,
                           action: navigationActions.Actions): string => {
    switch (action.name) {
        case navigationActions.NAVIGATE_TO_MISSIONS:
        case navigationActions.QUIT_MISSION_DETAILS:
        default:
            return AppPageName.MISSIONS

        case navigationActions.OPEN_MISSION_DETAILS:
        case navigationActions.QUIT_MISSION_DETAILS_CONTACT:
            return AppPageName.MISSION_DETAILS

        case navigationActions.OPEN_MISSION_DETAILS_CONTACT:
            return AppPageName.MISSION_DETAILS
    }
}

const handleMissionActions = (state: MissionListingState,
                              action: navigationActions.Actions): MissionListingState => {
    switch (action.name) {
        case navigationActions.NAVIGATE_TO_MISSIONS:
        default:
            return state

        case navigationActions.OPEN_MISSION_ACTIONS:
            return {
                ...state,
                id                  : String(path(['params', 'id'])(action)),
                isMissionActionsOpen: true
            }

        case navigationActions.QUIT_MISSION_ACTIONS:
            return {
                ...state,
                id                  : '',
                isMissionActionsOpen: false
            }
    }
}

const handleMissionDetails = (state: MissionDetailsState,
                              action: navigationActions.Actions): MissionDetailsState => {
    switch (action.name) {
        case navigationActions.NAVIGATE_TO_MISSIONS:
        default:
            return state

        case navigationActions.OPEN_MISSION_DETAILS_CONTACT:
        case navigationActions.QUIT_MISSION_DETAILS_CONTACT:
            return {
                ...state,
                isMissionContactOpen: !state.isMissionContactOpen
            }

        case navigationActions.OPEN_MISSION_DETAILS:
            return {
                ...state,
                id: String(path(['params', 'id'])(action))
            }

        case navigationActions.QUIT_MISSION_DETAILS:
            return {
                ...state,
                id: ''
            }
    }
}

export const handleMissionsAreaNavigation = (state: UserNavigationState, action: navigationActions.Actions): UserNavigationState => ({
    ...state,
    currentPage   : handleCurrentPage(state.currentPage, action),
    missionListing: handleMissionActions(state.missionListing, action),
    missionDetails: handleMissionDetails(state.missionDetails, action)
})
