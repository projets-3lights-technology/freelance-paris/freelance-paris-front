import { ActionsUnion } from '../../../../configuration/redux/helpers/actionsUnion.redux'
import { createNavAction } from '../../../../configuration/redux/helpers/createAction.redux'
import { AppPageName } from '../../../../configuration/withNavigation/appPageName.navigation'

/* Commands */
export const NAVIGATE = 'Navigation/NAVIGATE'
export const BACK = 'Navigation/BACK'
export const OPEN_DRAWER = 'Navigation/OPEN_DRAWER'
export const CLOSE_DRAWER = 'Navigation/CLOSE_DRAWER'
export const OTHER = 'Navigation/OTHER'

export const START_APPLICATION = 'START_APPLICATION'
export const OPEN_SIGN_IN = 'OPEN_SIGN_IN'
export const NAVIGATE_TO_MAIN_PAGE = 'NAVIGATE_TO_MAIN_PAGE'

export const NAVIGATE_TO_MISSIONS = 'NAVIGATE_TO_MISSIONS'
export const OPEN_MISSION_ACTIONS = 'OPEN_MISSION_ACTIONS'
export const QUIT_MISSION_ACTIONS = 'QUIT_MISSION_ACTIONS'

export const OPEN_MISSION_DETAILS = 'OPEN_MISSION_DETAILS'
export const QUIT_MISSION_DETAILS = 'QUIT_MISSION_DETAILS'
export const OPEN_MISSION_DETAILS_CONTACT = 'OPEN_MISSION_DETAILS_CONTACT'
export const QUIT_MISSION_DETAILS_CONTACT = 'QUIT_MISSION_DETAILS_CONTACT'

export const OPEN_SIDE_MENU = 'OPEN_SIDE_MENU'
export const QUIT_SIDE_MENU = 'QUIT_SIDE_MENU'
export const OPEN_MISSION_CREATION = 'OPEN_MISSION_CREATION'
export const OPEN_INFORMATION_TEMPLATE = 'OPEN_INFORMATION_TEMPLATE'
export const QUIT_INFORMATION_TEMPLATE = 'QUIT_INFORMATION_TEMPLATE'
export const QUIT_MISSION_CREATION = 'QUIT_MISSION_CREATION'
export const OPEN_MEMBER_PROFILE = 'OPEN_MEMBER_PROFILE'
export const QUIT_MEMBER_PROFILE = 'QUIT_MEMBER_PROFILE'
export const CONTACT_US = 'CONTACT_US'
export const CONTACT_US_OPENED = 'CONTACT_US_OPENED'

export const QUIT_APPLICATION = 'QUIT_APPLICATION'

export const Actions = {
    startApplication  : () => createNavAction(OTHER, START_APPLICATION),
    openSignIn        : () => createNavAction(OTHER, OPEN_SIGN_IN),
    navigateToMainPage: () => createNavAction(NAVIGATE, NAVIGATE_TO_MAIN_PAGE, AppPageName.MAIN_PAGE),

    navigateToMissions: () => createNavAction(NAVIGATE, NAVIGATE_TO_MISSIONS, AppPageName.MISSIONS),
    openMissionActions: (id: string) => createNavAction(OTHER, OPEN_MISSION_ACTIONS, AppPageName.MISSIONS, { id }),
    quitMissionActions: () => createNavAction(OTHER, QUIT_MISSION_ACTIONS),
    openMissionDetails: (id: string) => createNavAction(NAVIGATE, OPEN_MISSION_DETAILS, AppPageName.MISSION_DETAILS, { id }),
    quitMissionDetails: () => createNavAction(BACK, QUIT_MISSION_DETAILS),
    openMissionContact: () => createNavAction(OTHER, OPEN_MISSION_DETAILS_CONTACT),
    quitMissionContact: () => createNavAction(OTHER, QUIT_MISSION_DETAILS_CONTACT),

    openSideMenu           : () => createNavAction(OPEN_DRAWER, OPEN_SIDE_MENU, AppPageName.SIDE_MENU),
    quitSideMenu           : () => createNavAction(CLOSE_DRAWER, QUIT_SIDE_MENU),
    openMissionCreation    : () => createNavAction(NAVIGATE, OPEN_MISSION_CREATION, AppPageName.MISSION_CREATION),
    openInformationTemplate: () => createNavAction(OTHER, OPEN_INFORMATION_TEMPLATE),
    quitInformationTemplate: () => createNavAction(OTHER, QUIT_INFORMATION_TEMPLATE),
    quitMissionCreation    : () => createNavAction(BACK, QUIT_MISSION_CREATION),
    openMemberProfile      : () => createNavAction(NAVIGATE, OPEN_MEMBER_PROFILE, AppPageName.MEMBER_PROFILE),
    quitMemberProfile      : () => createNavAction(BACK, QUIT_MEMBER_PROFILE),
    contactUs              : () => createNavAction(CONTACT_US, CONTACT_US),
    contactUsOpened        : () => createNavAction(CONTACT_US_OPENED, CONTACT_US_OPENED),

    quitApplication: () => createNavAction(NAVIGATE, QUIT_APPLICATION, AppPageName.LOGIN)
}

export type Actions = ActionsUnion<typeof Actions>
