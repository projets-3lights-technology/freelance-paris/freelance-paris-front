import { ActionsObservable, combineEpics, ofType, StateObservable } from 'redux-observable'
import { Observable } from 'rxjs'
import { map, switchMap } from 'rxjs/operators'
import { AppConfiguration } from '../../../../configuration/environment/AppConfiguration'
import { AppState } from '../../../../configuration/redux/rootState.redux'
import { UrlOpener } from '../../domain/urlopener/UrlOpener'
import * as navigationActions from './navigation.actions'

const urlOpenerHandler = (action$: ActionsObservable<navigationActions.Actions>,
                          state$: StateObservable<AppState>,
                          { urlOpener, configuration }: { urlOpener: UrlOpener, configuration: AppConfiguration })
    : Observable<navigationActions.Actions> =>
    action$.pipe(
        ofType<navigationActions.Actions,
            ReturnType<typeof navigationActions.Actions.contactUs>>(navigationActions.CONTACT_US),
        switchMap(() => urlOpener.openUrl('mailto:' + configuration.get('MAIL_CONTACT'))),
        map(() => navigationActions.Actions.contactUsOpened())
    )

export const navigationEpics = combineEpics<navigationActions.Actions, any, AppState>(
    urlOpenerHandler
)
