import { AppPageName } from '../../../../../configuration/withNavigation/appPageName.navigation'
import { UserNavigationState } from '../../../domain/UserNavigationState.redux'
import * as navigationActions from '../navigation.actions'

export const handleDisconnectedAreaNavigation = (state: UserNavigationState,
                                                 action: navigationActions.Actions): UserNavigationState => {
    switch (action.name) {
        case navigationActions.START_APPLICATION:
            return {
                ...state,
                currentPage: AppPageName.LOGIN
            }

        case navigationActions.NAVIGATE_TO_MAIN_PAGE:
            return {
                ...state,
                currentPage: AppPageName.MISSIONS
            }

        default:
            return state
    }
}
