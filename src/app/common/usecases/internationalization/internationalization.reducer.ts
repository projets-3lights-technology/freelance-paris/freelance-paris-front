import { createSelector } from 'reselect'
import { InternationalizationState, TranslationsState } from '../../domain/InternationalizationState.redux'
import { I18nKeys } from '../../domain/translations/i18.keys'
import { i18nEn } from '../../domain/translations/i18n.en'
import { i18nFr } from '../../domain/translations/i18n.fr'
import * as i18nActions from './internationalization.actions'

const initialInternationalizationState: InternationalizationState = {
    lang        : 'fr',
    translations: {
        fr: i18nFr,
        en: i18nEn
    }
}

const handleInternationalizationActions = (state: InternationalizationState = initialInternationalizationState,
                                           action: i18nActions.Actions): InternationalizationState => {
    switch (action.type) {
        case i18nActions.SWITCH_LANG:
            return {
                ...state,
                lang: action.payload
            }

        default:
            return state
    }
}

export default handleInternationalizationActions

export const _t = (key: I18nKeys) =>
    createSelector<InternationalizationState, string, TranslationsState, string>(
        state => state.lang,
        state => state.translations,
        (lang, translations) => translations[lang][key]
    )

export const _capitalize = (key: I18nKeys) =>
    createSelector<InternationalizationState, string, TranslationsState, string>(
        state => state.lang,
        state => state.translations,
        (lang, translations) => translations[lang][key].charAt(0).toUpperCase() + translations[lang][key].slice(1)
    )
