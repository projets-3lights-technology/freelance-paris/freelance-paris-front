import { ActionsUnion } from '../../../../configuration/redux/helpers/actionsUnion.redux'
import { createAction } from '../../../../configuration/redux/helpers/createAction.redux'

/* commands */
export const SWITCH_LANG = 'SWITCH_LANG'

export const Actions = {
    switchLang: (lang: string) => createAction(SWITCH_LANG, lang)
}

export type Actions = ActionsUnion<typeof Actions>
