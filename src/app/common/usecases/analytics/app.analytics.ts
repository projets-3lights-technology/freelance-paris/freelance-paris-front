import { path } from 'ramda'
import { AppState } from '../../../../configuration/redux/rootState.redux'
import * as memberModificationActions from '../../../member/usecases/membermodification/memberModification.actions'
import * as missionContactActions from '../../../missions/usecases/missioncontact/missionContact.actions'
import * as missionCreationActions from '../../../missions/usecases/missioncreation/missionCreation.actions'
import * as missionsListingActions from '../../../missions/usecases/missionslisting/missionsListing.actions'
import { AnalyticsLogger, AnalyticsOption } from '../../domain/analyticslogger/AnalyticsLogger'
import { AppEventName } from '../../domain/analyticslogger/appEventName.analytics'
import { AppScreenName } from '../../domain/analyticslogger/appScreenName.analytics'
import * as navigationActions from '../navigation/navigation.actions'

const logScreen = (name: AppScreenName) => (analyticsLogger: AnalyticsLogger) =>
    analyticsLogger.logScreen(name)
const logEvent = (name: AppEventName, options?: AnalyticsOption) => (analyticsLogger: AnalyticsLogger) =>
    analyticsLogger.logEvent(name, options)

const missionsListingAnalytics = (action: missionsListingActions.Actions,
                                  state: AppState,
                                  { analyticsLogger }: { analyticsLogger: AnalyticsLogger }): void => {
    switch (action.type) {
        case missionsListingActions.REMOVE_MISSION:
            logEvent(AppEventName.REMOVE_MISSION, { mission_id: action.payload })(analyticsLogger)
            break
    }
}

const missionContactAnalytics = (action: missionContactActions.Actions,
                                 state: AppState,
                                 { analyticsLogger }: { analyticsLogger: AnalyticsLogger }): void => {
    switch (action.type) {
        case missionContactActions.SEND_MISSION_CONTACT:
            logEvent(AppEventName.SEND_MISSION_CONTACT, { mission_id: action.payload.missionId })(analyticsLogger)
            break
    }
}

const missionCreationAnalytics = (action: missionCreationActions.Actions,
                                  state: AppState,
                                  { analyticsLogger }: { analyticsLogger: AnalyticsLogger }): void => {
    switch (action.type) {
        case missionCreationActions.PUBLISH_MISSION:
            logEvent(AppEventName.PUBLISH_MISSION)(analyticsLogger)
            break
    }
}

const memberModificationAnalytics = (action: memberModificationActions.Actions,
                                     state: AppState,
                                     { analyticsLogger }: { analyticsLogger: AnalyticsLogger }): void => {
    switch (action.type) {
        case memberModificationActions.UPDATE_MEMBER:
            logEvent(AppEventName.UPDATE_MEMBER_INFORMATION)(analyticsLogger)
            break
    }
}

const navAnalytics = (action: navigationActions.Actions,
                      state: AppState,
                      { analyticsLogger }: { analyticsLogger: AnalyticsLogger }): void => {
    switch (action.type) {
        case navigationActions.OTHER:
            switch (action.name) {
                case navigationActions.START_APPLICATION:
                    logScreen(AppScreenName.LOGIN)(analyticsLogger)
                    break

                case navigationActions.OPEN_SIGN_IN:
                    logEvent(AppEventName.OPEN_SIGN_IN_MODAL)(analyticsLogger)
                    break

                case navigationActions.OPEN_MISSION_DETAILS_CONTACT:
                    logScreen(AppScreenName.MISSION_DETAILS_CONTACT)(analyticsLogger)
                    logEvent(AppEventName.OPEN_MISSION_DETAILS_CONTACT)(analyticsLogger)
                    break

                case navigationActions.QUIT_MISSION_DETAILS_CONTACT:
                    logEvent(AppEventName.CLOSE_MISSION_DETAILS_CONTACT)(analyticsLogger)
                    break

                case navigationActions.OPEN_INFORMATION_TEMPLATE:
                    logEvent(AppEventName.OPEN_INFORMATION_TEMPLATE)(analyticsLogger)
                    break

                case navigationActions.QUIT_INFORMATION_TEMPLATE:
                    logEvent(AppEventName.CLOSE_INFORMATION_TEMPLATE)(analyticsLogger)
                    break
            }
            break

        case navigationActions.NAVIGATE:
            switch (action.name) {
                case navigationActions.NAVIGATE_TO_MAIN_PAGE:
                    logScreen(AppScreenName.MAIN_PAGE)(analyticsLogger)
                    logEvent(AppEventName.CONNECT_MEMBER)(analyticsLogger)
                    break

                case navigationActions.NAVIGATE_TO_MISSIONS:
                    logScreen(AppScreenName.MISSIONS)(analyticsLogger)
                    break

                case navigationActions.OPEN_MISSION_DETAILS:
                    logScreen(AppScreenName.MISSION_DETAILS)(analyticsLogger)
                    logEvent(
                        AppEventName.OPEN_MISSION_DETAILS,
                        { mission_id: String(path(['params', 'id'])(action)) }
                    )(analyticsLogger)
                    break

                case navigationActions.OPEN_MISSION_CREATION:
                    logScreen(AppScreenName.MISSION_CREATION)(analyticsLogger)
                    logEvent(AppEventName.OPEN_MISSION_CREATION)(analyticsLogger)
                    break

                case navigationActions.OPEN_MEMBER_PROFILE:
                    logScreen(AppScreenName.MEMBER_PROFILE)(analyticsLogger)
                    logEvent(AppEventName.OPEN_MEMBER_PROFILE)(analyticsLogger)
                    break

                case navigationActions.QUIT_APPLICATION:
                    logEvent(AppEventName.LOG_OUT)(analyticsLogger)
                    break
            }
            break

        case navigationActions.BACK:
            switch (action.name) {
                case navigationActions.QUIT_MISSION_DETAILS:
                    logEvent(AppEventName.CLOSE_MISSION_DETAILS)(analyticsLogger)
                    break

                case navigationActions.QUIT_MISSION_CREATION:
                    logEvent(AppEventName.CLOSE_MISSION_CREATION)(analyticsLogger)
                    break

                case navigationActions.QUIT_MEMBER_PROFILE:
                    logEvent(AppEventName.CLOSE_MEMBER_PROFILE)(analyticsLogger)
                    break
            }
            break

        case navigationActions.OPEN_DRAWER:
            switch (action.name) {
                case navigationActions.OPEN_SIDE_MENU:
                    logEvent(AppEventName.OPEN_SIDE_MENU)(analyticsLogger)
                    break
            }
            break

        case navigationActions.CLOSE_DRAWER:
            switch (action.name) {
                case navigationActions.QUIT_SIDE_MENU:
                    logEvent(AppEventName.CLOSE_SIDE_MENU)(analyticsLogger)
                    break
            }
            break

        case navigationActions.CONTACT_US:
            logEvent(AppEventName.CONTACT_US)(analyticsLogger)
            break
    }
}

export const appAnalytics = [
    navAnalytics,
    missionsListingAnalytics,
    missionContactAnalytics,
    missionCreationAnalytics,
    memberModificationAnalytics
]
