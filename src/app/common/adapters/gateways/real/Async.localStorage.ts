import { compose, defaultTo } from 'ramda'
import { AsyncStorage } from 'react-native'
import { Observable, of } from 'rxjs'
import { fromPromise } from 'rxjs-compat/observable/fromPromise'
import { map } from 'rxjs/operators'
import { Token } from '../../../domain/entities/token'
import { LocalStorage } from '../../../domain/storage/LocalStorage'
import { StoredTokenDTO } from './DTO/StoredTokenDTO'

export class AsyncLocalStorage implements LocalStorage {

    private readonly accessTokenKey = 'access_token'

    saveToken(token: Token): Observable<void> {
        AsyncStorage.setItem(this.accessTokenKey, JSON.stringify({
            access_token: token.accessToken,
            expires_at  : token.expiresAt
        } as StoredTokenDTO))
        return of(undefined)
    }

    getToken(): Observable<Token> {
        const createToken = compose<string, any, StoredTokenDTO>(
            defaultTo({ access_token: '', expires_at: '' }),
            JSON.parse
        )

        return fromPromise(AsyncStorage.getItem(this.accessTokenKey))
            .pipe(
                map((t: string) =>
                    Token.create(createToken(t).access_token, createToken(t).expires_at)
                )
            )
    }

    removeToken(): Observable<void> {
        AsyncStorage.removeItem(this.accessTokenKey)
        return of(undefined)
    }

}
