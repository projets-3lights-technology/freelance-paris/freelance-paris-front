export interface StoredTokenDTO {
    access_token: string
    expires_at: string
}
