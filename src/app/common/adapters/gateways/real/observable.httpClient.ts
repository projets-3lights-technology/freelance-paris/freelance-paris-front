import { Observable } from 'rxjs'
import { ajax } from 'rxjs/ajax'
import { map, switchMap } from 'rxjs/operators'
import { LocalStorage } from '../../../domain/storage/LocalStorage'
import { AuthorizedHttpClient } from './AuthorizedHttpClient'

export class ObservableHttpClient implements AuthorizedHttpClient {

    constructor(private localStorage: LocalStorage) {
    }

    get<R>(endpoint: string, headersOpt?: object): Observable<R> {
        return this.localStorage.getToken()
            .pipe(
                switchMap(token => {
                    const headers = {
                        ...headersOpt,
                        'Content-Type' : 'application/json',
                        'Authorization': 'Bearer ' + token.accessToken
                    }
                    return ajax.getJSON<R>(endpoint, headers)
                })
            )
    }

    post<B>(endpoint: string, body: B, headersOpt?: object): Observable<void> {
        return this.localStorage.getToken()
            .pipe(
                switchMap(token => {
                    const headers = {
                        ...headersOpt,
                        'Content-Type' : 'application/x-www-form-urlencoded',
                        'Authorization': 'Bearer ' + token.accessToken
                    }
                    return ajax.post(endpoint, body, headers)
                        .pipe(
                            map(() => undefined)
                        )
                })
            )
    }

    patch<B>(endpoint: string, body: B, headersOpt?: object): Observable<void> {
        return this.localStorage.getToken()
            .pipe(
                switchMap(token => {
                    const headers = {
                        ...headersOpt,
                        'Content-Type' : 'application/x-www-form-urlencoded',
                        'Authorization': 'Bearer ' + token.accessToken
                    }
                    return ajax.patch(endpoint, body, headers)
                        .pipe(
                            map(() => undefined)
                        )
                })
            )
    }

    delete(endpoint: string, headersOpt?: object): Observable<void> {
        return this.localStorage.getToken()
            .pipe(
                switchMap(token => {
                    const headers = {
                        ...headersOpt,
                        'Content-Type' : 'application/json',
                        'Authorization': 'Bearer ' + token.accessToken
                    }
                    return ajax.delete(endpoint, headers)
                        .pipe(
                            map(() => undefined)
                        )
                })
            )
    }

}
