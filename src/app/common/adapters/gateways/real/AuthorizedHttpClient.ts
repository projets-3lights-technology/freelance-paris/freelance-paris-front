import { Observable } from 'rxjs'

export interface AuthorizedHttpClient {

    get<R>(endpoint: string, headers?: object): Observable<R>

    post<B>(endpoint: string, body: B, headersOpt?: object): Observable<void>

    patch<B>(endpoint: string, body: B, headersOpt?: object): Observable<void>

    delete(endpoint: string, headersOpt?: object): Observable<void>

}
