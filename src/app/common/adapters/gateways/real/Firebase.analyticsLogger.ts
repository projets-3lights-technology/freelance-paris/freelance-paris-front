import firebase from 'react-native-firebase'
import { AnalyticsLogger, AnalyticsOption } from '../../../domain/analyticslogger/AnalyticsLogger'

export class FirebaseAnalyticsLogger implements AnalyticsLogger {

    logScreen(name: string): void {
        firebase.analytics().setCurrentScreen(name)
    }

    logEvent(name: string, options?: AnalyticsOption): void {
        firebase.analytics().logEvent(name, options)
    }

}
