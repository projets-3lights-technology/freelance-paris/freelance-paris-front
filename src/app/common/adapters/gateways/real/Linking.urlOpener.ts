import { Linking } from 'react-native'
import { Observable } from 'rxjs'
import { fromPromise } from 'rxjs-compat/observable/fromPromise'
import { UrlOpener } from '../../../domain/urlopener/UrlOpener'

export class LinkingUrlOpener implements UrlOpener {

    openUrl(url: string): Observable<void> {
        return fromPromise(Linking.openURL(url))
    }

}
