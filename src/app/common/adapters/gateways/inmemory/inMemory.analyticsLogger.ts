import { AnalyticsLogger, AnalyticsOption } from '../../../domain/analyticslogger/AnalyticsLogger'

export class InMemoryAnalyticsLogger implements AnalyticsLogger {

    private _screenLogged: string = ''
    private _eventLogged: string = ''
    private _eventOptionsLogged: AnalyticsOption = null

    logScreen(name: string): void {
        this._screenLogged = name
    }

    logEvent(name: string, options?: AnalyticsOption): void {
        this._eventLogged = name
        this._eventOptionsLogged = options
    }

    get screenLoggedWith(): string {
        return this._screenLogged
    }

    get eventLoggedWith(): string {
        return this._eventLogged
    }

    get eventOptionsLoggedWith(): AnalyticsOption {
        return this._eventOptionsLogged
    }

}
