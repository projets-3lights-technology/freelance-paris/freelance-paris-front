import { Observable, Subject } from 'rxjs'
import { UrlOpener } from '../../../domain/urlopener/UrlOpener'

export class InMemoryUrlOpener implements UrlOpener {

    private _open$: Subject<void> = new Subject()
    private url: string = ''

    openUrl(url: string): Observable<void> {
        this.url = url
        return this._open$
    }

    get openWith(): string {
        return this.url
    }

    triggerOpen = (): void => this._open$.next()

}
