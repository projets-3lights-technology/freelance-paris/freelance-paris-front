import { Observable, of, Subject } from 'rxjs'
import { Token } from '../../../domain/entities/token'
import { LocalStorage } from '../../../domain/storage/LocalStorage'

export class InMemoryLocalStorage implements LocalStorage {

    private _save$: Subject<void> = new Subject()
    private _remove$: Subject<void> = new Subject()
    private savedToken: Token = null
    private removedToken: boolean = false

    constructor(private inApp: boolean = true,
                private token: Token) {
    }

    saveToken(token: Token): Observable<void> {
        this.savedToken = token

        return this.inApp
            ? of(undefined)
            : this._save$
    }

    getToken(): Observable<Token> {
        if (this.token)
            return of(this.token)
        return of(this.savedToken)
    }

    removeToken(): Observable<void> {
        this.removedToken = true

        return this.inApp
            ? of(undefined)
            : this._remove$
    }

    get saveWith(): Token {
        return this.savedToken
    }

    get isRemoved(): boolean {
        return this.removedToken
    }

    triggerSaveToken = (): void => this._save$.next()

    triggerRemoveToken = (): void => this._remove$.next()

}
