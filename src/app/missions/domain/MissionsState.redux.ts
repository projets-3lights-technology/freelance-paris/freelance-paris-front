import { MissionContractType } from './entities/missionContractType'

export interface MissionsHeadersById {
    id: string
    name: string
    description: string
    averageDailyRate: number
    authorId: string
}

export interface MissionsHeadersStateById {
    [id: string]: MissionsHeadersById
}

export interface MissionsListingState {
    byId: MissionsHeadersStateById
    allIds: string[]
    error: any
    isLoading: boolean
    isRefreshing: boolean
}

export interface MissionDetailsById {
    id: string
    name: string
    description: string
    averageDailyRate: number
    customer: string
    location: string
    startDate: string
    durationInMonths: string
    remoteIsPossible: boolean
    contractType: MissionContractType
    alreadyContacted: boolean
}

export interface MissionDetailsStateById {
    [id: string]: MissionDetailsById
}

export interface MissionDetailsState {
    byId: MissionDetailsStateById
    error: any
    isLoading: boolean
}

export interface MissionUnderCreationState {
    name: string
    averageDailyRate: string
    customer: string
    location: string
    startDate: string
    durationInMonths: string
    remoteIsPossible: boolean
    description: string
    contractType: string
}

export interface MissionUnderCreationErrorState {
    name: string
    averageDailyRate: string
    location: string
    startDate: string
    durationInMonths: string
    description: string
}

export interface MissionCreationState {
    missionUnderCreation: MissionUnderCreationState
    missionUnderCreationError: MissionUnderCreationErrorState
    isLoading: boolean
    error: any
}

export interface MissionContactState {
    isLoading: boolean
    isSent: boolean
    error: any
}
