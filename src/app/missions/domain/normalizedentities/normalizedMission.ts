import { normalize, schema } from 'normalizr'
import { MissionDetails } from '../entities/missionDetails'
import { MissionHeader } from '../entities/missionHeader'
import { MissionDetailsStateById, MissionsHeadersStateById } from '../MissionsState.redux'

export interface NormalizedMissionsListing {
    entities: {
        missions: MissionsHeadersStateById
    },
    result: string[]
}

export interface NormalizedMissionDetails {
    entities: {
        mission: MissionDetailsStateById
    },
    result: string[]
}

const missionsSchema = new schema.Entity('missions')
const missionsListingSchema = new schema.Array(missionsSchema)

const missionDetailsSchema = new schema.Entity('mission')

export const normalizeMissionsListing = (data: MissionHeader[]): NormalizedMissionsListing =>
    normalize(data, missionsListingSchema)

export const normalizeMissionDetails = (data: MissionDetails): NormalizedMissionDetails =>
    normalize(data, missionDetailsSchema)
