import { MissionContractType } from './missionContractType'
import { MissionDetails } from './missionDetails'

export class MissionDetailsBuilder {

    protected _id: string = ''
    protected _name: string = ''
    protected _description: string = ''
    protected _averageDailyRate: number = 0
    protected _customer: string = ''
    protected _location: string = ''
    protected _startDate: string = ''
    protected _durationInMonths: string = ''
    protected _remoteIsPossible: boolean = false
    protected _contractType: MissionContractType = MissionContractType.OTHER
    protected _alreadyContacted: boolean = false

    withId(value: string): MissionDetailsBuilder {
        this._id = value
        return this
    }

    withName(value: string): MissionDetailsBuilder {
        this._name = value
        return this
    }

    withDescription(value: string): MissionDetailsBuilder {
        this._description = value
        return this
    }

    withAverageDailyRate(value: number): MissionDetailsBuilder {
        this._averageDailyRate = value
        return this
    }

    withCustomer(value: string): MissionDetailsBuilder {
        this._customer = value
        return this
    }

    withLocation(value: string): MissionDetailsBuilder {
        this._location = value
        return this
    }

    withStartDate(value: string): MissionDetailsBuilder {
        this._startDate = value
        return this
    }

    withDurationInMonths(value: string): MissionDetailsBuilder {
        this._durationInMonths = value
        return this
    }

    withRemoteIsPossible(value: boolean): MissionDetailsBuilder {
        this._remoteIsPossible = value
        return this
    }

    withContractType(value: MissionContractType): MissionDetailsBuilder {
        this._contractType = value
        return this
    }

    withAlreadyContacted(value: boolean): MissionDetailsBuilder {
        this._alreadyContacted = value
        return this
    }

    build(): MissionDetails {
        return {
            id              : this._id,
            name            : this._name,
            description     : this._description,
            averageDailyRate: this._averageDailyRate,
            customer        : this._customer,
            location        : this._location,
            startDate       : this._startDate,
            durationInMonths: this._durationInMonths,
            remoteIsPossible: this._remoteIsPossible,
            contractType    : this._contractType,
            alreadyContacted: this._alreadyContacted
        }
    }

}
