import * as uuid from 'uuid'
import { MissionContractType } from './missionContractType'
import { MissionForCreation } from './missionForCreation'

export class MissionForCreationBuilder {

    protected _id: string = uuid.v4()
    protected _name: string = ''
    protected _description: string = ''
    protected _averageDailyRate: number = 0
    protected _customer: string = ''
    protected _location: string = ''
    protected _startDate: string = ''
    protected _durationInMonths: string = ''
    protected _remoteIsPossible: boolean = false
    protected _authorId: string = ''
    protected _contractType: MissionContractType = MissionContractType.FREELANCE

    withId(value: string): MissionForCreationBuilder {
        this._id = value
        return this
    }

    withName(value: string): MissionForCreationBuilder {
        this._name = value
        return this
    }

    withDescription(value: string): MissionForCreationBuilder {
        this._description = value
        return this
    }

    withAverageDailyRate(value: number): MissionForCreationBuilder {
        this._averageDailyRate = value
        return this
    }

    withCustomer(value: string): MissionForCreationBuilder {
        this._customer = value
        return this
    }

    withLocation(value: string): MissionForCreationBuilder {
        this._location = value
        return this
    }

    withStartDate(value: string): MissionForCreationBuilder {
        this._startDate = value
        return this
    }

    withDurationInMonths(value: string): MissionForCreationBuilder {
        this._durationInMonths = value
        return this
    }

    withRemoteIsPossible(value: boolean): MissionForCreationBuilder {
        this._remoteIsPossible = value
        return this
    }

    withAuthorId(value: string): MissionForCreationBuilder {
        this._authorId = value
        return this
    }

    withContractType(value: MissionContractType): MissionForCreationBuilder {
        this._contractType = value
        return this
    }

    build(): MissionForCreation {
        return new MissionForCreation(
            this._name,
            this._description,
            this._averageDailyRate,
            this._customer,
            this._location,
            this._startDate,
            this._durationInMonths,
            this._remoteIsPossible,
            this._authorId,
            this._contractType,
            this._id
        )
    }

}
