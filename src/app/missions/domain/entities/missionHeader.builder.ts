import { MissionHeader } from './missionHeader'

export class MissionHeaderBuilder {

    protected _id: string = ''
    protected _name: string = ''
    protected _description: string = ''
    protected _averageDailyRate: number = 0
    protected _authorId: string = ''

    withId(value: string): MissionHeaderBuilder {
        this._id = value
        return this
    }

    withName(value: string): MissionHeaderBuilder {
        this._name = value
        return this
    }

    withDescription(value: string): MissionHeaderBuilder {
        this._description = value
        return this
    }

    withAverageDailyRate(value: number): MissionHeaderBuilder {
        this._averageDailyRate = value
        return this
    }

    withAuthorId(value: string): MissionHeaderBuilder {
        this._authorId = value
        return this
    }

    build(): MissionHeader {
        return {
            id              : this._id,
            name            : this._name,
            description     : this._description,
            averageDailyRate: this._averageDailyRate,
            authorId        : this._authorId
        }
    }

}
