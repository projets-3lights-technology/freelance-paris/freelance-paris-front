import { always, cond, equals, T } from 'ramda'

export enum MissionContractType {
    APPRENTICESHIP,
    CDD,
    CDI,
    FREELANCE,
    INTERIM,
    OTHER,
    PORTAGE
}

export const ContractTypeTo: { enum: (s: string) => MissionContractType, string: (e: MissionContractType) => string } = {
    enum: cond([
        [equals('apprenticeship'), always(MissionContractType.APPRENTICESHIP)],
        [equals('cdd'), always(MissionContractType.CDD)],
        [equals('cdi'), always(MissionContractType.CDI)],
        [equals('freelance'), always(MissionContractType.FREELANCE)],
        [equals('interim'), always(MissionContractType.INTERIM)],
        [equals('portage'), always(MissionContractType.PORTAGE)],
        [T, always(MissionContractType.OTHER)]
    ]),

    string: cond([
        [equals(MissionContractType.APPRENTICESHIP), always('apprenticeship')],
        [equals(MissionContractType.CDD), always('cdd')],
        [equals(MissionContractType.CDI), always('cdi')],
        [equals(MissionContractType.FREELANCE), always('freelance')],
        [equals(MissionContractType.INTERIM), always('interim')],
        [equals(MissionContractType.PORTAGE), always('portage')],
        [T, always('other')]
    ])
}
