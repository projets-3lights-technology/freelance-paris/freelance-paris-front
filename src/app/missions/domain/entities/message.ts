export interface Message {
    missionId: string
    senderId: string
    object: string
    message: string
}
