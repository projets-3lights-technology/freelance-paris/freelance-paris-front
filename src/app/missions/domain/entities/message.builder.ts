import { Message } from './message'

export class MessageBuilder {

    protected _missionId: string = ''
    protected _senderId: string = ''
    protected _object: string = ''
    protected _message: string = ''

    withMissionId(value: string): MessageBuilder {
        this._missionId = value
        return this
    }

    withSenderId(value: string): MessageBuilder {
        this._senderId = value
        return this
    }

    withObject(value: string): MessageBuilder {
        this._object = value
        return this
    }

    withMessage(value: string): MessageBuilder {
        this._message = value
        return this
    }

    build(): Message {
        return {
            missionId: this._missionId,
            senderId : this._senderId,
            object   : this._object,
            message  : this._message
        }
    }

}
