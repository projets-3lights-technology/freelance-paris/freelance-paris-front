export interface MissionHeader {
    id: string
    name: string
    description: string
    averageDailyRate: number
    authorId: string
}
