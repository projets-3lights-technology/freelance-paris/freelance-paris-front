import { MissionContractType } from './missionContractType'

export interface MissionDetails {
    id: string
    name: string
    description: string
    averageDailyRate: number
    customer: string
    location: string
    startDate: string
    durationInMonths: string
    remoteIsPossible: boolean
    contractType: MissionContractType
    alreadyContacted: boolean
}
