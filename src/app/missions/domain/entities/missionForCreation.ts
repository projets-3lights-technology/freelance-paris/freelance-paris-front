import { format, parse } from 'date-fns/fp'
import uuid = require('uuid')
import { MissionContractType } from './missionContractType'

export class MissionForCreation {

    constructor(private _name: string,
                private _description: string,
                private _averageDailyRate: number,
                private _customer: string,
                private _location: string,
                private _startDate: string,
                private _durationInMonth: string,
                private _remoteIsPossible: boolean,
                private _authorId: string,
                private _contractType: MissionContractType,
                private _id: string = uuid.v4()) {
    }

    get id(): string {
        return this._id
    }

    get name(): string {
        return this._name
    }

    get description(): string {
        return this._description
    }

    get averageDailyRate(): number {
        return this._averageDailyRate
    }

    get customer(): string {
        return this._customer
    }

    get location(): string {
        return this._location
    }

    get startDate(): string {
        return format('yyyy-MM-dd')(parse(new Date())('dd/MM/yyyy')(this._startDate))
    }

    get durationInMonth(): string {
        return this._durationInMonth
    }

    get remoteIsPossible(): boolean {
        return this._remoteIsPossible
    }

    get authorId(): string {
        return this._authorId
    }

    get contractType(): MissionContractType {
        return this._contractType
    }

}
