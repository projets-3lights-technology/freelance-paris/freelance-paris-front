import { Observable } from 'rxjs'
import { MissionDetails } from './entities/missionDetails'
import { MissionForCreation } from './entities/missionForCreation'
import { MissionHeader } from './entities/missionHeader'

export interface MissionsRepository {

    findAll(): Observable<MissionHeader[]>

    getById(id: string): Observable<MissionDetails>

    create(missionForCreation: MissionForCreation): Observable<void>

    remove(id: string): Observable<void>

}
