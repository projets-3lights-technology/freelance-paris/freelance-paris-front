import { Observable } from 'rxjs'
import { Message } from './entities/message'

export interface MessageSender {

    send(message: Message): Observable<void>

}
