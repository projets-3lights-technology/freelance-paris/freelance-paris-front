import { ActionsUnion } from '../../../../configuration/redux/helpers/actionsUnion.redux'
import { createAction } from '../../../../configuration/redux/helpers/createAction.redux'
import { Message } from '../../domain/entities/message'

/* commands */
export const SEND_MISSION_CONTACT = 'SEND_MISSION_CONTACT'

/* Events */
export const MISSION_CONTACT_SENT = 'MISSION_CONTACT_SENT'
export const MISSION_CONTACT_SENDING_FAILED = 'MISSION_CONTACT_SENDING_FAILED'

export const Actions = {
    sendMissionContact: (message: Message) => createAction(SEND_MISSION_CONTACT, message),

    missionContactSent         : () => createAction(MISSION_CONTACT_SENT),
    missionContactSendingFailed: (error: any) => createAction(MISSION_CONTACT_SENDING_FAILED, error)
}

export type Actions = ActionsUnion<typeof Actions>
