import { MissionContactState } from '../../domain/MissionsState.redux'
import * as missionContactActions from './missionContact.actions'

const initialContactState: MissionContactState = {
    isLoading: false,
    isSent   : false,
    error    : ''
}

const handleMissionContactActions = (state: MissionContactState = initialContactState,
                                     action: missionContactActions.Actions): MissionContactState => {
    switch (action.type) {
        case missionContactActions.SEND_MISSION_CONTACT:
            return {
                ...state,
                isLoading: true
            }

        case missionContactActions.MISSION_CONTACT_SENT:
            return {
                ...state,
                isLoading: false,
                isSent   : true
            }

        case missionContactActions.MISSION_CONTACT_SENDING_FAILED:
            return {
                ...state,
                isLoading: false,
                isSent   : false,
                error    : action.payload
            }

        default:
            return state
    }
}

export default handleMissionContactActions

export const _getMissionContactIsLoading = (state: MissionContactState) => state.isLoading
export const _getMissionContactIsSent = (state: MissionContactState) => state.isSent
