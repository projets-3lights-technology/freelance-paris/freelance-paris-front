import { ActionsObservable, combineEpics, ofType, StateObservable } from 'redux-observable'
import { Observable, of } from 'rxjs'
import { catchError, map, switchMap } from 'rxjs/operators'
import { AppState } from '../../../../configuration/redux/rootState.redux'
import { MessageSender } from '../../domain/MessageSender'
import * as missionContactActions from './missionContact.actions'

const contact = (action$: ActionsObservable<missionContactActions.Actions>,
                 state$: StateObservable<AppState>,
                 { messageSender }: { messageSender: MessageSender })
    : Observable<missionContactActions.Actions> =>
    action$.pipe(
        ofType<missionContactActions.Actions,
            ReturnType<typeof missionContactActions.Actions.sendMissionContact>>(missionContactActions.SEND_MISSION_CONTACT),
        switchMap(action => messageSender.send(action.payload)),
        map(() => missionContactActions.Actions.missionContactSent()),
        catchError(error => of(missionContactActions.Actions.missionContactSendingFailed(error)))
    )

export const missionContactEpics = combineEpics<missionContactActions.Actions, missionContactActions.Actions, AppState>(
    contact
)
