import { ActionsObservable, combineEpics, ofType, StateObservable } from 'redux-observable'
import { concat, Observable, of } from 'rxjs'
import { catchError, map, switchMap } from 'rxjs/operators'
import { AppState } from '../../../../configuration/redux/rootState.redux'
import { MissionsRepository } from '../../domain/MissionsRepository'
import * as missionsListingActions from '../missionslisting/missionsListing.actions'
import * as missionCreationActions from './missionCreation.actions'
import { MapToMissionForCreation } from './missionCreation.mapper'
import { MissionCreationValidator$ } from './missionCreation.validator'

const publishMission = () => concat(
    of(missionCreationActions.Actions.missionPublished()),
    of(missionsListingActions.Actions.refreshAllMissions())
)

const missionCreator = (action$: ActionsObservable<missionCreationActions.Actions>,
                        state$: StateObservable<AppState>,
                        { missionsRepository }: { missionsRepository: MissionsRepository })
    : Observable<missionCreationActions.Actions | missionsListingActions.Actions> =>
    action$.pipe(
        ofType<missionCreationActions.Actions,
            ReturnType<typeof missionCreationActions.Actions.publishMission>>(missionCreationActions.PUBLISH_MISSION),
        map(() => state$.value.domain.missionCreation.missionUnderCreation),
        switchMap(missionUnderCreation => MissionCreationValidator$(missionUnderCreation)
            .pipe(
                map(MapToMissionForCreation(state$.value.domain.memberInformation.allIds)),
                switchMap(missionForCreation => missionsRepository.create(missionForCreation)
                    .pipe(
                        switchMap(() => publishMission()),
                        catchError(error => of(missionCreationActions.Actions.missionPublishingFailed(error)))
                    )
                ),
                catchError(error => of(missionCreationActions.Actions.missionPublishingValidationFailed(error)))
            )
        )
    )

export const missionCreationEpics = combineEpics<missionCreationActions.Actions, any, AppState>(
    missionCreator
)
