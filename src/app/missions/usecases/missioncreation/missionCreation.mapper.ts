import { ContractTypeTo } from '../../domain/entities/missionContractType'
import { MissionForCreation } from '../../domain/entities/missionForCreation'
import { MissionForCreationBuilder } from '../../domain/entities/missionForCreation.builder'
import { MissionUnderCreationState } from '../../domain/MissionsState.redux'

export const MapToMissionForCreation = (memberId: string) => (missionUnderCreation: MissionUnderCreationState): MissionForCreation =>
    new MissionForCreationBuilder()
        .withName(missionUnderCreation.name)
        .withAverageDailyRate(+missionUnderCreation.averageDailyRate)
        .withCustomer(missionUnderCreation.customer)
        .withLocation(missionUnderCreation.location)
        .withStartDate(missionUnderCreation.startDate)
        .withDurationInMonths(missionUnderCreation.durationInMonths)
        .withRemoteIsPossible(missionUnderCreation.remoteIsPossible)
        .withDescription(missionUnderCreation.description)
        .withAuthorId(memberId)
        .withContractType(ContractTypeTo.enum(missionUnderCreation.contractType))
        .build()
