import { anyPass, evolve, isEmpty, not, pick, propSatisfies, trim } from 'ramda'
import { Observable, of, pipe, throwError } from 'rxjs'
import { MissionUnderCreationErrorState, MissionUnderCreationState } from '../../domain/MissionsState.redux'
import { MissionCreationFields } from './missionCreation.actions'

const requiredField = (field: string): string => isEmpty(trim(field)) ? 'required' : ''
export const defenseAgainstBadInformation = pipe<MissionUnderCreationState, MissionUnderCreationErrorState, MissionUnderCreationErrorState>(
    pick(['name', 'averageDailyRate', 'location', 'startDate', 'durationInMonths', 'description']),
    evolve({
        name            : requiredField,
        averageDailyRate: requiredField,
        location        : requiredField,
        startDate       : requiredField,
        durationInMonths: requiredField,
        description     : requiredField
    })
)

const hasAnError = pipe(isEmpty, not)
const fieldIsBad = (field: MissionCreationFields) => propSatisfies(hasAnError, field)
export const hasOneBadField: (form: MissionUnderCreationErrorState) => boolean = anyPass([
    fieldIsBad('name'),
    fieldIsBad('averageDailyRate'),
    fieldIsBad('location'),
    fieldIsBad('startDate'),
    fieldIsBad('durationInMonths'),
    fieldIsBad('description')
])

export const MissionCreationValidator$ = (missionUnderCreation: MissionUnderCreationState): Observable<MissionUnderCreationState> => {
    const errors: MissionUnderCreationErrorState = defenseAgainstBadInformation(missionUnderCreation)

    return hasOneBadField(errors)
        ? throwError(errors)
        : of(missionUnderCreation)
}
