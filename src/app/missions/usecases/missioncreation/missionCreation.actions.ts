import { ActionsUnion } from '../../../../configuration/redux/helpers/actionsUnion.redux'
import { createAction } from '../../../../configuration/redux/helpers/createAction.redux'
import { MissionUnderCreationErrorState } from '../../domain/MissionsState.redux'

export type MissionCreationFields = 'name'
    | 'averageDailyRate'
    | 'customer'
    | 'location'
    | 'startDate'
    | 'durationInMonths'
    | 'remoteIsPossible'
    | 'description'
    | 'contractType'

/* commands */
export const CHANGE_MISSION_INFORMATION = 'CHANGE_MISSION_INFORMATION'
export const RESET_MISSION_CREATION = 'RESET_MISSION_CREATION'
export const PUBLISH_MISSION = 'PUBLISH_MISSION'

/* Events */
export const MISSION_PUBLISHING_VALIDATION_FAILED = 'MISSION_PUBLISHING_VALIDATION_FAILED'
export const MISSION_PUBLISHED = 'MISSION_PUBLISHED'
export const MISSION_PUBLISHING_FAILED = 'MISSION_PUBLISHING_FAILED'

export const Actions = {
    changeMissionInformation: (field: MissionCreationFields, value: string | number | boolean) => createAction(CHANGE_MISSION_INFORMATION, {
        field,
        value
    }),
    changeMissionStartDate  : (value: string) => createAction(CHANGE_MISSION_INFORMATION, {
        field: 'startDate',
        value
    }),
    resetMissionCreation    : () => createAction(RESET_MISSION_CREATION),
    publishMission          : () => createAction(PUBLISH_MISSION),

    missionPublishingValidationFailed: (error: MissionUnderCreationErrorState) => createAction(MISSION_PUBLISHING_VALIDATION_FAILED, error),
    missionPublished                 : () => createAction(MISSION_PUBLISHED),
    missionPublishingFailed          : (error: any) => createAction(MISSION_PUBLISHING_FAILED, error)
}

export type Actions = ActionsUnion<typeof Actions>
