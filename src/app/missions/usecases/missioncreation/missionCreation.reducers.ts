import {
    MissionCreationState,
    MissionUnderCreationErrorState,
    MissionUnderCreationState
} from '../../domain/MissionsState.redux'
import * as missionCreationActions from './missionCreation.actions'
import { defenseAgainstBadInformation, hasOneBadField } from './missionCreation.validator'

const initialMissionCreationState: MissionCreationState = {
    missionUnderCreation     : {
        name            : '',
        averageDailyRate: '',
        customer        : '',
        location        : '',
        startDate       : '',
        durationInMonths: '',
        remoteIsPossible: false,
        description     : '',
        contractType    : 'freelance'
    },
    missionUnderCreationError: {
        name            : 'required',
        averageDailyRate: 'required',
        location        : 'required',
        startDate       : 'required',
        durationInMonths: 'required',
        description     : 'required'
    },
    isLoading                : false,
    error                    : ''
}

const handleMissionCreationActions = (state: MissionCreationState = initialMissionCreationState,
                                      action: missionCreationActions.Actions): MissionCreationState => {
    switch (action.type) {
        case missionCreationActions.CHANGE_MISSION_INFORMATION:
            const newMissionUnderCreation = {
                ...state.missionUnderCreation,
                [action.payload.field]: action.payload.field === 'startDate'
                    ? action.payload.value.toString()
                    : action.payload.value
            }
            return {
                ...state,
                missionUnderCreation     : newMissionUnderCreation,
                missionUnderCreationError: defenseAgainstBadInformation(newMissionUnderCreation)
            }

        case missionCreationActions.PUBLISH_MISSION:
            return {
                ...state,
                isLoading: true
            }

        case missionCreationActions.RESET_MISSION_CREATION:
        case missionCreationActions.MISSION_PUBLISHED:
            return initialMissionCreationState

        case missionCreationActions.MISSION_PUBLISHING_VALIDATION_FAILED:
            return {
                ...state,
                missionUnderCreationError: action.payload,
                isLoading                : false
            }

        case missionCreationActions.MISSION_PUBLISHING_FAILED:
            return {
                ...state,
                isLoading: false,
                error    : action.payload
            }

        default:
            return state
    }
}

export default handleMissionCreationActions

export const _getMissionCreationIsLoading = (state: MissionCreationState): boolean => state.isLoading
export const _getMissionUnderCreation = (state: MissionCreationState): MissionUnderCreationState => state.missionUnderCreation
export const _getMissionUnderCreationError = (state: MissionCreationState): MissionUnderCreationErrorState => state.missionUnderCreationError
export const _getMissionCreationHasError = (state: MissionCreationState): boolean => hasOneBadField(state.missionUnderCreationError)
