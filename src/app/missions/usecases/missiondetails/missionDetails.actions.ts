import { ActionsUnion } from '../../../../configuration/redux/helpers/actionsUnion.redux'
import { createAction } from '../../../../configuration/redux/helpers/createAction.redux'
import { MissionDetails } from '../../domain/entities/missionDetails'

/* Commands */
export const FETCH_MISSION_DETAILS = 'FETCH_MISSION_DETAILS'
export const ABORT_MISSION_DETAILS_FETCHING = 'ABORT_MISSION_DETAILS_FETCHING'

/* Events */
export const MISSION_DETAILS_FETCHED = 'MISSION_DETAILS_FETCHED'
export const MISSION_DETAILS_FETCHING_FAILED = 'MISSION_DETAILS_FETCHING_FAILED'

export const Actions = {
    fetchMissionDetails        : (id: string) => createAction(FETCH_MISSION_DETAILS, id),
    abortMissionDetailsFetching: () => createAction(ABORT_MISSION_DETAILS_FETCHING),

    missionDetailsFetched       : (mission: MissionDetails) => createAction(MISSION_DETAILS_FETCHED, mission),
    missionDetailsFetchingFailed: (error: any) => createAction(MISSION_DETAILS_FETCHING_FAILED, error)
}

export type Actions = ActionsUnion<typeof Actions>
