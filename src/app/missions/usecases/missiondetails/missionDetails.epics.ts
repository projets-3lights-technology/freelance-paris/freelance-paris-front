import { ActionsObservable, combineEpics, ofType, StateObservable } from 'redux-observable'
import { Observable, of } from 'rxjs'
import { catchError, map, switchMap, takeUntil } from 'rxjs/operators'
import { AppState } from '../../../../configuration/redux/rootState.redux'
import { MissionsRepository } from '../../domain/MissionsRepository'
import * as missionDetailsActions from './missionDetails.actions'

const missionByIdRetriever = (action$: ActionsObservable<missionDetailsActions.Actions>,
                              state$: StateObservable<AppState>,
                              { missionsRepository }: { missionsRepository: MissionsRepository })
    : Observable<missionDetailsActions.Actions> =>
    action$.pipe(
        ofType<missionDetailsActions.Actions,
            ReturnType<typeof missionDetailsActions.Actions.fetchMissionDetails>>(missionDetailsActions.FETCH_MISSION_DETAILS),
        switchMap(action => missionsRepository.getById(action.payload)
            .pipe(
                map(mission => missionDetailsActions.Actions.missionDetailsFetched(mission)),
                takeUntil(action$.ofType(missionDetailsActions.ABORT_MISSION_DETAILS_FETCHING)),
                catchError(error => of(missionDetailsActions.Actions.missionDetailsFetchingFailed(error)))
            )
        )
    )

export const missionDetailsEpics = combineEpics<missionDetailsActions.Actions, missionDetailsActions.Actions, AppState>(
    missionByIdRetriever
)
