import { defaultTo, path } from 'ramda'
import { combineReducers } from 'redux'
import { createSelector } from 'reselect'
import { Optional } from 'typescript-optional'
import { MissionDetailsById, MissionDetailsState, MissionDetailsStateById } from '../../domain/MissionsState.redux'
import { normalizeMissionDetails } from '../../domain/normalizedentities/normalizedMission'
import * as missionDetailsActions from './missionDetails.actions'

const isLoading = (state: boolean = false,
                   action: missionDetailsActions.Actions): boolean => {
    switch (action.type) {
        case missionDetailsActions.FETCH_MISSION_DETAILS:
            return true

        case missionDetailsActions.MISSION_DETAILS_FETCHED:
        case missionDetailsActions.ABORT_MISSION_DETAILS_FETCHING:
        case missionDetailsActions.MISSION_DETAILS_FETCHING_FAILED:
            return false

        default:
            return state
    }
}

const error = (state: any = '',
               action: missionDetailsActions.Actions): any => {
    switch (action.type) {
        case missionDetailsActions.MISSION_DETAILS_FETCHING_FAILED:
            return action.payload

        case missionDetailsActions.FETCH_MISSION_DETAILS:
        case missionDetailsActions.MISSION_DETAILS_FETCHED:
        case missionDetailsActions.ABORT_MISSION_DETAILS_FETCHING:
            return ''

        default:
            return state
    }
}

const byId = (state: MissionDetailsStateById = {},
              action: missionDetailsActions.Actions): MissionDetailsStateById => {
    switch (action.type) {
        case missionDetailsActions.MISSION_DETAILS_FETCHED:
            const normalizedMissionDetails = normalizeMissionDetails(action.payload)
            return defaultTo({})(path(['entities', 'mission'], normalizedMissionDetails))

        case missionDetailsActions.FETCH_MISSION_DETAILS:
        case missionDetailsActions.ABORT_MISSION_DETAILS_FETCHING:
        case missionDetailsActions.MISSION_DETAILS_FETCHING_FAILED:
            return {}

        default:
            return state
    }
}

export default combineReducers<MissionDetailsState>({
    byId,
    isLoading,
    error
})

export const _getMissionsDetailsIsLoading = (state: MissionDetailsState): boolean => state.isLoading
export const _getMissionDetails = (id: string) =>
    createSelector<MissionDetailsState, MissionDetailsStateById, Optional<MissionDetailsById>>(
        state => state.byId,
        missionById => Optional.ofNullable(missionById[id])
    )
