import { defaultTo, dissoc, equals, path, reject } from 'ramda'
import { combineReducers } from 'redux'
import { createSelector } from 'reselect'
import { MissionsHeadersById, MissionsHeadersStateById, MissionsListingState } from '../../domain/MissionsState.redux'
import { normalizeMissionsListing } from '../../domain/normalizedentities/normalizedMission'
import * as missionsListingActions from './missionsListing.actions'

const isLoading = (state: boolean = false,
                   action: missionsListingActions.Actions): boolean => {
    switch (action.type) {
        case missionsListingActions.FETCH_ALL_MISSIONS:
            return true

        case missionsListingActions.ALL_MISSIONS_FETCHED:
        case missionsListingActions.ABORT_ALL_MISSIONS_FETCHING:
        case missionsListingActions.ALL_MISSIONS_FETCHING_FAILED:
            return false

        default:
            return state
    }
}

const isRefreshing = (state: boolean = false,
                      action: missionsListingActions.Actions): boolean => {
    switch (action.type) {
        case missionsListingActions.REFRESH_ALL_MISSIONS:
            return true

        case missionsListingActions.ALL_MISSIONS_FETCHED:
        case missionsListingActions.ABORT_ALL_MISSIONS_FETCHING:
        case missionsListingActions.ALL_MISSIONS_FETCHING_FAILED:
            return false

        default:
            return state
    }
}

const error = (state: any = '',
               action: missionsListingActions.Actions): any => {
    switch (action.type) {
        case missionsListingActions.ALL_MISSIONS_FETCHING_FAILED:
            return action.payload

        case missionsListingActions.FETCH_ALL_MISSIONS:
        case missionsListingActions.ALL_MISSIONS_FETCHED:
        case missionsListingActions.ABORT_ALL_MISSIONS_FETCHING:
            return ''

        default:
            return state
    }
}

const byId = (state: MissionsHeadersStateById = {},
              action: missionsListingActions.Actions): MissionsHeadersStateById => {
    switch (action.type) {
        case missionsListingActions.ALL_MISSIONS_FETCHED:
            const normalizedMissionsListing = normalizeMissionsListing(action.payload)
            return defaultTo({})(path(['entities', 'missions'], normalizedMissionsListing))

        case missionsListingActions.MISSION_REMOVED:
            return dissoc(action.payload)(state)

        case missionsListingActions.FETCH_ALL_MISSIONS:
        case missionsListingActions.ABORT_ALL_MISSIONS_FETCHING:
        case missionsListingActions.ALL_MISSIONS_FETCHING_FAILED:
            return {}

        default:
            return state
    }
}

const allIds = (state: string[] = [],
                action: missionsListingActions.Actions): string[] => {
    switch (action.type) {
        case missionsListingActions.ALL_MISSIONS_FETCHED:
            const normalizedMissionsListing = normalizeMissionsListing(action.payload)
            return normalizedMissionsListing.result

        case missionsListingActions.MISSION_REMOVED:
            return reject(equals(action.payload))(state)

        case missionsListingActions.FETCH_ALL_MISSIONS:
        case missionsListingActions.ABORT_ALL_MISSIONS_FETCHING:
        case missionsListingActions.ALL_MISSIONS_FETCHING_FAILED:
            return []

        default:
            return state
    }
}

export default combineReducers<MissionsListingState>({
    byId,
    allIds,
    isLoading,
    isRefreshing,
    error
})

export const _getMissionsListingIsLoading = (state: MissionsListingState): boolean => state.isLoading
export const _getMissionsListingIsRefreshing = (state: MissionsListingState): boolean => state.isRefreshing

export const _getMissionsListing = createSelector<MissionsListingState, string[], MissionsHeadersStateById, MissionsHeadersById[]>(
    state => state.allIds,
    state => state.byId,
    (allMissions, missionsById) => allMissions.map(id => missionsById[id])
)
