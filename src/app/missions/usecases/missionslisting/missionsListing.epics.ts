import { ActionsObservable, combineEpics, ofType, StateObservable } from 'redux-observable'
import { Observable, of } from 'rxjs'
import { catchError, map, switchMap, takeUntil } from 'rxjs/operators'
import { AppState } from '../../../../configuration/redux/rootState.redux'
import { MissionsRepository } from '../../domain/MissionsRepository'
import * as missionsListingActions from './missionsListing.actions'

const missionsRetriever = (action$: ActionsObservable<missionsListingActions.Actions>,
                           state$: StateObservable<AppState>,
                           { missionsRepository }: { missionsRepository: MissionsRepository })
    : Observable<missionsListingActions.Actions> =>
    action$.pipe(
        ofType<missionsListingActions.Actions,
            ReturnType<typeof missionsListingActions.Actions.fetchAllMissions>>(missionsListingActions.FETCH_ALL_MISSIONS),
        switchMap(() => missionsRepository.findAll()
            .pipe(
                map(missions => missionsListingActions.Actions.allMissionsFetched(missions)),
                takeUntil(action$.ofType(missionsListingActions.ABORT_ALL_MISSIONS_FETCHING)),
                catchError(error => of(missionsListingActions.Actions.allMissionsFetchingFailed(error)))
            )
        )
    )

const missionsRefresher = (action$: ActionsObservable<missionsListingActions.Actions>,
                           state$: StateObservable<AppState>,
                           { missionsRepository }: { missionsRepository: MissionsRepository })
    : Observable<missionsListingActions.Actions> =>
    action$.pipe(
        ofType<missionsListingActions.Actions,
            ReturnType<typeof missionsListingActions.Actions.refreshAllMissions>>(missionsListingActions.REFRESH_ALL_MISSIONS),
        switchMap(() => missionsRepository.findAll()
            .pipe(
                map(missions => missionsListingActions.Actions.allMissionsFetched(missions)),
                takeUntil(action$.ofType(missionsListingActions.ABORT_ALL_MISSIONS_FETCHING)),
                catchError(error => of(missionsListingActions.Actions.allMissionsFetchingFailed(error)))
            )
        )
    )

const missionsDestroyer = (action$: ActionsObservable<missionsListingActions.Actions>,
                           state$: StateObservable<AppState>,
                           { missionsRepository }: { missionsRepository: MissionsRepository })
    : Observable<missionsListingActions.Actions> =>
    action$.pipe(
        ofType<missionsListingActions.Actions,
            ReturnType<typeof missionsListingActions.Actions.removeMission>>(missionsListingActions.REMOVE_MISSION),
        switchMap(action => missionsRepository.remove(action.payload)
            .pipe(
                map(() => missionsListingActions.Actions.missionRemoved(action.payload))
                //catchError(error => of(missionsListingActions.Actions.allMissionsFetchingFailed(error)))
            )
        )
    )

export const missionsListingEpics = combineEpics<missionsListingActions.Actions, missionsListingActions.Actions, AppState>(
    missionsRetriever,
    missionsRefresher,
    missionsDestroyer
)
