import { ActionsUnion } from '../../../../configuration/redux/helpers/actionsUnion.redux'
import { createAction } from '../../../../configuration/redux/helpers/createAction.redux'
import { MissionHeader } from '../../domain/entities/missionHeader'

/* Commands */
export const FETCH_ALL_MISSIONS = 'FETCH_ALL_MISSIONS'
export const ABORT_ALL_MISSIONS_FETCHING = 'ABORT_ALL_MISSIONS_FETCHING'
export const REFRESH_ALL_MISSIONS = 'REFRESH_ALL_MISSIONS'
export const REMOVE_MISSION = 'REMOVE_MISSION'

/* Events */
export const ALL_MISSIONS_FETCHED = 'ALL_MISSIONS_FETCHED'
export const ALL_MISSIONS_FETCHING_FAILED = 'ALL_MISSIONS_FETCHING_FAILED'
export const MISSION_REMOVED = 'MISSION_REMOVED'

export const Actions = {
    fetchAllMissions        : () => createAction(FETCH_ALL_MISSIONS),
    abortAllMissionsFetching: () => createAction(ABORT_ALL_MISSIONS_FETCHING),
    refreshAllMissions      : () => createAction(REFRESH_ALL_MISSIONS),
    removeMission           : (id: string) => createAction(REMOVE_MISSION, id),

    allMissionsFetched       : (missions: MissionHeader[]) => createAction(ALL_MISSIONS_FETCHED, missions),
    allMissionsFetchingFailed: (error: any) => createAction(ALL_MISSIONS_FETCHING_FAILED, error),
    missionRemoved           : (id: string) => createAction(MISSION_REMOVED, id)
}

export type Actions = ActionsUnion<typeof Actions>
