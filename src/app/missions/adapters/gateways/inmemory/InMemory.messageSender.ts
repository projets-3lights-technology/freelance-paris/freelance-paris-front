import { Observable, of, Subject } from 'rxjs'
import { Message } from '../../../domain/entities/message'
import { MessageSender } from '../../../domain/MessageSender'

export class InMemoryMessageSender implements MessageSender {

    private _contact$: Subject<void> = new Subject()
    private message: Message = null

    constructor(private inApp: boolean = true) {
    }

    send(message: Message): Observable<void> {
        this.message = message

        return this.inApp
            ? of(undefined)
            : this._contact$
    }

    get sendWith(): Message {
        return this.message
    }

    triggerContact = (): void => this._contact$.next()
    contactOnError = (error: any): void => this._contact$.error(error)

}
