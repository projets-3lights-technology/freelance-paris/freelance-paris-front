import { BehaviorSubject, Observable, of, Subject } from 'rxjs'
import { delay } from 'rxjs/operators'
import { MissionDetails } from '../../../domain/entities/missionDetails'
import { MissionDetailsBuilder } from '../../../domain/entities/missionDetails.builder'
import { MissionForCreation } from '../../../domain/entities/missionForCreation'
import { MissionHeader } from '../../../domain/entities/missionHeader'
import { MissionHeaderBuilder } from '../../../domain/entities/missionHeader.builder'
import { MissionsRepository } from '../../../domain/MissionsRepository'

export class InMemoryMissionsRepository implements MissionsRepository {
    private _allMissions$: Subject<MissionHeader[]> = new BehaviorSubject(this.missionsHeader)
    private _missionDetails$: Subject<MissionDetails> = new Subject()
    private _missionCreation$: Subject<void> = new Subject()
    private _missionRemove$: Subject<void> = new Subject()
    private missionForCreation: MissionForCreation = null

    constructor(private missionsHeader: MissionHeader[] = [], private missionDetails: MissionDetails[] = []) {
    }

    findAll(): Observable<MissionHeader[]> {
        return this.missionsHeader.length > 0
            ? of(this.missionsHeader).pipe(delay(250))
            : this._allMissions$
    }

    getById(id: string): Observable<MissionDetails> {
        return this.missionDetails.length > 0
            ? of(this.missionDetails.filter(m => m.id === id)[0]).pipe(delay(250))
            : this._missionDetails$
    }

    create(missionForCreation: MissionForCreation): Observable<void> {
        this.missionForCreation = missionForCreation

        if (this.missionsHeader.length < 1)
            return this._missionCreation$

        const newMissionHeader: MissionHeader = new MissionHeaderBuilder()
            .withId(missionForCreation.id)
            .withName(missionForCreation.name)
            .withDescription(missionForCreation.description)
            .withAverageDailyRate(missionForCreation.averageDailyRate)
            .withAuthorId(missionForCreation.authorId)
            .build()
        const newMissionDetails: MissionDetails = new MissionDetailsBuilder()
            .withId(missionForCreation.id)
            .withName(missionForCreation.name)
            .withAverageDailyRate(missionForCreation.averageDailyRate)
            .withCustomer(missionForCreation.customer)
            .withLocation(missionForCreation.location)
            .withStartDate(missionForCreation.startDate)
            .withDurationInMonths(missionForCreation.durationInMonth)
            .withContractType(missionForCreation.contractType)
            .withRemoteIsPossible(missionForCreation.remoteIsPossible)
            .withDescription(missionForCreation.description)
            .build()

        this.missionsHeader.push(newMissionHeader)
        this.missionDetails.push(newMissionDetails)
        this._allMissions$.next(this.missionsHeader)
        return of(undefined).pipe(delay(250))
    }

    remove(id: string): Observable<void> {
        if (this.missionsHeader.length > 0) {
            this.missionsHeader = this.missionsHeader.filter(mission => mission.id !== id)
            this._allMissions$.next(this.missionsHeader)
            return of(undefined)
        }

        return this._missionRemove$
    }

    get createWith(): MissionForCreation {
        return this.missionForCreation
    }

    populateAllMissions = (missionsPopulation: MissionHeader[]): void => this._allMissions$.next(missionsPopulation)
    populateMissionDetails = (missionPopulation: MissionDetails): void => this._missionDetails$.next(missionPopulation)
    triggerMissionCreation = (): void => this._missionCreation$.next()
    triggerMissionRemove = (): void => this._missionRemove$.next()

    allMissionsOnError = (error: any): void => this._allMissions$.error(error)
    missionDetailsOnError = (error: any): void => this._missionDetails$.error(error)
    createMissionOnError = (error: any): void => this._missionCreation$.error(error)

}
