import { Observable } from 'rxjs'
import { AppConfiguration } from '../../../../../configuration/environment/AppConfiguration'
import { AuthorizedHttpClient } from '../../../../common/adapters/gateways/real/AuthorizedHttpClient'
import { Message } from '../../../domain/entities/message'
import { MessageSender } from '../../../domain/MessageSender'
import { MissionContactDTO } from './DTO/MissionContactDTO'

export class RESTMessageSender implements MessageSender {

    private readonly missionsEndpoint = this.appConfiguration.get('FREELANCE_API_URL') + 'missions'

    constructor(private authorizedHttpClient: AuthorizedHttpClient,
                private appConfiguration: AppConfiguration) {
    }

    send(message: Message): Observable<void> {
        const body: MissionContactDTO = {
            subject: message.object,
            message: message.message
        }
        return this.authorizedHttpClient
            .post<MissionContactDTO>(this.missionsEndpoint + '/' + message.missionId + '/contact', body)
    }

}
