export interface MissionHeaderDTO {
    id: string,
    name: string,
    description: string,
    average_daily_rate: number,
    author_id: string
}
