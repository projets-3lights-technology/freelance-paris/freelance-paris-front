export interface MissionCreationDTO {
    id: string,
    name: string,
    description: string,
    average_daily_rate: number,
    customer: string,
    location: string,
    start_date: string,
    duration_in_months: string,
    remote_is_possible: boolean,
    contract_type: string
}
