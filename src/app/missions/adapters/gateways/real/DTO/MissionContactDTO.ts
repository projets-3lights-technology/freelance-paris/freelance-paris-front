export interface MissionContactDTO {
    subject: string
    message: string
}
