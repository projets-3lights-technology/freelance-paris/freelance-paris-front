import { ContractTypeTo } from '../../../../domain/entities/missionContractType'
import { MissionDetails } from '../../../../domain/entities/missionDetails'
import { MissionDetailsBuilder } from '../../../../domain/entities/missionDetails.builder'
import { MissionHeader } from '../../../../domain/entities/missionHeader'
import { MissionHeaderBuilder } from '../../../../domain/entities/missionHeader.builder'
import { MissionDetailsDTO } from '../DTO/MissionDetailsDTO'
import { MissionHeaderDTO } from '../DTO/MissionHeaderDTO'

export const MapToMissionHeader = (missionHeaderDTO: MissionHeaderDTO): MissionHeader =>
    new MissionHeaderBuilder()
        .withId(missionHeaderDTO.id)
        .withName(missionHeaderDTO.name)
        .withDescription(missionHeaderDTO.description)
        .withAverageDailyRate(missionHeaderDTO.average_daily_rate)
        .withAuthorId(missionHeaderDTO.author_id)
        .build()

export const MapToMissionDetails = (missionDetailsDTO: MissionDetailsDTO): MissionDetails =>
    new MissionDetailsBuilder()
        .withId(missionDetailsDTO.id)
        .withName(missionDetailsDTO.name)
        .withDescription(missionDetailsDTO.description)
        .withAverageDailyRate(missionDetailsDTO.average_daily_rate)
        .withCustomer(missionDetailsDTO.customer)
        .withLocation(missionDetailsDTO.location)
        .withStartDate(missionDetailsDTO.start_date)
        .withDurationInMonths(missionDetailsDTO.duration_in_months)
        .withRemoteIsPossible(missionDetailsDTO.remote_is_possible)
        .withContractType(ContractTypeTo.enum(missionDetailsDTO.contract_type.toLowerCase()))
        .withAlreadyContacted(missionDetailsDTO.already_contacted)
        .build()
