import { format, parse } from 'date-fns/fp'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { AppConfiguration } from '../../../../../configuration/environment/AppConfiguration'
import { AuthorizedHttpClient } from '../../../../common/adapters/gateways/real/AuthorizedHttpClient'
import { ContractTypeTo } from '../../../domain/entities/missionContractType'
import { MissionDetails } from '../../../domain/entities/missionDetails'
import { MissionForCreation } from '../../../domain/entities/missionForCreation'
import { MissionHeader } from '../../../domain/entities/missionHeader'
import { MissionsRepository } from '../../../domain/MissionsRepository'
import { MissionCreationDTO } from './DTO/MissionCreationDTO'
import { MissionDetailsDTO } from './DTO/MissionDetailsDTO'
import { MissionHeaderDTO } from './DTO/MissionHeaderDTO'
import { MapToMissionDetails, MapToMissionHeader } from './mappers/mission.mapper'

export class RESTMissionsRepository implements MissionsRepository {

    private readonly missionsEndpoint = this.appConfiguration.get('FREELANCE_API_URL') + 'missions'

    constructor(private authorizedHttpClient: AuthorizedHttpClient,
                private appConfiguration: AppConfiguration) {
    }

    findAll(): Observable<MissionHeader[]> {
        return this.authorizedHttpClient
            .get<MissionHeaderDTO[]>(this.missionsEndpoint)
            .pipe(
                map<MissionHeaderDTO[], MissionHeader[]>(res => res.map(MapToMissionHeader))
            )
    }

    getById(id: string): Observable<MissionDetails> {
        return this.authorizedHttpClient
            .get<MissionDetailsDTO>(this.missionsEndpoint + '/' + id)
            .pipe(
                map<MissionDetailsDTO, MissionDetails>(MapToMissionDetails)
            )
    }

    create(missionForCreation: MissionForCreation): Observable<void> {
        const body: MissionCreationDTO = {
            id                : missionForCreation.id,
            name              : missionForCreation.name,
            description       : missionForCreation.description,
            average_daily_rate: missionForCreation.averageDailyRate,
            customer          : missionForCreation.customer,
            location          : missionForCreation.location,
            start_date        : format('yyyy-MM-dd\'T\'HH:mm:ss.SSSxxx')(parse(new Date())('yyyy-MM-dd')(missionForCreation.startDate)),
            duration_in_months: missionForCreation.durationInMonth,
            remote_is_possible: missionForCreation.remoteIsPossible,
            contract_type     : ContractTypeTo.string(missionForCreation.contractType).toUpperCase()
        }
        return this.authorizedHttpClient
            .post<MissionCreationDTO>(this.missionsEndpoint, body)
    }

    remove(id: string): Observable<void> {
        return this.authorizedHttpClient
            .delete(this.missionsEndpoint + '/' + id)
    }

}
