export const AppAssets = {
    logo       : require('./freelance_logo.png'),
    background : require('./background.jpg'),
    userDefault: require('./user_default.png')
}
