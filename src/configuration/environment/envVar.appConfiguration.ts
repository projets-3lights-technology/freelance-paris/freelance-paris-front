import Config from 'react-native-config'
import { AppConfiguration, AppMode, ConfigurationKeys } from './AppConfiguration'

export class EnvVarConfig implements AppConfiguration {

    get(key: ConfigurationKeys): string {
        switch (key) {
            case 'MODE':
                return Config[key] === 'inmemory' ? AppMode.IN_MEMORY : AppMode.REAL
            default:
                return Config[key]
        }
    }

}
