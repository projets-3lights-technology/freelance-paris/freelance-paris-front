export type ConfigurationKeys = 'MODE'
    | 'FREELANCE_API_URL'
    | 'LINKEDIN_CLIENT_ID'
    | 'LINKEDIN_CLIENT_SECRET'
    | 'LINKEDIN_REDIRECT_URL'
    | 'MAIL_CONTACT'

export enum AppMode {
    IN_MEMORY = 'IN_MEMORY',
    REAL = 'REAL'
}

export interface AppConfiguration {

    get(key: ConfigurationKeys): string

}
