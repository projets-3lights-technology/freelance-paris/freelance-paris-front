import { AnalyticsLogger } from '../../app/common/domain/analyticslogger/AnalyticsLogger'
import { EnvVarConfig } from '../environment/envVar.appConfiguration'
import { AnalyticsLoggerFactory } from './dependencyfactories/analyticsLogger.factory'

export interface AnalyticsDependencies {
    analyticsLogger: AnalyticsLogger
}

export const appAnalyticsDependencies: AnalyticsDependencies = {
    analyticsLogger: new AnalyticsLoggerFactory(new EnvVarConfig()).makeAnalyticsLogger()
}
