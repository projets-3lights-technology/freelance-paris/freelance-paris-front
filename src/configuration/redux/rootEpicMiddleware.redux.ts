import { Epic } from 'redux-observable'
import { LocalStorage } from '../../app/common/domain/storage/LocalStorage'
import { UrlOpener } from '../../app/common/domain/urlopener/UrlOpener'
import { gatekeeperEpics } from '../../app/common/usecases/gatekeeper/gatekeeper.epics'
import { navigationEpics } from '../../app/common/usecases/navigation/navigation.epics'
import { MemberRepository } from '../../app/member/domain/MemberRepository'
import { memberInformationEpics } from '../../app/member/usecases/memberinformation/memberInformation.epics'
import { memberModificationEpics } from '../../app/member/usecases/membermodification/memberModification.epics'
import { MessageSender } from '../../app/missions/domain/MessageSender'
import { MissionsRepository } from '../../app/missions/domain/MissionsRepository'
import { missionContactEpics } from '../../app/missions/usecases/missioncontact/missionContact.epics'
import { missionCreationEpics } from '../../app/missions/usecases/missioncreation/missionCreation.epics'
import { missionDetailsEpics } from '../../app/missions/usecases/missiondetails/missionDetails.epics'
import { missionsListingEpics } from '../../app/missions/usecases/missionslisting/missionsListing.epics'
import { AppConfiguration } from '../environment/AppConfiguration'
import { EnvVarConfig } from '../environment/envVar.appConfiguration'
import { LocalStorageFactory } from './dependencyfactories/localStorage.factory'
import { MemberRepositoryFactory } from './dependencyfactories/memberRepositoryFactory'
import { MessageSenderFactory } from './dependencyfactories/messageSender.factory'
import { MissionsRepositoryFactory } from './dependencyfactories/missionsRepository.factory'
import { UrlOpenerFactory } from './dependencyfactories/urlOpener.factory'

export interface EpicsDependencies {
    dependencies: {
        configuration: AppConfiguration
        missionsRepository: MissionsRepository
        messageSender: MessageSender
        localStorage: LocalStorage
        memberRepository: MemberRepository
        urlOpener: UrlOpener
    }
}

// todo
export const appEpics: Array<Epic<any, any>> = [
    gatekeeperEpics,
    memberInformationEpics,
    memberModificationEpics,
    missionsListingEpics,
    missionDetailsEpics,
    missionCreationEpics,
    missionContactEpics,
    navigationEpics
]

export const appEpicsDependencies: EpicsDependencies = {
    dependencies: {
        configuration     : new EnvVarConfig(),
        missionsRepository: new MissionsRepositoryFactory(new EnvVarConfig()).makeMissionsRepository(),
        messageSender     : new MessageSenderFactory(new EnvVarConfig()).makeMessageSender(),
        localStorage      : new LocalStorageFactory(new EnvVarConfig()).makeLocalStorage(),
        memberRepository  : new MemberRepositoryFactory(new EnvVarConfig()).makeMemberRepository(),
        urlOpener         : new UrlOpenerFactory(new EnvVarConfig()).makeUrlOpener()
    }
}
