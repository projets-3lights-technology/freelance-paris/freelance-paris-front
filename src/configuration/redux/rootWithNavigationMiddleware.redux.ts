import {
    createNavigationReducer,
    createReactNavigationReduxMiddleware,
    ReducerState,
    reduxifyNavigator
} from 'react-navigation-redux-helpers'
import { connect } from 'react-redux'
import { AnyAction, Reducer } from 'redux'
import { AppNavigationLayout } from '../withNavigation/appPagesLayout.navigation'
import { AppState } from './rootState.redux'

export const reactNavigationReducer: Reducer<ReducerState, AnyAction> = createNavigationReducer(AppNavigationLayout)

export const navigationMiddleware = createReactNavigationReduxMiddleware<AppState>(
    'root',
    state => state.ui.navigation
)

const App: any = reduxifyNavigator(AppNavigationLayout, 'root')

const mapStateToProps = (state: AppState) => ({
    state: state.ui.navigation
})

export const WithNavigation = connect(mapStateToProps)(App)
