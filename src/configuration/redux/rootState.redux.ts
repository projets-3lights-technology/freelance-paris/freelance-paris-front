import { GatekeeperState } from '../../app/common/domain/GatekeeperState.redux'
import { InternationalizationState } from '../../app/common/domain/InternationalizationState.redux'
import { UserNavigationState } from '../../app/common/domain/UserNavigationState.redux'
import { MemberInformationState, MemberModificationState } from '../../app/member/domain/MemberState.redux'
import {
    MissionContactState,
    MissionCreationState,
    MissionDetailsState,
    MissionsListingState
} from '../../app/missions/domain/MissionsState.redux'

export interface UIState {
    navigation: any
    userNavigation: UserNavigationState
    internationalization: InternationalizationState
}

export interface DomainState {
    gatekeeper: GatekeeperState
    memberInformation: MemberInformationState
    memberModification: MemberModificationState
    missions: MissionsListingState
    missionDetails: MissionDetailsState
    missionCreation: MissionCreationState
    missionContact: MissionContactState
}

export interface AppState {
    ui: UIState
    domain: DomainState
}
