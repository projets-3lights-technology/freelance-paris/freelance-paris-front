import { Action } from 'redux'

export interface ActionWithPayload<T extends string, P> extends Action<T> {
    payload: P
}

export function createAction<T extends string>(type: T): Action<T>
export function createAction<T extends string, P>(type: T, payload: P): ActionWithPayload<T, P>
export function createAction(type: string, payload?: any) {
    return payload
        ? { type, payload }
        : { type }
}

export interface NavAction<T, N> extends Action<T> {
    name: N
}
export interface NavActionWithRouteName<T, N, R> extends NavAction<T, N> {
    routeName: R
}
export interface NavActionWithParams<T, N, R, P> extends NavActionWithRouteName<T, N, R> {
    params: P
}

export function createNavAction<T, N>(type: T, name: N): NavAction<T, N>
export function createNavAction<T, N, R>(type: T, name: N, routeName: R): NavActionWithRouteName<T, N, R>
export function createNavAction<T, N, R, P extends { [key: string]: string }>(type: T, name: N, routeName: R, params: P): NavActionWithParams<T, N, R, P>
export function createNavAction(type: string, name: string, routeName?: string, params?: { [key: string]: string }) {
    return routeName
        ? params
            ? { type, name, routeName, params }
            : { type, name, routeName }
        : { type, name }
}
