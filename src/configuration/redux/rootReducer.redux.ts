import { combineReducers } from 'redux'
import { Optional } from 'typescript-optional'
import { I18nKeys } from '../../app/common/domain/translations/i18.keys'
import * as gatekeeperActions from '../../app/common/usecases/gatekeeper/gatekeeper.actions'
import gatekeeper, * as fromGatekeeper from '../../app/common/usecases/gatekeeper/gatekeeper.reducers'
import * as i18nActions from '../../app/common/usecases/internationalization/internationalization.actions'
import internationalization, * as fromI18n from '../../app/common/usecases/internationalization/internationalization.reducer'
import * as navigationActions from '../../app/common/usecases/navigation/navigation.actions'
import userNavigation, * as fromNavigation from '../../app/common/usecases/navigation/navigation.reducers'
import {
    MemberInformationById,
    MemberUnderModificationErrorState,
    MemberUnderModificationState
} from '../../app/member/domain/MemberState.redux'
import * as memberInformationAction from '../../app/member/usecases/memberinformation/memberInformation.actions'
import memberInformation, * as fromMemberInformation from '../../app/member/usecases/memberinformation/memberInformation.reducers'
import * as memberModificationAction from '../../app/member/usecases/membermodification/memberModification.actions'
import memberModification, * as fromMemberModification from '../../app/member/usecases/membermodification/memberModification.reducers'
import {
    MissionDetailsById,
    MissionsHeadersById,
    MissionUnderCreationErrorState,
    MissionUnderCreationState
} from '../../app/missions/domain/MissionsState.redux'
import * as missionContactActions from '../../app/missions/usecases/missioncontact/missionContact.actions'
import missionContact, * as fromMissionContact from '../../app/missions/usecases/missioncontact/missionContact.reducers'
import * as missionCreationActions from '../../app/missions/usecases/missioncreation/missionCreation.actions'
import missionCreation, * as fromMissionCreation from '../../app/missions/usecases/missioncreation/missionCreation.reducers'
import * as missionDetailsActions from '../../app/missions/usecases/missiondetails/missionDetails.actions'
import missionDetails, * as fromMissionDetails from '../../app/missions/usecases/missiondetails/missionDetails.reducers'
import * as missionsListingActions from '../../app/missions/usecases/missionslisting/missionsListing.actions'
import missions, * as fromMissionsListing from '../../app/missions/usecases/missionslisting/missionsListing.reducers'
import { AppState } from './rootState.redux'
import { reactNavigationReducer } from './rootWithNavigationMiddleware.redux'

export type AppActionsType = navigationActions.Actions
    | i18nActions.Actions
    | gatekeeperActions.Actions
    | memberInformationAction.Actions
    | memberModificationAction.Actions
    | missionsListingActions.Actions
    | missionDetailsActions.Actions
    | missionCreationActions.Actions
    | missionContactActions.Actions

// todo: add AppActionsType
const AppReducer = combineReducers<AppState>({
    ui    : combineReducers({
        navigation: reactNavigationReducer,
        userNavigation,
        internationalization
    }),
    domain: combineReducers({
        gatekeeper,
        memberInformation,
        memberModification,
        missions,
        missionDetails,
        missionCreation,
        missionContact
    })
})

export const RootReducer = (state: AppState, action: any) => {
    if (action.name === navigationActions.QUIT_APPLICATION)
        state = undefined

    return AppReducer(state, action)
}

export const getCurrentPage = (state: AppState): string =>
    fromNavigation._getCurrentPage(state.ui.userNavigation)
export const getMissionDetailsId = (state: AppState): string =>
    fromNavigation._getMissionDetailsId(state.ui.userNavigation)
export const isOnMissions = (state: AppState): boolean =>
    fromNavigation._isOnMissions(state.ui.userNavigation)
export const isMissionActionsOpen = (state: AppState) => (id: string): boolean =>
    fromNavigation._isMissionActionsOpen(id)(state.ui.userNavigation)
export const isMissionContactOpen = (state: AppState) =>
    fromNavigation._isMissionContactOpen(state.ui.userNavigation)
export const isInformationTemplateOpen = (state: AppState) =>
    fromNavigation._isInformationTemplateOpen(state.ui.userNavigation)

export const t = (state: AppState) => (key: I18nKeys): string =>
    fromI18n._t(key)(state.ui.internationalization)
export const capitalize = (state: AppState) => (key: I18nKeys): string =>
    fromI18n._capitalize(key)(state.ui.internationalization)

export const canAutoConnect = (state: AppState): boolean =>
    fromGatekeeper._canAutoConnect(state.domain.gatekeeper)
export const isAuthenticated = (state: AppState): boolean =>
    fromGatekeeper._isAuthenticated(state.domain.gatekeeper)

export const getMemberInformationIsLoading = (state: AppState): boolean =>
    fromMemberInformation._getMemberInformationIsLoading(state.domain.memberInformation)
export const getMemberInformation = (state: AppState): Optional<MemberInformationById> =>
    fromMemberInformation._getMemberInformation(state.domain.memberInformation)

export const getMemberModificationIsLoading = (state: AppState): boolean =>
    fromMemberModification._getMemberModificationIsLoading(state.domain.memberModification)
export const getMemberUnderModification = (state: AppState): MemberUnderModificationState =>
    fromMemberModification._getMemberUnderModification(state.domain.memberModification)
export const getMemberUnderModificationError = (state: AppState): MemberUnderModificationErrorState =>
    fromMemberModification._getMemberUnderModificationError(state.domain.memberModification)
export const getMemberModificationHasError = (state: AppState): boolean =>
    fromMemberModification._getMemberModificationHasError(state.domain.memberModification)

export const getMissionsListingIsLoading = (state: AppState): boolean =>
    fromMissionsListing._getMissionsListingIsLoading(state.domain.missions)
export const getMissionsListing = (state: AppState): MissionsHeadersById[] =>
    fromMissionsListing._getMissionsListing(state.domain.missions)
export const getMissionsListingIsRefreshing = (state: AppState): boolean =>
    fromMissionsListing._getMissionsListingIsRefreshing(state.domain.missions)

export const getMissionsDetailsIsLoading = (state: AppState): boolean =>
    fromMissionDetails._getMissionsDetailsIsLoading(state.domain.missionDetails)
export const getMissionDetails = (state: AppState): Optional<MissionDetailsById> =>
    fromMissionDetails._getMissionDetails(state.ui.userNavigation.missionDetails.id)(state.domain.missionDetails)

export const getMissionCreationIsLoading = (state: AppState): boolean =>
    fromMissionCreation._getMissionCreationIsLoading(state.domain.missionCreation)
export const getMissionUnderCreation = (state: AppState): MissionUnderCreationState =>
    fromMissionCreation._getMissionUnderCreation(state.domain.missionCreation)
export const getMissionUnderCreationError = (state: AppState): MissionUnderCreationErrorState =>
    fromMissionCreation._getMissionUnderCreationError(state.domain.missionCreation)
export const getMissionCreationHasError = (state: AppState): boolean =>
    fromMissionCreation._getMissionCreationHasError(state.domain.missionCreation)

export const getMissionContactIsLoading = (state: AppState) =>
    fromMissionContact._getMissionContactIsLoading(state.domain.missionContact)
export const getMissionContactIsSent = (state: AppState) =>
    fromMissionContact._getMissionContactIsSent(state.domain.missionContact)
