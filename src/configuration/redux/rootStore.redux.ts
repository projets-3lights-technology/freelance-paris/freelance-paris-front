import { applyMiddleware, createStore, Middleware, Store, StoreEnhancer } from 'redux'
import { ActionsObservable, combineEpics, createEpicMiddleware, Epic, StateObservable } from 'redux-observable'
import { composeWithDevTools } from 'remote-redux-devtools'
import { BehaviorSubject, Subject } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { createAnalyticsMiddleware } from './analyticsMiddleware.redux'
import { AnalyticsDependencies } from './rootAnalyticsMiddleware.redux'
import { EpicsDependencies } from './rootEpicMiddleware.redux'
import { AppActionsType, RootReducer } from './rootReducer.redux'
import { AppState } from './rootState.redux'
import { navigationMiddleware } from './rootWithNavigationMiddleware.redux'

const reduxImmutableState = require('redux-immutable-state-invariant').default()

interface ReduxObservableParameters {
    epics: Array<Epic<AppActionsType, AppActionsType>>
    dependencies: EpicsDependencies
}

// todo: add AppActionsType
interface AnalyticsLoggerParameters {
    analytics: Array<(action: any, state: AppState, dependencies: AnalyticsDependencies) => void>
    dependencies: AnalyticsDependencies
}

interface MiddlewareParameters {
    reduxObservable: ReduxObservableParameters
    analyticsLogger: AnalyticsLoggerParameters
}

export class ReduxStore {

    private _epic$: Subject<Epic<AppActionsType, AppActionsType>> = new BehaviorSubject(combineEpics())

    configure(middlewareParameters: MiddlewareParameters,
              additionalMiddlewares: Middleware[] = [],
              enhancers: Array<StoreEnhancer<AppState>> = []): Store<AppState> {
        const middlewares = (__DEV__)
            ? [...additionalMiddlewares, /*reduxLogger,*/ reduxImmutableState]
            : [...additionalMiddlewares]

        const reduxObservableMiddleware = createEpicMiddleware(middlewareParameters.reduxObservable.dependencies)
        const analyticsMiddleware = createAnalyticsMiddleware({
            analytics   : middlewareParameters.analyticsLogger.analytics,
            dependencies: middlewareParameters.analyticsLogger.dependencies
        })

        const store = createStore<AppState, AppActionsType, any, any>(
            RootReducer,
            composeWithDevTools(
                applyMiddleware(
                    reduxObservableMiddleware,
                    navigationMiddleware,
                    analyticsMiddleware,
                    ...middlewares
                ),
                ...enhancers
            )
        )

        const rootEpic: any = (action$: ActionsObservable<AppActionsType>,
                               state$: StateObservable<AppState>,
                               dependencies: EpicsDependencies) =>
            this._epic$
                .pipe(
                    mergeMap(epic => epic(action$, state$, dependencies))
                )
        reduxObservableMiddleware.run(rootEpic)

        for (const epic of middlewareParameters.reduxObservable.epics)
            this.addEpic(epic)

        return store
    }

    addEpic(epic: Epic<AppActionsType, AppActionsType>): void {
        this._epic$.next(epic)
    }

}
