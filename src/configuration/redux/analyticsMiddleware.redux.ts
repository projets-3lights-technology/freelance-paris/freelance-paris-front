import { Action, AnyAction, Dispatch, Middleware, MiddlewareAPI } from 'redux'

interface Params<T extends Action, S = any, D = any> {
    analytics: Array<(action: any, state: S, dependencies?: D) => void>
    dependencies?: D
}

export const createAnalyticsMiddleware =
    <T extends Action, S = any, D = any>(params: Params<T, S, D>): Middleware => {
        return (store: MiddlewareAPI<Dispatch<AnyAction>, S>) => {
            return (next: Dispatch<AnyAction>) => {
                return (action: T): any => {
                    params.analytics.forEach(analytic => {
                        if (params.dependencies)
                            analytic(action, store.getState(), params.dependencies)
                        else
                            analytic(action, store.getState())
                    })

                    next(action)
                }
            }
        }
    }
