import { modulo } from 'ramda'
import { AsyncLocalStorage } from '../../../app/common/adapters/gateways/real/Async.localStorage'
import { ObservableHttpClient } from '../../../app/common/adapters/gateways/real/observable.httpClient'
import { InMemoryMissionsRepository } from '../../../app/missions/adapters/gateways/inmemory/inMemory.missionsRepository'
import { RESTMissionsRepository } from '../../../app/missions/adapters/gateways/real/REST.missionsRepository'
import { MissionContractType } from '../../../app/missions/domain/entities/missionContractType'
import { MissionDetails } from '../../../app/missions/domain/entities/missionDetails'
import { MissionDetailsBuilder } from '../../../app/missions/domain/entities/missionDetails.builder'
import { MissionHeader } from '../../../app/missions/domain/entities/missionHeader'
import { MissionHeaderBuilder } from '../../../app/missions/domain/entities/missionHeader.builder'
import { MissionsRepository } from '../../../app/missions/domain/MissionsRepository'
import { AppConfiguration, AppMode } from '../../environment/AppConfiguration'

const loremIpsum = require('lorem-ipsum')

export class MissionsRepositoryFactory {

    constructor(private appConfiguration: AppConfiguration) {
    }

    makeMissionsRepository(): MissionsRepository {
        switch (this.appConfiguration.get('MODE')) {
            case AppMode.IN_MEMORY:
                const missionNumber = 10
                const missionsHeader: MissionHeader[] = []
                const missionsDetails: MissionDetails[] = []

                for (let i = 1; i <= missionNumber; i++) {
                    const name = loremIpsum({
                        count             : 1,
                        units             : 'sentences',
                        sentenceUpperBound: 10
                    })
                    const description = loremIpsum({
                        count: 1,
                        units: 'paragraphs'
                    })
                    const customer = loremIpsum({
                        count             : 1,
                        units             : 'sentences',
                        sentenceLowerBound: 2,
                        sentenceUpperBound: 3
                    })
                    const missionHeader: MissionHeader = new MissionHeaderBuilder()
                        .withId(i.toString())
                        .withName(name)
                        .withDescription(description)
                        .withAverageDailyRate(i * 125)
                        .withAuthorId(Math.floor(Math.random() * 2).toString())
                        .build()
                    const missionDetails: MissionDetails = new MissionDetailsBuilder()
                        .withId(i.toString())
                        .withName(name)
                        .withDescription(description)
                        .withAverageDailyRate(i * 125)
                        .withCustomer(customer)
                        .withLocation('Place de l\'Hôtel-de-Ville 75196 Paris Cedex 04')
                        .withStartDate(new Date().toISOString())
                        .withDurationInMonths(i.toString())
                        .withRemoteIsPossible(Boolean(modulo(i, 2)))
                        .withContractType(MissionContractType.FREELANCE)
                        .withAlreadyContacted(i % 2 === 0)
                        .build()
                    missionsHeader.push(missionHeader)
                    missionsDetails.push(missionDetails)
                }
                return new InMemoryMissionsRepository(missionsHeader, missionsDetails)

            default:
                return new RESTMissionsRepository(new ObservableHttpClient(new AsyncLocalStorage()), this.appConfiguration)
        }
    }

}
