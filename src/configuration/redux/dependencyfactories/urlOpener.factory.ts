import { LinkingUrlOpener } from '../../../app/common/adapters/gateways/real/Linking.urlOpener'
import { UrlOpener } from '../../../app/common/domain/urlopener/UrlOpener'
import { AppConfiguration, AppMode } from '../../environment/AppConfiguration'

export class UrlOpenerFactory {

    constructor(private appConfiguration: AppConfiguration) {
    }

    makeUrlOpener(): UrlOpener {
        switch (this.appConfiguration.get('MODE')) {
            case AppMode.IN_MEMORY:
            default:
                return new LinkingUrlOpener()
        }
    }

}
