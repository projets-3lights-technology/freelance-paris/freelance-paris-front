import { AsyncLocalStorage } from '../../../app/common/adapters/gateways/real/Async.localStorage'
import { ObservableHttpClient } from '../../../app/common/adapters/gateways/real/observable.httpClient'
import { InMemoryMessageSender } from '../../../app/missions/adapters/gateways/inmemory/InMemory.messageSender'
import { RESTMessageSender } from '../../../app/missions/adapters/gateways/real/REST.messageSender'
import { MessageSender } from '../../../app/missions/domain/MessageSender'
import { AppConfiguration, AppMode } from '../../environment/AppConfiguration'

export class MessageSenderFactory {

    constructor(private appConfiguration: AppConfiguration) {
    }

    makeMessageSender(): MessageSender {
        switch (this.appConfiguration.get('MODE')) {
            case AppMode.IN_MEMORY:
                return new InMemoryMessageSender()

            default:
                return new RESTMessageSender(new ObservableHttpClient(new AsyncLocalStorage()), this.appConfiguration)
        }
    }

}
