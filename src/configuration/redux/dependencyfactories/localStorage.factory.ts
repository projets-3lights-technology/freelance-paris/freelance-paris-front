import { InMemoryLocalStorage } from '../../../app/common/adapters/gateways/inmemory/inMemory.localStorage'
import { AsyncLocalStorage } from '../../../app/common/adapters/gateways/real/Async.localStorage'
import { Token } from '../../../app/common/domain/entities/token'
import { LocalStorage } from '../../../app/common/domain/storage/LocalStorage'
import { AppConfiguration, AppMode } from '../../environment/AppConfiguration'

export class LocalStorageFactory {

    constructor(private appConfiguration: AppConfiguration) {
    }

    makeLocalStorage(): LocalStorage {
        switch (this.appConfiguration.get('MODE')) {
            case AppMode.IN_MEMORY:
                return new InMemoryLocalStorage(true, new Token('token', 86400))
            default:
                return new AsyncLocalStorage()
        }
    }

}
