import { AsyncLocalStorage } from '../../../app/common/adapters/gateways/real/Async.localStorage'
import { ObservableHttpClient } from '../../../app/common/adapters/gateways/real/observable.httpClient'
import { InMemoryMemberRepository } from '../../../app/member/adapters/gateways/inmemory/inMemory.memberRepository'
import { RESTMemberRepository } from '../../../app/member/adapters/gateways/real/REST.memberRepository'
import { Member } from '../../../app/member/domain/entities/member'
import { MemberBuilder } from '../../../app/member/domain/entities/member.builder'
import { MemberRepository } from '../../../app/member/domain/MemberRepository'
import { AppConfiguration, AppMode } from '../../environment/AppConfiguration'

export class MemberRepositoryFactory {

    constructor(private appConfiguration: AppConfiguration) {
    }

    makeMemberRepository(): MemberRepository {
        switch (this.appConfiguration.get('MODE')) {
            case AppMode.IN_MEMORY:
                const member: Member = new MemberBuilder()
                    .withId('1')
                    .withFirstName('First Name')
                    .withLastName('Last Name')
                    .withEmail('email@gmail.com')
                    .withAvatar('https://via.placeholder.com/150x150')
                    .build()
                return new InMemoryMemberRepository(member)

            default:
                return new RESTMemberRepository(new ObservableHttpClient(new AsyncLocalStorage()), this.appConfiguration)
        }
    }

}
