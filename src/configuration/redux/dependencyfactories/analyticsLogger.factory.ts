import { FirebaseAnalyticsLogger } from '../../../app/common/adapters/gateways/real/Firebase.analyticsLogger'
import { AnalyticsLogger } from '../../../app/common/domain/analyticslogger/AnalyticsLogger'
import { AppConfiguration, AppMode } from '../../environment/AppConfiguration'

export class AnalyticsLoggerFactory {

    constructor(private appConfiguration: AppConfiguration) {
    }

    makeAnalyticsLogger(): AnalyticsLogger {
        switch (this.appConfiguration.get('MODE')) {
            case AppMode.IN_MEMORY:
            default:
                return new FirebaseAnalyticsLogger()
        }
    }

}
