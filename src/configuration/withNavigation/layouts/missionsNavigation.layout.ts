import { createStackNavigator } from 'react-navigation'
import { MemberProfilePage } from '../../../ui/member'
import { MissionCreationPage } from '../../../ui/missions/missioncreation'
import { MissionDetailsPage } from '../../../ui/missions/missiondetails'
import { MissionsListingPage } from '../../../ui/missions/missionslisting'
import { AppPageName } from '../appPageName.navigation'

export const missionNavigationLayout = createStackNavigator(
    {
        main                          : {
            screen: MissionsListingPage
        },
        [AppPageName.MISSION_DETAILS] : {
            screen: MissionDetailsPage
        },
        [AppPageName.MISSION_CREATION]: {
            screen: MissionCreationPage
        },
        [AppPageName.MEMBER_PROFILE]  : {
            screen: MemberProfilePage
        }
    },
    {
        mode      : 'modal',
        headerMode: 'none'
    }
)
