import { createStackNavigator } from 'react-navigation'
import { LoginPage } from '../../../ui/login/index'

export const disconnectedAreaNavigationLayout = createStackNavigator(
    {
        main              : {
            screen: LoginPage
        }
    },
    {
        mode: 'modal',
        headerMode: 'none'
    }
)
