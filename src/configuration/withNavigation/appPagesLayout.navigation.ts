import { Dimensions } from 'react-native'
import { createBottomTabNavigator, createDrawerNavigator, createSwitchNavigator } from 'react-navigation'
import { SideMenu } from '../../ui/common/sidemenu'
import { AppPageName } from './appPageName.navigation'
import { disconnectedAreaNavigationLayout } from './layouts/loginNavigation.layout'
import { missionNavigationLayout } from './layouts/missionsNavigation.layout'

const width = Dimensions.get('window').width

const mainConnectedNavigationLayout = createBottomTabNavigator(
    {
        [AppPageName.MISSIONS]: missionNavigationLayout
    },
    {
        tabBarComponent: () => null
    }
)

const connectedAreaNavigationLayout = createDrawerNavigator(
    {
        main                   : {
            screen: mainConnectedNavigationLayout
        },
        [AppPageName.SIDE_MENU]: {
            screen: SideMenu
        }
    },
    {
        drawerWidth     : width - 50,
        // @ts-ignore
        contentComponent: SideMenu
    }
)

export const AppNavigationLayout = createSwitchNavigator(
    {
        [AppPageName.LOGIN]    : disconnectedAreaNavigationLayout,
        [AppPageName.MAIN_PAGE]: connectedAreaNavigationLayout
    }
)
