export enum AppPageName {
    LOGIN = 'Login',
    MAIN_PAGE = 'Main page',
    MISSIONS = 'Missions',
    MISSION_DETAILS = 'Mission Details',
    MISSION_CREATION = 'Mission creation',
    SIDE_MENU = 'Side menu',
    MEMBER_PROFILE = 'Member profile'
}
