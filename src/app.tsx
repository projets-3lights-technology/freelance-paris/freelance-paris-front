import React, { Component } from 'react'
import { StatusBar } from 'react-native'
import SplashScreen from 'react-native-splash-screen'
import { Provider } from 'react-redux'
import { appAnalytics } from './app/common/usecases/analytics/app.analytics'
import * as gatekeeperActions from './app/common/usecases/gatekeeper/gatekeeper.actions'
import * as navigationActions from './app/common/usecases/navigation/navigation.actions'
import { appAnalyticsDependencies } from './configuration/redux/rootAnalyticsMiddleware.redux'
import { appEpics, appEpicsDependencies } from './configuration/redux/rootEpicMiddleware.redux'
import { ReduxStore } from './configuration/redux/rootStore.redux'
import { WithNavigation } from './configuration/redux/rootWithNavigationMiddleware.redux'
import { LaunchScreen } from './ui/common/launchscreen/LaunchScreen'

type State = {
    isLoaded: boolean
}

const store = new ReduxStore()
    .configure({
        reduxObservable: { epics: appEpics, dependencies: appEpicsDependencies },
        analyticsLogger: { analytics: appAnalytics, dependencies: appAnalyticsDependencies }
    })

export class App extends Component<any, State> {

    state = {
        isLoaded: false
    }

    componentDidMount(): void {
        store.dispatch(navigationActions.Actions.startApplication())
        store.dispatch(gatekeeperActions.Actions.autoLogin())
        setTimeout(() => this.setState({ isLoaded: true }, () => SplashScreen.hide()), 50)
    }

    render() {
        const content = this.state.isLoaded ? <WithNavigation/> : <LaunchScreen/>
        return (
            <Provider store={store}>
                <React.Fragment>
                    <StatusBar barStyle={'light-content'}/>
                    {content}
                </React.Fragment>
            </Provider>
        )
    }

}
