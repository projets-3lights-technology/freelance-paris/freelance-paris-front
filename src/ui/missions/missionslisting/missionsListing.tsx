import React, { Component } from 'react'
import { FlatList, StyleSheet, ViewStyle } from 'react-native'
import { ApplicationHeader } from '../../common/applicationheader'
import { Loading } from '../../common/loading/loading'
import { MissionHeaderVM } from '../viewmodels/missionHeader.viewmodel'
import { Props } from './index'
import { MissionsListingItem } from './item'
import { EmptyMissionsList } from './item/EmptyMissionsList'

export class MissionsListingContainer extends Component<Props> {

    componentDidMount(): void {
        this.props.fetchAllMissions()
    }

    render() {
        const content = this.props.isLoading || this.props.currentMember.isEmpty()
            ? <Loading/>
            : <FlatList
                data={this.props.missions}
                renderItem={this.renderItem}
                ListEmptyComponent={this.renderEmptyList}
                keyExtractor={this.keyExtractor}
                contentContainerStyle={styles.container}
                onRefresh={this.props.refreshAllMissions}
                refreshing={this.props.isRefreshing}
            />

        return (
            <React.Fragment>
                <ApplicationHeader title={this.props.capitalize('missions')}/>
                {content}
            </React.Fragment>
        )
    }

    private renderItem = ({ item }: { item: MissionHeaderVM }) =>
        <MissionsListingItem
            mission={item}
            currentMemberId={this.props.currentMember.get().id}
        />

    private renderEmptyList = () => <EmptyMissionsList message={this.props.t('empty_mission')}/>

    private keyExtractor = (item: MissionHeaderVM) => item.id

}

type Styles = {
    container: ViewStyle
}
const styles = StyleSheet.create<Styles>({
    container: {
        paddingTop   : 10,
        paddingBottom: 20
    }
})
