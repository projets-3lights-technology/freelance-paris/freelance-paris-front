import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { Optional } from 'typescript-optional'
import { I18nKeys } from '../../../app/common/domain/translations/i18.keys'
import * as allMissionsActions from '../../../app/missions/usecases/missionslisting/missionsListing.actions'
import {
    capitalize,
    getMemberInformation,
    getMissionsListing,
    getMissionsListingIsLoading,
    getMissionsListingIsRefreshing,
    t
} from '../../../configuration/redux/rootReducer.redux'
import { AppState } from '../../../configuration/redux/rootState.redux'
import { MemberPresenter } from '../../login/presenters/member.present'
import { MemberVM } from '../../login/viewmodels/member.viewmodel'
import { MissionsListingPresenter } from '../presenters/missionsListing.presenter'
import { MissionHeaderVM } from '../viewmodels/missionHeader.viewmodel'
import { MissionsListingContainer } from './missionsListing'

type StateProps = {
    missions: MissionHeaderVM[]
    isLoading: boolean
    isRefreshing: boolean
    t: (key: I18nKeys) => string
    capitalize: (key: I18nKeys) => string
    currentMember: Optional<MemberVM>
}
type DispatchProps = {
    fetchAllMissions: () => void
    refreshAllMissions: () => void
}
type OwnProps = {}
export type Props = StateProps & DispatchProps & OwnProps

const mapStateToProps = (state: AppState): StateProps => ({
    missions     : MissionsListingPresenter.present(getMissionsListing(state)),
    currentMember: MemberPresenter.present(getMemberInformation(state)),
    t            : (key: I18nKeys) => t(state)(key),
    capitalize   : (key: I18nKeys) => capitalize(state)(key),
    isLoading    : getMissionsListingIsLoading(state),
    isRefreshing : getMissionsListingIsRefreshing(state)
})

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => bindActionCreators(
    {
        fetchAllMissions  : allMissionsActions.Actions.fetchAllMissions,
        refreshAllMissions: allMissionsActions.Actions.refreshAllMissions
    },
    dispatch
)

export const MissionsListingPage = connect<StateProps, DispatchProps, OwnProps, AppState>(
    mapStateToProps,
    mapDispatchToProps
)(MissionsListingContainer)
