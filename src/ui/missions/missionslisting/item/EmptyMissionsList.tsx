import { Text } from 'native-base'
import React, { FunctionComponent } from 'react'
import { StyleSheet, TextStyle, View, ViewStyle } from 'react-native'
import { AppTheme } from '../../../../assets/appTheme'

type Props = {
    message: string
}

export const EmptyMissionsList: FunctionComponent<Props> = ({ ...props }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>{props.message}</Text>
        </View>
    )
}

type Style = {
    container: ViewStyle
    text: TextStyle
}
const styles = StyleSheet.create<Style>({
    container: {
        alignItems       : 'center',
        marginTop        : 50,
        paddingHorizontal: 50
    },
    text     : {
        color    : AppTheme.spindle,
        textAlign: 'center'
    }
})
