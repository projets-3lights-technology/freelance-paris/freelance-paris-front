import React, { PureComponent } from 'react'
import { MissionsListingItemCard } from './elements/missionsListingItemCard'
import { Props } from './index'

export class MissionsListingItemContainer extends PureComponent<Props> {
    render() {
        return (
            <MissionsListingItemCard
                mission={this.props.mission}
                onPress={this.onPressCard}
                onShowPopover={this.showPopover}
                isTheAuthor={this.props.mission.isPublishedBy(this.props.currentMemberId)}
                isPopoverVisible={this.props.isMissionActionsOpen(this.props.mission.id)}
                onRemove={this.onRemove}
                onCancel={this.props.quitMissionActions}
                capitalize={this.props.capitalize}
            />
        )
    }

    private showPopover = () => this.props.openMissionActions(this.props.mission.id)
    private onPressCard = () => this.props.openMissionDetails(this.props.mission.id)
    private onRemove = () => this.props.removeMission(this.props.mission.id)
}
