import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { I18nKeys } from '../../../../app/common/domain/translations/i18.keys'
import * as navigationActions from '../../../../app/common/usecases/navigation/navigation.actions'
import * as allMissionsActions from '../../../../app/missions/usecases/missionslisting/missionsListing.actions'
import { capitalize, isMissionActionsOpen } from '../../../../configuration/redux/rootReducer.redux'
import { AppState } from '../../../../configuration/redux/rootState.redux'
import { MissionHeaderVM } from '../../viewmodels/missionHeader.viewmodel'
import { MissionsListingItemContainer } from './missionsListingItem'

type StateProps = {
    isMissionActionsOpen?: (id: string) => boolean
    capitalize: (key: I18nKeys) => string
}
type DispatchProps = {
    openMissionDetails?: (id: string) => void
    removeMission?: (id: string) => void
    openMissionActions?: (id: string) => void
    quitMissionActions?: () => void
}
type OwnProps = {
    mission: MissionHeaderVM
    currentMemberId: string
}
export type Props = StateProps & DispatchProps & OwnProps

const mapStateToProps = (state: AppState): StateProps => ({
    isMissionActionsOpen: isMissionActionsOpen(state),
    capitalize          : (key: I18nKeys) => capitalize(state)(key)
})

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => bindActionCreators(
    {
        openMissionDetails: navigationActions.Actions.openMissionDetails,
        removeMission     : allMissionsActions.Actions.removeMission,
        openMissionActions: navigationActions.Actions.openMissionActions,
        quitMissionActions: navigationActions.Actions.quitMissionActions
    },
    dispatch
)

export const MissionsListingItem = connect<StateProps, DispatchProps, OwnProps, AppState>(
    mapStateToProps,
    mapDispatchToProps
)(MissionsListingItemContainer)
