import React, { Component } from 'react'
import { StyleSheet, TouchableOpacity, ViewStyle } from 'react-native'
import IconFa from 'react-native-vector-icons/FontAwesome'
import { I18nKeys } from '../../../../../app/common/domain/translations/i18.keys'

type Props = {
    isVisible: boolean
    onPress: () => void
    onRemove: () => void
    onCancel: () => void
    capitalize: (key: I18nKeys) => string
}

export class MissionsListingItemPopover extends Component<Props> {

    render() {
        return (
            <React.Fragment>
                <TouchableOpacity
                    //ref={ref => this.ref = ref}
                    style={styles.missionDeleteIcon}
                    onPress={this.props.onRemove}
                >
                    <IconFa
                        name={'trash'}
                        color={'rgba(100,100,100,0.8)'}
                        size={22}
                    />
                </TouchableOpacity>
            </React.Fragment>
        )
    }

    /*
        todo the first intention is to have a popover menu like LinkedIn for actions
        private ref: React.Component
        <RNPopover
            visible={this.props.isVisible}
            reference={this.ref}
            onDone={this.props.onRemove}
            onCancel={this.props.onCancel}
            tintColor={'#222222'}
            textColor={'#FFFFFF'}
            roundedArrow={false}
        >
            <RNPopover.Menu>
                <RNPopover.Menu label={this.props.capitalize('remove')}/>
            </RNPopover.Menu>
        </RNPopover>
     */
}

type Styles = {
    missionDeleteIcon: ViewStyle
}
const styles = StyleSheet.create<Styles>({
    missionDeleteIcon: {
        width      : 24,
        alignItems : 'center',
        marginRight: -4
    }
})
