import { Badge, Col, Grid, H3, Text } from 'native-base'
import React, { FunctionComponent } from 'react'
import { StyleSheet, TextStyle, TouchableOpacity, View, ViewStyle } from 'react-native'
import IconIc from 'react-native-vector-icons/Ionicons'
import { I18nKeys } from '../../../../../app/common/domain/translations/i18.keys'
import { AppTheme } from '../../../../../assets/appTheme'
import { MissionHeaderVM } from '../../../viewmodels/missionHeader.viewmodel'
import { MissionsListingItemPopover } from './missionsListingItemPopover'

type MissionActionsProps = {
    isTheAuthor: boolean
    isPopoverVisible: boolean
    onShowPopover: () => void
    onRemove: () => void
    onCancel: () => void
    capitalize: (key: I18nKeys) => string
}

const MissionActionsIcon: FunctionComponent<MissionActionsProps> = ({ ...props }) => {
    if (props.isTheAuthor)
        return (<Col style={styles.missionDelete}>
            <MissionsListingItemPopover
                isVisible={props.isPopoverVisible}
                onPress={props.onShowPopover}
                onRemove={props.onRemove}
                onCancel={props.onCancel}
                capitalize={props.capitalize}
            />
        </Col>)
    return <Col/>
}

type CardProps = {
    mission: MissionHeaderVM
    onPress: () => void
}

export const MissionsListingItemCard: FunctionComponent<CardProps & MissionActionsProps> = ({ ...props }) => {
    const authorBadge = props.isTheAuthor
        ? <Badge style={styles.badgeAuthor}><Text style={styles.badgeAuthorText}>{props.capitalize('author')}</Text></Badge>
        : <Text/>

    return (
        <TouchableOpacity
            key={props.mission.id}
            style={styles.container}
            activeOpacity={0.8}
            onPress={props.onPress}
        >
            <View>
                <View style={styles.missionHeaderPrice}>
                    {authorBadge}
                    <Text style={styles.missionPrice}>
                        {props.mission.averageDailyRate} <IconIc name="logo-euro" size={16}/>
                    </Text>
                </View>
                <Grid>
                    <Col size={11}>
                        <H3 style={styles.missionName} numberOfLines={2}>{props.mission.name}</H3>
                    </Col>
                    <MissionActionsIcon
                        isTheAuthor={props.isTheAuthor}
                        isPopoverVisible={props.isPopoverVisible}
                        onShowPopover={props.onShowPopover}
                        onRemove={props.onRemove}
                        onCancel={props.onCancel}
                        capitalize={props.capitalize}
                    />
                </Grid>
            </View>
            <Text style={styles.missionDescription} numberOfLines={5}>
                {props.mission.description}
            </Text>
        </TouchableOpacity>
    )
}

type Styles = {
    container: ViewStyle
    missionHeaderPrice: ViewStyle
    badgeAuthor: ViewStyle
    badgeAuthorText: TextStyle
    missionPrice: TextStyle
    missionName: TextStyle
    missionDescription: TextStyle
    missionDelete: ViewStyle
}
const styles = StyleSheet.create<Styles>({
    container         : {
        backgroundColor: 'white',
        padding        : 20,
        marginVertical : 5
    },
    missionHeaderPrice: {
        display       : 'flex',
        flexDirection : 'row',
        justifyContent: 'space-between',
        alignItems    : 'center',
        marginBottom  : 10
    },
    badgeAuthor       : {
        backgroundColor: AppTheme.spindle
    },
    badgeAuthorText   : {
        fontSize: 12
    },
    missionPrice      : {
        fontWeight : 'bold',
        fontSize   : 16
    },
    missionName       : {
        color     : AppTheme.mulledWine,
        fontWeight: 'bold'
    },
    missionDescription: {
        marginTop: 20,
        color    : 'rgba(0, 0, 0, 0.5)'
    },
    missionDelete     : {
        alignItems: 'flex-end'
    }
})
