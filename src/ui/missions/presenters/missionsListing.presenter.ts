import { MissionsHeadersById } from '../../../app/missions/domain/MissionsState.redux'
import { MissionHeaderVM } from '../viewmodels/missionHeader.viewmodel'
import { MissionHeaderVMBuilder } from '../viewmodels/missionHeaderVM.builder'

export class MissionsListingPresenter {

    static present(missions: MissionsHeadersById[]): MissionHeaderVM[] {
        return missions.map(mission => new MissionHeaderVMBuilder()
            .withId(mission.id)
            .withName(mission.name)
            .withDescription(mission.description)
            .withAverageDailyRate(mission.averageDailyRate)
            .withAuthorId(mission.authorId)
            .build()
        )
    }

}
