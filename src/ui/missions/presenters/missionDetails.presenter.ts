import { Optional } from 'typescript-optional'
import { ContractTypeTo } from '../../../app/missions/domain/entities/missionContractType'
import { MissionDetailsById } from '../../../app/missions/domain/MissionsState.redux'
import { MissionDetailsVM } from '../viewmodels/missionDetails.viewmodel'
import { MissionDetailsVMBuilder } from '../viewmodels/missionDetailsVM.builder'

export class MissionDetailsPresenter {

    static present(missionDetails: Optional<MissionDetailsById>): Optional<MissionDetailsVM> {
        return missionDetails.map(mission => new MissionDetailsVMBuilder()
            .withId(mission.id)
            .withName(mission.name)
            .withDescription(mission.description)
            .withAverageDailyRate(mission.averageDailyRate)
            .withCustomer(mission.customer)
            .withLocation(mission.location)
            .withStartDate(mission.startDate)
            .withDurationInMonths(mission.durationInMonths)
            .withRemoteIsPossible(mission.remoteIsPossible)
            .withContractType(ContractTypeTo.string(mission.contractType))
            .withAlreadyContacted(mission.alreadyContacted)
            .build()
        )
    }

}
