import { H2 } from 'native-base'
import React, { PureComponent } from 'react'
import { BackHandler, StyleSheet, TextStyle, View, ViewStyle } from 'react-native'
import Modal from 'react-native-modal'
import { Props } from '.'
import { Message } from '../../../../app/missions/domain/entities/message'
import { MessageBuilder } from '../../../../app/missions/domain/entities/message.builder'
import { MessageContactForm } from './messageContactForm'
import { MissionContactSuccessful } from './missionContactSuccessful'

type State = {
    object: string
    message: string
    isSent: boolean
}

export class MissionContact extends PureComponent<Props, State> {

    constructor(props: Props) {
        super(props)

        this.state = {
            object : 'object',
            message: '',
            isSent : false
        }
    }

    componentDidMount(): void {
        BackHandler.addEventListener('hardwareBackPress', this.props.quitMissionContact)
    }

    render() {
        const content = this.state.isSent
            ? <MissionContactSuccessful
                t={this.props.t}
                capitalize={this.props.capitalize}
                onClose={this.handleCloseModal}
            />
            : <MessageContactForm
                capitalize={this.props.capitalize}
                onChangeObject={this.onChangeName}
                onChangeMessage={this.onChangeMessage}
                onSubmit={this.submitForm}
                onCancel={this.handleCloseModal}
            />

        return (
            <Modal
                isVisible={this.props.isOpen}
                animationIn="fadeIn"
                animationOut="fadeOut"
                backdropColor="black"
                onBackdropPress={this.handleCloseModal}
                onBackButtonPress={this.handleCloseModal}
                avoidKeyboard={true}
            >
                <View style={styles.container}>
                    <H2 style={styles.title}>{this.props.capitalize('contact')}</H2>
                    {content}
                </View>
            </Modal>
        )
    }

    private onChangeName = (value: string) => this.setState({ object: value })
    private onChangeMessage = (value: string) => this.setState({ message: value })

    private submitForm = () => {
        const message: Message = new MessageBuilder()
            .withMissionId(this.props.missionDetailsId)
            .withSenderId(this.props.member.get().id)
            .withObject(this.state.object)
            .withMessage(this.state.message)
            .build()
        this.props.contact(message)
        this.setState({ isSent: true })
    }

    private handleCloseModal = () => {
        this.props.quitMissionContact()
        setTimeout(() => this.setState({ isSent: false }), 100)
        BackHandler.removeEventListener('hardwareBackPress', this.props.quitMissionContact)
    }

}

type Styles = {
    container: ViewStyle
    title: TextStyle
}
const styles = StyleSheet.create<Styles>({
    container: {
        backgroundColor: 'white',
        borderRadius   : 30,
        padding        : 20,
        paddingBottom  : 50
    },
    title    : {
        marginBottom: 20,
        textAlign   : 'center'
    }
})
