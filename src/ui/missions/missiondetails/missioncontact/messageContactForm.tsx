import { Button, Col, Form, Grid, Icon, Item, Label, Textarea } from 'native-base'
import React, { FunctionComponent } from 'react'
import { StyleSheet, Text, TextStyle, ViewStyle } from 'react-native'
import { I18nKeys } from '../../../../app/common/domain/translations/i18.keys'

type Props = {
    capitalize: (key: I18nKeys) => string
    onChangeObject: (value: string) => void
    onChangeMessage: (value: string) => void
    onSubmit: () => void
    onCancel: () => void
}

export const MessageContactForm: FunctionComponent<Props> = ({ ...props }) => {
    return (
        <Form>
            <Item stackedLabel={true} style={styles.item}>
                <Label>{props.capitalize('message')}</Label>
                <Textarea
                    style={styles.textArea}
                    rowSpan={5}
                    onChangeText={props.onChangeMessage}
                    allowFontScaling={true}
                    bordered={false}
                />
            </Item>

            <Grid style={styles.buttonsGroup}>
                <Col size={5}>
                    <Button onPress={props.onCancel} light={true} style={styles.button}>
                        <Text>{props.capitalize('cancel')}</Text>
                    </Button>
                </Col>
                <Col size={1}/>
                <Col size={5}>
                    <Button iconRight={true} onPress={props.onSubmit} style={styles.button}>
                        <Text style={styles.sendButton}>{props.capitalize('send')}</Text>
                        <Icon name="ios-send"/>
                    </Button>
                </Col>
            </Grid>
        </Form>
    )
}

type Styles = {
    item: ViewStyle
    textArea: TextStyle
    buttonsGroup: ViewStyle
    button: ViewStyle
    sendButton: TextStyle
}
const styles = StyleSheet.create<Styles>({
    item        : {
        marginBottom     : 15,
        marginLeft       : 0,
        borderBottomWidth: 0.5,
        borderBottomColor: 'rgb(200, 200, 200)'
    },
    textArea    : {
        width         : '100%',
        marginVertical: 10,
        marginLeft    : -20,
        fontSize      : 18
    },
    buttonsGroup: {
        marginBottom: 20
    },
    button      : {
        width      : '100%',
        paddingLeft: 10
    },
    sendButton  : {
        color: 'white'
    }
})
