import { Button, Icon, Text } from 'native-base'
import React, { FunctionComponent } from 'react'
import { StyleSheet, TextStyle, View, ViewStyle } from 'react-native'
import { I18nKeys } from '../../../../app/common/domain/translations/i18.keys'

type Props = {
    t: (key: I18nKeys) => string
    capitalize: (key: I18nKeys) => string
    onClose: () => void
}

export const MissionContactSuccessful: FunctionComponent<Props> = ({ ...props }) => {
    return (
        <View style={styles.container}>
            <Icon style={styles.icon} type={'FontAwesome'} name="check-circle-o"/>
            <Text style={styles.text}>{props.t('contact_successful')}</Text>
            <Button onPress={props.onClose} light={true} style={styles.button}>
                <Text>{props.capitalize('close')}</Text>
            </Button>
        </View>
    )
}

type Styles = {
    container: ViewStyle
    icon: TextStyle
    text: TextStyle
    button: ViewStyle
}
const styles = StyleSheet.create<Styles>({
    container: {
        alignItems: 'center'
    },
    icon     : {
        marginTop: 10,
        fontSize : 60,
        color    : 'green'
    },
    text     : {
        marginTop: 30,
        textAlign: 'center'
    },
    button   : {
        alignSelf: 'center',
        marginTop: 30
    }
})
