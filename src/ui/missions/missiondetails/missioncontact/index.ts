import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { Optional } from 'typescript-optional'
import { I18nKeys } from '../../../../app/common/domain/translations/i18.keys'
import * as navigationActions from '../../../../app/common/usecases/navigation/navigation.actions'
import { Message } from '../../../../app/missions/domain/entities/message'
import * as missionContactActions from '../../../../app/missions/usecases/missioncontact/missionContact.actions'
import {
    capitalize,
    getMemberInformation,
    getMissionDetailsId,
    isMissionContactOpen,
    t
} from '../../../../configuration/redux/rootReducer.redux'
import { AppState } from '../../../../configuration/redux/rootState.redux'
import { MemberPresenter } from '../../../login/presenters/member.present'
import { MemberVM } from '../../../login/viewmodels/member.viewmodel'
import { MissionContact } from './missionContact'

type StateProps = {
    isOpen: boolean
    capitalize: (key: I18nKeys) => string
    t: (key: I18nKeys) => string
    missionDetailsId: string
    member: Optional<MemberVM>
}
type DispatchProps = {
    contact: (message: Message) => void
    quitMissionContact: () => void
}
type OwnProps = {}
export type Props = StateProps & DispatchProps & OwnProps

const mapStateToProps = (state: AppState): StateProps => ({
    isOpen          : isMissionContactOpen(state),
    capitalize      : (key: I18nKeys) => capitalize(state)(key),
    t               : (key: I18nKeys) => t(state)(key),
    missionDetailsId: getMissionDetailsId(state),
    member          : MemberPresenter.present(getMemberInformation(state))
})

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => bindActionCreators(
    {
        contact           : missionContactActions.Actions.sendMissionContact,
        quitMissionContact: navigationActions.Actions.quitMissionContact
    },
    dispatch
)

export const MissionContactModal = connect<StateProps, DispatchProps, OwnProps, AppState>(
    mapStateToProps,
    mapDispatchToProps
)(MissionContact)
