import { Body, Button, Col, Grid, H1, Left, ListItem, Row } from 'native-base'
import React, { FunctionComponent } from 'react'
import { ScrollView, StyleSheet, Text, TextStyle, View, ViewStyle } from 'react-native'
import IconFA from 'react-native-vector-icons/FontAwesome'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { I18nKeys } from '../../../app/common/domain/translations/i18.keys'
import { AppTheme } from '../../../assets/appTheme'
import { MissionDetailsVM } from '../viewmodels/missionDetails.viewmodel'

type Props = {
    mission: MissionDetailsVM
    openMissionContact: () => void
    t: (key: I18nKeys) => string
    capitalize: (key: I18nKeys) => string
    canDisplayContact: boolean
}

export const MissionDetailsCard: FunctionComponent<Props> = ({ ...props }) => {
    const remote = props.mission.remoteIsPossible
        ? <ListItem icon={true} noIndent={true} noBorder={true}>
            <Left style={styles.iconBox}>
                <IconFA name="laptop" size={27} color={'rgba(0, 0, 0, 0.2)'}/>
            </Left>
            <Body>
                <Text>{props.capitalize('remote_available')}</Text>
            </Body>
        </ListItem>
        : <ListItem icon={true} noIndent={true} noBorder={true}>
            <Left style={styles.iconBox}>
                <IconFA name="desktop" size={27} color={'rgba(0, 0, 0, 0.2)'}/>
            </Left>
            <Body>
                <Text>{props.capitalize('remote_unavailable')}</Text>
            </Body>
        </ListItem>

    return (
        <ScrollView style={styles.container}>
            <H1 style={styles.missionName}>{props.mission.name}</H1>

            {props.canDisplayContact && <React.Fragment>
                <View style={styles.divider}/>

                {props.mission.alreadyContacted && (<Text style={styles.isContacted}>{props.t('already_contacted')}</Text>)}

                <Button block={true} onPress={props.openMissionContact}>
                    <Text style={styles.chatButton}>{props.capitalize('contact')}</Text>
                </Button>

            </React.Fragment>}

            <View style={styles.divider}/>

            <Grid style={{ marginLeft: -20 }}>
                <Row>
                    <Col size={55}>
                        <ListItem icon={true} noIndent={true} noBorder={true}>
                            <Left style={styles.iconBox}>
                                <Ionicons name="logo-euro" size={25} color={'rgba(0, 0, 0, 0.2)'}/>
                            </Left>
                            <Body>
                                <Text style={styles.missionInfo}>
                                    {props.mission.averageDailyRate} ({props.t('indicative')})
                                </Text>
                            </Body>
                        </ListItem>
                    </Col>
                    <Col size={45}>
                        {remote}
                    </Col>
                </Row>
                <Row style={styles.centralRow}>
                    <Col size={55}>
                        <ListItem icon={true} noIndent={true} noBorder={true}>
                            <Left style={styles.iconBox}>
                                <Ionicons name="md-time" size={27} color={'rgba(0, 0, 0, 0.2)'}/>
                            </Left>
                            <Body>
                                <Text>{props.capitalize('start')} {props.mission.startDate}</Text>
                                <Text>{props.capitalize('duration')} {props.mission.durationInMonths} {props.t('months')}</Text>
                            </Body>
                        </ListItem>
                    </Col>
                    <Col size={45}>
                        <ListItem icon={true} noIndent={true} noBorder={true}>
                            <Left style={styles.iconBox}>
                                <IconFA name="file-text" size={27} color={'rgba(0, 0, 0, 0.2)'}/>
                            </Left>
                            <Body>
                                <Text>
                                    {props.capitalize(props.mission.contractType as I18nKeys)}
                                </Text>
                            </Body>
                        </ListItem>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <ListItem icon={true} noIndent={true} noBorder={true}>
                            <Left style={styles.iconBox}>
                                <IconFA name="map-marker" size={28} color={'rgba(0, 0, 0, 0.2)'}/>
                            </Left>
                            <Body>
                                <Text style={styles.missionInfo}>{props.mission.customer}</Text>
                                <Text>{props.mission.location}</Text>
                            </Body>
                        </ListItem>
                    </Col>
                </Row>
            </Grid>

            <Text style={styles.missionDescription}>{props.mission.description}</Text>
        </ScrollView>
    )
}

type Styles = {
    container: ViewStyle
    missionName: TextStyle
    divider: ViewStyle
    isContacted: TextStyle
    chatButton: TextStyle
    iconBox: ViewStyle
    missionInfo: TextStyle
    customerInformation: ViewStyle
    centralRow: ViewStyle
    missionDescription: ViewStyle
}
const styles = StyleSheet.create<Styles>({
    container          : {
        flex             : 1,
        paddingHorizontal: 15,
        backgroundColor  : 'white'
    },
    missionName        : {
        marginTop : 15,
        fontWeight: 'bold',
        color     : AppTheme.mulledWine
    },
    divider            : {
        height         : 1,
        width          : '100%',
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
        marginTop      : 20,
        marginBottom   : 20
    },
    isContacted        : {
        textAlign   : 'center',
        marginBottom: 10
    },
    chatButton         : {
        color   : 'white',
        fontSize: 20
    },
    iconBox            : {
        width: 45
    },
    missionInfo        : {
        fontWeight: 'bold'
    },
    customerInformation: {
        marginVertical: 5
    },
    centralRow         : {
        marginVertical: 10
    },
    missionDescription : {
        marginTop    : 20,
        paddingBottom: 20
    }
})
