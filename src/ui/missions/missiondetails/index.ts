import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { Optional } from 'typescript-optional'
import { I18nKeys } from '../../../app/common/domain/translations/i18.keys'
import * as navigationActions from '../../../app/common/usecases/navigation/navigation.actions'
import * as missionDetailsActions from '../../../app/missions/usecases/missiondetails/missionDetails.actions'
import {
    capitalize,
    getMissionDetails,
    getMissionDetailsId,
    getMissionsDetailsIsLoading,
    isAuthenticated,
    t
} from '../../../configuration/redux/rootReducer.redux'
import { AppState } from '../../../configuration/redux/rootState.redux'
import { MissionDetailsPresenter } from '../presenters/missionDetails.presenter'
import { MissionDetailsVM } from '../viewmodels/missionDetails.viewmodel'
import { MissionDetails } from './missionDetails'

type StateProps = {
    id: string
    mission: Optional<MissionDetailsVM>
    isLoading: boolean
    t: (key: I18nKeys) => string
    capitalize: (key: I18nKeys) => string
    isAuthenticated: boolean
}
type DispatchProps = {
    quitMissionDetails: () => void
    fetchMissionDetails: (id: string) => void
    abortMissionDetailsFetching: () => void
    openMissionContact: () => void
}
type OwnProps = {}
export type Props = StateProps & DispatchProps & OwnProps

const mapStateToProps = (state: AppState): StateProps => ({
    id             : getMissionDetailsId(state),
    mission        : MissionDetailsPresenter.present(getMissionDetails(state)),
    isLoading      : getMissionsDetailsIsLoading(state),
    t              : (key: I18nKeys) => t(state)(key),
    capitalize     : (key: I18nKeys) => capitalize(state)(key),
    isAuthenticated: isAuthenticated(state)
})

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => bindActionCreators(
    {
        fetchMissionDetails        : missionDetailsActions.Actions.fetchMissionDetails,
        abortMissionDetailsFetching: missionDetailsActions.Actions.abortMissionDetailsFetching,
        quitMissionDetails         : navigationActions.Actions.quitMissionDetails,
        openMissionContact         : navigationActions.Actions.openMissionContact
    },
    dispatch
)

export const MissionDetailsPage = connect<StateProps, DispatchProps, OwnProps, AppState>(
    mapStateToProps,
    mapDispatchToProps
)(MissionDetails)
