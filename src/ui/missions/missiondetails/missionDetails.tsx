import React, { PureComponent } from 'react'
import { BackHandler } from 'react-native'
import { Loading } from '../../common/loading/loading'
import { ModalHeader } from '../../common/modalheader/modalHeader'
import { Props } from './index'
import { MissionContactModal } from './missioncontact'
import { MissionDetailsCard } from './missionDetailsCard'

export class MissionDetails extends PureComponent<Props> {

    componentDidMount(): void {
        this.props.fetchMissionDetails(this.props.id)
        BackHandler.addEventListener('hardwareBackPress', this.props.quitMissionDetails)
    }

    componentWillUnmount(): void {
        this.props.abortMissionDetailsFetching()
        BackHandler.removeEventListener('hardwareBackPress', this.props.quitMissionDetails)
    }

    render() {
        const content = (this.props.isLoading || this.props.mission.isEmpty())
            ? <Loading/>
            : <MissionDetailsCard
                mission={this.props.mission.get()}
                openMissionContact={this.props.openMissionContact}
                t={this.props.t}
                capitalize={this.props.capitalize}
                canDisplayContact={this.props.isAuthenticated}
            />

        return (
            <React.Fragment>
                <ModalHeader leftIcon={'close-circle-outline'} onLeftClick={this.props.quitMissionDetails}/>
                {content}
                <MissionContactModal/>
            </React.Fragment>
        )
    }

}
