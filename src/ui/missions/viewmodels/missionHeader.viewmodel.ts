export class MissionHeaderVM {

    constructor(private _id: string,
                private _name: string,
                private _description: string,
                private _averageDailyRate: number,
                private _authorId: string) {
    }

    get id(): string {
        return this._id
    }

    get name(): string {
        return this._name
    }

    get description(): string {
        return this._description
    }

    get averageDailyRate(): number {
        return this._averageDailyRate
    }

    isPublishedBy(authorId: string): boolean {
        return this._authorId === authorId
    }
}
