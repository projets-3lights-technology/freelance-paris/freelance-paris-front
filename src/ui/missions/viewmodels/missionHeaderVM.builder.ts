import { MissionHeaderVM } from './missionHeader.viewmodel'

export class MissionHeaderVMBuilder {

    protected _id: string = ''
    protected _name: string = ''
    protected _description: string = ''
    protected _averageDailyRate: number = 0
    protected _authorId: string = ''

    withId(value: string): MissionHeaderVMBuilder {
        this._id = value
        return this
    }

    withName(value: string): MissionHeaderVMBuilder {
        this._name = value
        return this
    }

    withDescription(value: string): MissionHeaderVMBuilder {
        this._description = value
        return this
    }

    withAverageDailyRate(value: number): MissionHeaderVMBuilder {
        this._averageDailyRate = value
        return this
    }

    withAuthorId(value: string): MissionHeaderVMBuilder {
        this._authorId = value
        return this
    }

    build(): MissionHeaderVM {
        return new MissionHeaderVM(
            this._id,
            this._name,
            this._description,
            this._averageDailyRate,
            this._authorId
        )
    }

}
