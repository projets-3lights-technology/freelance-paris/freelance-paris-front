import { MissionDetailsVM } from './missionDetails.viewmodel'

export class MissionDetailsVMBuilder {

    protected _id: string = ''
    protected _name: string = ''
    protected _description: string = ''
    protected _averageDailyRate: number = 0
    protected _customer: string = ''
    protected _location: string = ''
    protected _startDate: string = ''
    protected _durationInMonths: string = ''
    protected _remoteIsPossible: boolean = false
    protected _contractType: string = 'other'
    protected _alreadyContacted: boolean = false

    withId(value: string): MissionDetailsVMBuilder {
        this._id = value
        return this
    }

    withName(value: string): MissionDetailsVMBuilder {
        this._name = value
        return this
    }

    withDescription(value: string): MissionDetailsVMBuilder {
        this._description = value
        return this
    }

    withAverageDailyRate(value: number): MissionDetailsVMBuilder {
        this._averageDailyRate = value
        return this
    }

    withCustomer(value: string): MissionDetailsVMBuilder {
        this._customer = value
        return this
    }

    withLocation(value: string): MissionDetailsVMBuilder {
        this._location = value
        return this
    }

    withStartDate(value: string): MissionDetailsVMBuilder {
        this._startDate = value
        return this
    }

    withDurationInMonths(value: string): MissionDetailsVMBuilder {
        this._durationInMonths = value
        return this
    }

    withRemoteIsPossible(value: boolean): MissionDetailsVMBuilder {
        this._remoteIsPossible = value
        return this
    }

    withContractType(value: string): MissionDetailsVMBuilder {
        this._contractType = value
        return this
    }

    withAlreadyContacted(value: boolean): MissionDetailsVMBuilder {
        this._alreadyContacted = value
        return this
    }

    build(): MissionDetailsVM {
        return new MissionDetailsVM(
            this._id,
            this._name,
            this._description,
            this._averageDailyRate,
            this._customer,
            this._location,
            this._startDate,
            this._durationInMonths,
            this._remoteIsPossible,
            this._contractType,
            this._alreadyContacted
        )
    }

}
