import { format } from 'date-fns/fp'
import { concat, cond, endsWith, identity, init, pipe, startsWith, T, tail } from 'ramda'

export class MissionDetailsVM {

    constructor(private _id: string,
                private _name: string,
                private _description: string,
                private _averageDailyRate: number,
                private _customer: string,
                private _location: string,
                private _startDate: string,
                private _durationInMonths: string,
                private _remoteIsPossible: boolean,
                private _contractType: string,
                private _alreadyContacted: boolean) {
    }

    get id(): string {
        return this._id
    }

    get name(): string {
        return this._name
    }

    get description(): string {
        return this._description
    }

    get averageDailyRate(): number {
        return this._averageDailyRate
    }

    get customer(): string {
        return this._customer
    }

    get location(): string {
        return this._location
    }

    get startDate(): string {
        return format('dd/MM/yyyy')(this._startDate)
    }

    get durationInMonths(): string {
        return cond([
            [startsWith('-'), pipe(tail, concat('< '))],
            [endsWith('+'), pipe(init, concat('> '))],
            [T, identity]
        ])(this._durationInMonths)
    }

    get remoteIsPossible(): boolean {
        return this._remoteIsPossible
    }

    get contractType(): string {
        return this._contractType
    }

    get alreadyContacted(): boolean {
        return this._alreadyContacted
    }

}
