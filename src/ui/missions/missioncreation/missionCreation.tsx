import { Button, Form, H2, Icon, Text } from 'native-base'
import React, { PureComponent } from 'react'
import { BackHandler, Keyboard, StyleSheet, TextStyle, View, ViewStyle } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { MissionCreationFields } from '../../../app/missions/usecases/missioncreation/missionCreation.actions'
import { FormDataPicker } from '../../common/form/formDataPicker'
import { FormDatePicker } from '../../common/form/formDatePicker'
import { FormInputNumeric } from '../../common/form/formInputNumeric'
import { FormInputText } from '../../common/form/formInputText'
import { FormSwitch } from '../../common/form/formSwitch'
import { FormTextArea } from '../../common/form/formTextArea'
import { ModalHeader } from '../../common/modalheader/modalHeader'
import { Props } from './index'
import { InformationTemplateModal } from './informationtemplate'
import { pickerDataContractType, pickerDataDuration } from './pickerDataValues'

export class MissionCreation extends PureComponent<Props> {

    componentDidMount(): void {
        BackHandler.addEventListener('hardwareBackPress', this.handleClose)
    }

    render() {
        return (
            <React.Fragment>
                <ModalHeader leftIcon={'close-circle-outline'} onLeftClick={this.handleClose}/>

                <KeyboardAwareScrollView>
                    <View style={styles.container}>
                        <H2 style={styles.title}>{this.props.capitalize('new_mission')}</H2>
                        <Form>
                            <FormInputText
                                label={this.props.capitalize('title_of_the_mission')}
                                value={this.props.missionUnderCreation.name}
                                error={this.props.missionUnderCreationError.name}
                                onChange={this.onChangeMissionInformation('name')}
                            />

                            <FormInputNumeric
                                label={this.props.capitalize('average_daily_rate_indicative')}
                                value={this.props.missionUnderCreation.averageDailyRate}
                                error={this.props.missionUnderCreationError.averageDailyRate}
                                onChange={this.onChangeMissionInformation('averageDailyRate')}
                            />

                            <FormInputText
                                label={this.props.capitalize('customer')}
                                value={this.props.missionUnderCreation.customer}
                                onChange={this.onChangeMissionInformation('customer')}
                            />

                            <FormInputText
                                label={this.props.capitalize('location')}
                                value={this.props.missionUnderCreation.location}
                                error={this.props.missionUnderCreationError.location}
                                onChange={this.onChangeMissionInformation('location')}
                            />

                            <FormDatePicker
                                label={this.props.capitalize('start_date')}
                                placeholder={this.props.t('select')}
                                confirmButtonText={this.props.capitalize('confirm')}
                                cancelButtonText={this.props.capitalize('cancel')}
                                value={this.props.missionUnderCreation.startDate}
                                error={this.props.missionUnderCreationError.startDate}
                                onChange={this.props.changeMissionStartDate}
                            />

                            <FormDataPicker
                                label={this.props.capitalize('duration_in_months')}
                                placeholder={this.props.t('select')}
                                values={pickerDataDuration(this.props.t)}
                                value={this.props.missionUnderCreation.durationInMonths}
                                error={this.props.missionUnderCreationError.durationInMonths}
                                onChange={this.onChangeMissionInformation('durationInMonths')}
                            />

                            <FormDataPicker
                                label={this.props.capitalize('contract_type')}
                                values={pickerDataContractType(this.props.capitalize)}
                                value={this.props.missionUnderCreation.contractType}
                                onChange={this.onChangeMissionInformation('contractType')}
                            />

                            <FormSwitch
                                label={this.props.capitalize('remote')}
                                value={this.props.missionUnderCreation.remoteIsPossible}
                                onChange={this.onChangeMissionInformation('remoteIsPossible')}
                            />

                            <View>
                                <Icon
                                    name={'information-circle'}
                                    style={styles.information}
                                    onPress={this.props.openInformationTemplate}
                                />
                                <FormTextArea
                                    label={this.props.capitalize('description')}
                                    value={this.props.missionUnderCreation.description}
                                    error={this.props.missionUnderCreationError.description}
                                    onChange={this.onChangeMissionInformation('description')}
                                />
                            </View>

                            <Button block={true} onPress={this.submitForm} style={styles.publishButton}>
                                <Text>{this.props.capitalize('publish')}</Text>
                            </Button>

                            <Button block={true} onPress={this.handleClose} light={true}>
                                <Text>{this.props.capitalize('cancel')}</Text>
                            </Button>
                        </Form>
                    </View>
                </KeyboardAwareScrollView>
                <InformationTemplateModal/>
            </React.Fragment>
        )
    }

    private onChangeMissionInformation = (field: MissionCreationFields) =>
        (value: string | number | boolean) => this.props.changeMissionInformation(field, value)

    private submitForm = () => {
        this.props.publishMission()

        if (!this.props.hasError)
            this.handleClose()
    }

    private handleClose = () => {
        Keyboard.dismiss()
        this.props.quitMissionCreation()
        BackHandler.removeEventListener('hardwareBackPress', this.handleClose)
    }

}

type Styles = {
    container: ViewStyle
    title: TextStyle
    item: ViewStyle
    inlineItem: ViewStyle
    information: TextStyle
    publishButton: ViewStyle
}
const styles = StyleSheet.create<Styles>({
    container    : {
        flex           : 1,
        backgroundColor: 'white',
        padding        : 20
    },
    title        : {
        marginBottom: 20,
        textAlign   : 'center'
    },
    item         : {
        marginBottom     : 15,
        marginLeft       : 0,
        borderBottomWidth: 0.5,
        borderBottomColor: 'rgb(200, 200, 200)'
    },
    inlineItem   : {
        paddingBottom: 10
    },
    information  : {
        fontSize: 25,
        position: 'absolute',
        right   : 0,
        top     : 5,
        zIndex  : 1
    },
    publishButton: {
        marginBottom: 10
    }
})
