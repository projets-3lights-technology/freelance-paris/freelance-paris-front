import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { I18nKeys } from '../../../app/common/domain/translations/i18.keys'
import * as navigationActions from '../../../app/common/usecases/navigation/navigation.actions'
import {
    MissionUnderCreationErrorState,
    MissionUnderCreationState
} from '../../../app/missions/domain/MissionsState.redux'
import * as missionCreationActions from '../../../app/missions/usecases/missioncreation/missionCreation.actions'
import { MissionCreationFields } from '../../../app/missions/usecases/missioncreation/missionCreation.actions'
import {
    capitalize,
    getMissionCreationHasError,
    getMissionCreationIsLoading,
    getMissionUnderCreation,
    getMissionUnderCreationError,
    t
} from '../../../configuration/redux/rootReducer.redux'
import { AppState } from '../../../configuration/redux/rootState.redux'
import { MissionCreation } from './missionCreation'

type StateProps = {
    missionUnderCreation: MissionUnderCreationState
    isLoading: boolean
    hasError: boolean
    missionUnderCreationError: MissionUnderCreationErrorState
    capitalize: (key: I18nKeys) => string
    t: (key: I18nKeys) => string
}
type DispatchProps = {
    quitMissionCreation: () => void
    publishMission: () => void
    changeMissionInformation: (field: MissionCreationFields, value: string | number | boolean) => void
    changeMissionStartDate: (value: string) => void
    openInformationTemplate: () => void
}
type OwnProps = {}
export type Props = StateProps & DispatchProps & OwnProps

const mapStateToProps = (state: AppState): StateProps => ({
    missionUnderCreation     : getMissionUnderCreation(state),
    isLoading                : getMissionCreationIsLoading(state),
    hasError                 : getMissionCreationHasError(state),
    missionUnderCreationError: getMissionUnderCreationError(state),
    capitalize               : (key: I18nKeys) => capitalize(state)(key),
    t                        : (key: I18nKeys) => t(state)(key)
})

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => bindActionCreators(
    {
        quitMissionCreation     : navigationActions.Actions.quitMissionCreation,
        publishMission          : missionCreationActions.Actions.publishMission,
        changeMissionInformation: missionCreationActions.Actions.changeMissionInformation,
        changeMissionStartDate  : missionCreationActions.Actions.changeMissionStartDate,
        openInformationTemplate : navigationActions.Actions.openInformationTemplate
    },
    dispatch
)

export const MissionCreationPage = connect<StateProps, DispatchProps, OwnProps, AppState>(
    mapStateToProps,
    mapDispatchToProps
)(MissionCreation)
