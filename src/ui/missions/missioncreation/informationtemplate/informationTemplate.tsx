import { H2 } from 'native-base'
import React, { PureComponent } from 'react'
import { BackHandler, StyleSheet, Text, TextStyle, View, ViewStyle } from 'react-native'
import Modal from 'react-native-modal'
import { Props } from '.'

export class InformationTemplate extends PureComponent<Props> {

    componentDidMount(): void {
        BackHandler.addEventListener('hardwareBackPress', this.handleClose)
    }

    render() {
        return (
            <Modal
                isVisible={this.props.isOpen}
                animationIn="fadeIn"
                animationOut="fadeOut"
                backdropColor="black"
                onBackdropPress={this.handleClose}
                onBackButtonPress={this.handleClose}
                avoidKeyboard={true}
            >
                <View style={styles.container}>
                    <H2 style={styles.title}>{this.props.t('faq')}</H2>
                    <View>
                        <Text style={styles.item}>{this.props.t('faq_payment_condition')}</Text>
                        <Text style={styles.item}>{this.props.t('faq_renew_contract')}</Text>
                        <Text style={styles.item}>{this.props.t('faq_team_size')}</Text>
                        <Text style={styles.item}>{this.props.t('faq_mission_type')}</Text>
                        <Text style={styles.item}>{this.props.t('faq_stack')}</Text>
                        <Text style={styles.item}>{this.props.t('faq_mission_context')}</Text>
                    </View>
                </View>
            </Modal>
        )
    }

    private handleClose = () => {
        this.props.quitInformationTemplate()
        BackHandler.removeEventListener('hardwareBackPress', this.handleClose)
    }

}

type Styles = {
    container: ViewStyle
    title: TextStyle
    item: TextStyle
}
const styles = StyleSheet.create<Styles>({
    container: {
        backgroundColor: 'white',
        borderRadius   : 30,
        padding        : 20
    },
    title    : {
        marginBottom: 30,
        textAlign   : 'center'
    },
    item     : {
        marginVertical: 5,
        marginLeft    : 10
    }
})
