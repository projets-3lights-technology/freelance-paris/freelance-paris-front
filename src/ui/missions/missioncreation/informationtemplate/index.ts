import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { I18nKeys } from '../../../../app/common/domain/translations/i18.keys'
import * as navigationActions from '../../../../app/common/usecases/navigation/navigation.actions'
import { isInformationTemplateOpen, t } from '../../../../configuration/redux/rootReducer.redux'
import { AppState } from '../../../../configuration/redux/rootState.redux'
import { InformationTemplate } from './informationTemplate'

type StateProps = {
    isOpen: boolean
    t: (key: I18nKeys) => string
}
type DispatchProps = {
    quitInformationTemplate: () => void
}
type OwnProps = {}
export type Props = StateProps & DispatchProps & OwnProps

const mapStateToProps = (state: AppState): StateProps => ({
    isOpen: isInformationTemplateOpen(state),
    t     : (key: I18nKeys) => t(state)(key)
})

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => bindActionCreators(
    {
        quitInformationTemplate: navigationActions.Actions.quitInformationTemplate
    },
    dispatch
)

export const InformationTemplateModal = connect<StateProps, DispatchProps, OwnProps, AppState>(
    mapStateToProps,
    mapDispatchToProps
)(InformationTemplate)
