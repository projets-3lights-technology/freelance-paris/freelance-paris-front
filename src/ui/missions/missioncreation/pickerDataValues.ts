import { I18nKeys } from '../../../app/common/domain/translations/i18.keys'
import { ContractTypeTo, MissionContractType } from '../../../app/missions/domain/entities/missionContractType'
import { DataPickerValues } from '../../common/form/formDataPicker'

export const pickerDataDuration = (t: (key: I18nKeys) => string): DataPickerValues => [
    { label: `< 1 ${t('months')}`, value: '-1' },
    { label: `1 ${t('month')}`, value: '1' },
    { label: `2 ${t('months')}`, value: '2' },
    { label: `3 ${t('months')}`, value: '3' },
    { label: `4 ${t('months')}`, value: '4' },
    { label: `5 ${t('months')}`, value: '5' },
    { label: `6 ${t('months')}`, value: '6' },
    { label: `7 ${t('months')}`, value: '7' },
    { label: `8 ${t('months')}`, value: '8' },
    { label: `9 ${t('months')}`, value: '9' },
    { label: `10 ${t('months')}`, value: '10' },
    { label: `11 ${t('months')}`, value: '11' },
    { label: `> 12 ${t('months')}`, value: '12+' }
]

export const pickerDataContractType = (t: (key: I18nKeys) => string): DataPickerValues => [
    { label: t('apprenticeship'), value: ContractTypeTo.string(MissionContractType.APPRENTICESHIP) },
    { label: t('cdd'), value: ContractTypeTo.string(MissionContractType.CDD) },
    { label: t('cdi'), value: ContractTypeTo.string(MissionContractType.CDI) },
    { label: t('freelance'), value: ContractTypeTo.string(MissionContractType.FREELANCE) },
    { label: t('interim'), value: ContractTypeTo.string(MissionContractType.INTERIM) },
    { label: t('portage'), value: ContractTypeTo.string(MissionContractType.PORTAGE) },
    { label: t('other'), value: ContractTypeTo.string(MissionContractType.OTHER) }
]
