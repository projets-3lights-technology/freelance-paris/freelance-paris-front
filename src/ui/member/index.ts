import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { Optional } from 'typescript-optional'
import { I18nKeys } from '../../app/common/domain/translations/i18.keys'
import * as gatekeeperActions from '../../app/common/usecases/gatekeeper/gatekeeper.actions'
import * as navigationActions from '../../app/common/usecases/navigation/navigation.actions'
import {
    MemberUnderModificationErrorState,
    MemberUnderModificationState
} from '../../app/member/domain/MemberState.redux'
import * as memberModificationActions from '../../app/member/usecases/membermodification/memberModification.actions'
import { MemberModificationFields } from '../../app/member/usecases/membermodification/memberModification.actions'
import {
    capitalize,
    getMemberInformation,
    getMemberModificationHasError,
    getMemberModificationIsLoading,
    getMemberUnderModification,
    getMemberUnderModificationError,
    t
} from '../../configuration/redux/rootReducer.redux'
import { AppState } from '../../configuration/redux/rootState.redux'
import { MemberPresenter } from '../login/presenters/member.present'
import { MemberVM } from '../login/viewmodels/member.viewmodel'
import { MemberProfileContainer } from './memberProfile'

type StateProps = {
    member: Optional<MemberVM>
    memberUnderModification: MemberUnderModificationState
    isLoading: boolean
    hasError: boolean
    memberUnderModificationError: MemberUnderModificationErrorState
    capitalize: (key: I18nKeys) => string
    t: (key: I18nKeys) => string
}
type DispatchProps = {
    quitMemberProfile: () => void
    updateMember: () => void
    changeMemberInformation: (field: MemberModificationFields, value: string) => void
    unsubscribe: () => void
}
type OwnProps = {}
export type Props = StateProps & DispatchProps & OwnProps

const mapStateToProps = (state: AppState): StateProps => ({
    member                      : MemberPresenter.present(getMemberInformation(state)),
    memberUnderModification     : getMemberUnderModification(state),
    isLoading                   : getMemberModificationIsLoading(state),
    hasError                    : getMemberModificationHasError(state),
    memberUnderModificationError: getMemberUnderModificationError(state),
    capitalize                  : (key: I18nKeys) => capitalize(state)(key),
    t                           : (key: I18nKeys) => t(state)(key)
})

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => bindActionCreators(
    {
        quitMemberProfile      : navigationActions.Actions.quitMemberProfile,
        updateMember           : memberModificationActions.Actions.updateMember,
        changeMemberInformation: memberModificationActions.Actions.changeMemberInformation,
        unsubscribe            : gatekeeperActions.Actions.unsubscribe
    },
    dispatch
)

export const MemberProfilePage = connect<StateProps, DispatchProps, OwnProps, AppState>(
    mapStateToProps,
    mapDispatchToProps
)(MemberProfileContainer)
