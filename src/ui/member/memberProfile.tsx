import { Button, Form, H2, Text } from 'native-base'
import { isEmpty } from 'ramda'
import React, { PureComponent } from 'react'
import { BackHandler, Keyboard, StyleSheet, TextStyle, View, ViewStyle } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { MemberModificationFields } from '../../app/member/usecases/membermodification/memberModification.actions'
import { FormInputText } from '../common/form/formInputText'
import { ModalHeader } from '../common/modalheader/modalHeader'
import { Props } from './index'

export class MemberProfileContainer extends PureComponent<Props> {

    componentDidMount(): void {
        BackHandler.addEventListener('hardwareBackPress', this.handleClose)
    }

    render() {
        const memberEmail = isEmpty(this.props.memberUnderModification.email)
            ? this.props.member.get().email
            : this.props.memberUnderModification.email
        return (
            <React.Fragment>
                <ModalHeader leftIcon={'close-circle-outline'} onLeftClick={this.handleClose}/>

                <KeyboardAwareScrollView>
                    <View style={styles.container}>
                        <H2 style={styles.title}>{this.props.capitalize('member_profile')}</H2>
                        <Form>
                            <FormInputText
                                label={'Email'}
                                value={memberEmail}
                                error={this.props.memberUnderModificationError.email}
                                onChange={this.onChangeMemberInformation('email')}
                            />

                            <Text>{this.props.t('email_member_information')}</Text>

                            <Button block={true} onPress={this.submitForm} style={styles.publishButton}>
                                <Text>{this.props.capitalize('update')}</Text>
                            </Button>

                            <Button block={true} onPress={this.handleClose} light={true}>
                                <Text>{this.props.capitalize('cancel')}</Text>
                            </Button>

                            <Button
                                block={true}
                                onPress={this.handleUnsubscribe}
                                bordered={true}
                                danger={true}
                                style={styles.deleteAccount}
                            >
                                <Text>{this.props.capitalize('delete_account')}</Text>
                            </Button>
                        </Form>
                    </View>
                </KeyboardAwareScrollView>
            </React.Fragment>
        )
    }

    private onChangeMemberInformation = (field: MemberModificationFields) =>
        (value: string) => this.props.changeMemberInformation(field, value)

    private submitForm = () => {
        this.props.updateMember()

        if (!this.props.hasError)
            this.handleClose()
    }

    private handleClose = () => {
        Keyboard.dismiss()
        this.props.quitMemberProfile()
        BackHandler.removeEventListener('hardwareBackPress', this.handleClose)
    }

    private handleUnsubscribe = () => {
        Keyboard.dismiss()
        this.props.unsubscribe()
        BackHandler.removeEventListener('hardwareBackPress', this.handleClose)
    }

}

type Styles = {
    container: ViewStyle
    title: TextStyle
    publishButton: ViewStyle
    deleteAccount: ViewStyle
}
const styles = StyleSheet.create<Styles>({
    container    : {
        flex           : 1,
        backgroundColor: 'white',
        padding        : 20
    },
    title        : {
        marginBottom: 20,
        textAlign   : 'center'
    },
    publishButton: {
        marginTop   : 20,
        marginBottom: 10
    },
    deleteAccount: {
        marginTop: 40
    }
})
