import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { Token } from '../../app/common/domain/entities/token'
import { I18nKeys } from '../../app/common/domain/translations/i18.keys'
import * as gatekeeperActions from '../../app/common/usecases/gatekeeper/gatekeeper.actions'
import * as navigationActions from '../../app/common/usecases/navigation/navigation.actions'
import { capitalize } from '../../configuration/redux/rootReducer.redux'
import { AppState } from '../../configuration/redux/rootState.redux'
import { Login } from './login'

type StateProps = {
    capitalize: (key: I18nKeys) => string
}
type DispatchProps = {
    login: (token: Token) => void
    loginGuest: () => void
    openSignIn: () => void
}
type OwnProps = {}
export type Props = StateProps & DispatchProps & OwnProps

const mapStateToProps = (state: AppState): StateProps => ({
    capitalize: (key: I18nKeys) => capitalize(state)(key)
})

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => bindActionCreators(
    {
        login     : gatekeeperActions.Actions.logIn,
        loginGuest: gatekeeperActions.Actions.logInGuest,
        openSignIn: navigationActions.Actions.openSignIn
    }, dispatch
)

export const LoginPage = connect<StateProps, DispatchProps, OwnProps, AppState>(
    mapStateToProps,
    mapDispatchToProps
)(Login)
