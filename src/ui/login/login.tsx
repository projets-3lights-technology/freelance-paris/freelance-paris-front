import { Button, Form, H1, Icon, Text } from 'native-base'
import React, { PureComponent } from 'react'
import { ImageBackground, ImageStyle, StyleSheet, TextStyle, View, ViewStyle } from 'react-native'
// @ts-ignore
import LinkedInModal from 'react-native-linkedin'
import { Token } from '../../app/common/domain/entities/token'
import { AppAssets } from '../../assets/appAssets'
import { EnvVarConfig } from '../../configuration/environment/envVar.appConfiguration'
import { Props } from './index'

export class Login extends PureComponent<Props> {

    private linkedModal: any

    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={AppAssets.background} style={styles.background}>
                    <View style={styles.logo}>
                        <H1 style={styles.text}>{this.props.capitalize('freelanceParis')}</H1>
                    </View>
                    <Form style={styles.formContainer}>
                        <View style={styles.form}>
                            <LinkedInModal
                                ref={this.setLinkedInModal}
                                clientID={new EnvVarConfig().get('LINKEDIN_CLIENT_ID')}
                                clientSecret={new EnvVarConfig().get('LINKEDIN_CLIENT_SECRET')}
                                redirectUri={new EnvVarConfig().get('LINKEDIN_REDIRECT_URL')}
                                permissions={['r_basicprofile', 'r_emailaddress']}
                                renderButton={this.linkedInSignInButton}
                                onSuccess={this.signIn}
                            />
                            <Text
                                style={styles.guestButton}
                                onPress={this.props.loginGuest}
                            >
                                {this.props.capitalize('enter_as_guest')}
                            </Text>
                        </View>
                    </Form>
                </ImageBackground>
            </View>
        )
    }

    private linkedInSignInButton = () => (
        <Button
            block={true}
            onPress={this.openLinkedInModal}
            iconLeft={true}
            style={{ marginBottom: 10 }}
        >
            <Icon name="linkedin" type={'FontAwesome'}/>
            <Text>LinkedIn</Text>
        </Button>
    )

    private signIn = (token: { access_token: string, expires_in: number }) =>
        this.props.login(new Token(token.access_token, token.expires_in))

    private setLinkedInModal = (ref: any) => this.linkedModal = ref

    private openLinkedInModal = () => {
        this.props.openSignIn()
        this.linkedModal.open()
    }

}

type Styles = {
    container: ViewStyle
    background: ImageStyle
    logo: ViewStyle
    text: TextStyle
    formContainer: ViewStyle
    form: ViewStyle
    guestButton: TextStyle
}

const styles = StyleSheet.create<Styles>({
    container    : {
        flex: 2
    },
    background   : {
        flex          : 1,
        width         : '100%',
        height        : '100%',
        alignItems    : 'center',
        justifyContent: 'center',
        resizeMode    : 'cover'
    },
    logo         : {
        flex          : 2,
        justifyContent: 'center'
    },
    formContainer: {
        flex : 2,
        width: '90%'
    },
    form         : {
        paddingHorizontal: 20,
        paddingTop       : 40,
        paddingBottom    : 30,
        backgroundColor  : 'rgba(255, 255, 255, 0.5)',
        borderRadius     : 30
    },
    text         : {
        color: 'white'
    },
    guestButton  : {
        marginTop         : 10,
        color             : 'white',
        fontSize          : 13,
        textAlign         : 'right',
        textDecorationLine: 'underline'
    }
})
