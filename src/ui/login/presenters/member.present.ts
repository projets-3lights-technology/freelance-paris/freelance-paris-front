import { Optional } from 'typescript-optional'
import { MemberInformationById } from '../../../app/member/domain/MemberState.redux'
import { MemberVM } from '../viewmodels/member.viewmodel'
import { MemberVMBuilder } from '../viewmodels/memberVM.builder'

export class MemberPresenter {

    static present(member: Optional<MemberInformationById>): Optional<MemberVM> {
        return member.map(value => new MemberVMBuilder()
            .withId(value.id)
            .withFirstName(value.firstName)
            .withLastName(value.lastName)
            .withEmail(value.email)
            .withAvatar(value.avatar)
            .build()
        )
    }

}
