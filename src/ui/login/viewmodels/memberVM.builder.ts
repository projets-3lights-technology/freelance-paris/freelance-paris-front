import { MemberVM } from './member.viewmodel'

export class MemberVMBuilder {

    protected _id: string = ''
    protected _firstName: string = ''
    protected _lastName: string = ''
    protected _email: string = ''
    protected _avatar: string = ''

    withId(value: string): MemberVMBuilder {
        this._id = value
        return this
    }

    withFirstName(value: string): MemberVMBuilder {
        this._firstName = value
        return this
    }

    withLastName(value: string): MemberVMBuilder {
        this._lastName = value
        return this
    }

    withEmail(value: string): MemberVMBuilder {
        this._email = value
        return this
    }

    withAvatar(value: string): MemberVMBuilder {
        this._avatar = value
        return this
    }

    build(): MemberVM {
        return new MemberVM(
            this._id,
            this._firstName,
            this._lastName,
            this._email,
            this._avatar
        )
    }

}
