import { isEmpty } from 'ramda'
import { AppAssets } from '../../../assets/appAssets'

export class MemberVM {

    constructor(private _id: string,
                private _firstName: string,
                private _lastName: string,
                private _email: string,
                private _avatar: string) {
    }

    get id(): string {
        return this._id
    }

    get fullName(): string {
        return this._firstName + ' ' + this._lastName
    }

    get email(): string {
        return this._email
    }

    get avatar(): any {
        return isEmpty(this._avatar)
            ? AppAssets.userDefault
            : { uri: this._avatar }
    }

}
