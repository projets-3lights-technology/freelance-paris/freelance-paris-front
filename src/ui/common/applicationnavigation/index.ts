import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { I18nKeys } from '../../../app/common/domain/translations/i18.keys'
import * as navigationActions from '../../../app/common/usecases/navigation/navigation.actions'
import { capitalize, isOnMissions } from '../../../configuration/redux/rootReducer.redux'
import { AppState } from '../../../configuration/redux/rootState.redux'
import { ApplicationNavigationContainer } from './applicationNavigation'

type StateProps = {
    isOnMissions: boolean
    capitalize: (key: I18nKeys) => string
}
type DispatchProps = {
    navigateToMissions: () => void
}
type OwnProps = {}
export type Props = StateProps & DispatchProps & OwnProps

const mapStateToProps = (state: AppState): StateProps => ({
    isOnMissions: isOnMissions(state),
    capitalize  : (key: I18nKeys) => capitalize(state)(key)
})

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => bindActionCreators(
    {
        navigateToMissions: navigationActions.Actions.navigateToMissions
    },
    dispatch
)

export const ApplicationNavigation = connect<StateProps, DispatchProps, OwnProps, AppState>(
    mapStateToProps,
    mapDispatchToProps
)(ApplicationNavigationContainer)
