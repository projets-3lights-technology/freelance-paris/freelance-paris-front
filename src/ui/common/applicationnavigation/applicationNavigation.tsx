import { Button, Footer, FooterTab, Text } from 'native-base'
import React, { PureComponent } from 'react'
import { StyleSheet, TextStyle } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Props } from './index'

export class ApplicationNavigationContainer extends PureComponent<Props> {

    render() {
        return (
            <Footer>
                <FooterTab style={{ backgroundColor: 'white' }}>
                    <Button active={this.props.isOnMissions} vertical={true} onPress={this.props.navigateToMissions}>
                        <Icon name="briefcase" size={22}/>
                        <Text style={styles.text}>{this.props.capitalize('missions')}</Text>
                    </Button>
                </FooterTab>
            </Footer>
        )
    }

}

type Styles = {
    text: TextStyle
}
const styles = StyleSheet.create<Styles>({
    text: {
        color: 'black'
    }
})
