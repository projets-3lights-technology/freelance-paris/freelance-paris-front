import { Body, Button, Header, Icon, Left, Right } from 'native-base'
import React, { FunctionComponent } from 'react'
import { StyleSheet, TextStyle, ViewStyle } from 'react-native'
import { AppTheme } from '../../../assets/appTheme'

type Props = {
    leftIcon: string
    onLeftClick: () => void
}

export const ModalHeader: FunctionComponent<Props> = ({ ...props }) => {
    return (
        <Header style={styles.header} iosBarStyle={'light-content'} androidStatusBarColor={AppTheme.mulledWine}>
            <Left>
                <Button transparent={true} onPress={props.onLeftClick}>
                    <Icon style={styles.icon} name={props.leftIcon}/>
                </Button>
            </Left>
            <Body/>
            <Right/>
        </Header>
    )
}

type Styles = {
    header: ViewStyle
    icon: TextStyle
}
const styles = StyleSheet.create<Styles>({
    header: {
        backgroundColor: AppTheme.mulledWine
    },
    icon  : {
        color     : 'white',
        marginLeft: 10
    }
})
