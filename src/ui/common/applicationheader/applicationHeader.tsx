import { Body, Button, Header, Icon, Left, Right, Title } from 'native-base'
import React, { PureComponent } from 'react'
import { StyleSheet, TextStyle, ViewStyle } from 'react-native'
import { AppTheme } from '../../../assets/appTheme'
import { Props } from './index'

export class ApplicationHeaderContainer extends PureComponent<Props> {

    render() {
        return (
            <Header style={styles.header} iosBarStyle={'light-content'} androidStatusBarColor={AppTheme.mulledWine}>
                <Left>
                    <Button transparent={true} onPress={this.props.openSideMenu}>
                        <Icon style={styles.icon} type={'FontAwesome'} name="bars"/>
                    </Button>
                </Left>
                <Body>
                <Title style={styles.text}>{this.props.title}</Title>
                </Body>
                <Right/>
            </Header>
        )
    }

}

type Styles = {
    header: ViewStyle
    icon: TextStyle
    text: TextStyle
}
const styles = StyleSheet.create<Styles>({
    header: {
        backgroundColor: AppTheme.mulledWine
    },
    icon  : {
        marginLeft: 10,
        color     : 'white',
        fontSize  : 26
    },
    text  : {
        color: 'white'
    }
})
