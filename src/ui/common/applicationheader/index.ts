import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import * as navigationActions from '../../../app/common/usecases/navigation/navigation.actions'
import { AppState } from '../../../configuration/redux/rootState.redux'
import { ApplicationHeaderContainer } from './applicationHeader'

type StateProps = {}
type DispatchProps = {
    openSideMenu: () => void
}
type OwnProps = {
    title: string
}
export type Props = StateProps & DispatchProps & OwnProps

const mapStateToProps = (state: AppState): StateProps => ({})

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => bindActionCreators(
    {
        openSideMenu: navigationActions.Actions.openSideMenu
    },
    dispatch
)

export const ApplicationHeader = connect<StateProps, DispatchProps, OwnProps, AppState>(
    mapStateToProps,
    mapDispatchToProps
)(ApplicationHeaderContainer)
