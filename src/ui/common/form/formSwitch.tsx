import { Item, Label, Right } from 'native-base'
import { isEmpty, isNil } from 'ramda'
import React, { FunctionComponent } from 'react'
import { StyleSheet, Switch, Text, TextStyle, ViewStyle } from 'react-native'

type Props = {
    label: string
    value: boolean
    onChange: (value: boolean) => void
    error?: string
}

export const FormSwitch: FunctionComponent<Props> = ({ ...props }) => {
    const label = isEmpty(props.error) || isNil(props.error)
        ? <Label style={styles.label}>{props.label}</Label>
        : <Label style={styles.label}>{props.label} <Text style={styles.required}>*</Text></Label>
    return (
        <Item inlineLabel={true} style={styles.item}>
            {label}
            <Right>
                <Switch
                    value={props.value}
                    onValueChange={props.onChange}
                />
            </Right>
        </Item>
    )
}

type Style = {
    label: TextStyle
    item: ViewStyle
    required: TextStyle
}
const styles = StyleSheet.create<Style>({
    label   : {
        fontSize: 16
    },
    item    : {
        marginBottom     : 15,
        marginLeft       : 0,
        borderBottomWidth: 0.5,
        borderBottomColor: 'rgb(200, 200, 200)',
        paddingTop       : 5,
        paddingBottom    : 14
    },
    required: {
        color: 'red'
    }
})
