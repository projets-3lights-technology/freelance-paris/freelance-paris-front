import { Item, Label } from 'native-base'
import { isEmpty, isNil } from 'ramda'
import React, { FunctionComponent } from 'react'
import { Text } from 'react-native'
import { KeyboardTypeOptions, StyleSheet, TextInput, TextStyle, ViewStyle } from 'react-native'
import { defaultProps } from 'recompose'

type Props = {
    label: string
    value: string
    onChange: (value: string) => void
    type?: KeyboardTypeOptions
    error?: string
}

const InputTextWithoutDefaultProps: FunctionComponent<Props> = ({ ...props }) => {
    const label = isEmpty(props.error) || isNil(props.error)
        ? <Label>{props.label}</Label>
        : <Label>{props.label} <Text style={styles.required}>*</Text></Label>
    return (
        <Item stackedLabel={true} style={styles.item}>
            {label}
            <TextInput
                style={styles.input}
                value={props.value}
                clearButtonMode={'while-editing'}
                onChangeText={props.onChange}
                keyboardType={props.type}
                returnKeyType={'done'}
            />
        </Item>
    )
}

const withDefaultProps = defaultProps<Props>({
    label   : '',
    value   : '',
    onChange: null,
    type    : 'default'
})

export const FormInputText = withDefaultProps(InputTextWithoutDefaultProps)

type Style = {
    item: ViewStyle
    input: TextStyle
    required: TextStyle
}
const styles = StyleSheet.create<Style>({
    item    : {
        marginBottom     : 15,
        marginLeft       : 0,
        borderBottomWidth: 0.5,
        borderBottomColor: 'rgb(200, 200, 200)'
    },
    input   : {
        width        : '100%',
        paddingTop   : 13,
        paddingBottom: 10,
        fontSize     : 16
    },
    required: {
        color: 'red'
    }
})
