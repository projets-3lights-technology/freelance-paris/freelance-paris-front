import { addDays } from 'date-fns/fp'
import { Item, Label, Right } from 'native-base'
import { isEmpty, isNil } from 'ramda'
import React, { FunctionComponent } from 'react'
import { StyleSheet, Text, TextStyle, ViewStyle } from 'react-native'
import DatePicker from 'react-native-datepicker'
import { AppTheme } from '../../../assets/appTheme'

type Props = {
    label: string
    placeholder: string
    confirmButtonText: string
    cancelButtonText: string
    value: string
    onChange: (value: string) => void
    error?: string
}

export const FormDatePicker: FunctionComponent<Props> = ({ ...props }) => {
    const label = isEmpty(props.error) || isNil(props.error)
        ? <Label style={styles.label}>{props.label}</Label>
        : <Label style={styles.label}>{props.label} <Text style={styles.required}>*</Text></Label>
    return (
        <Item inlineLabel={true} style={styles.item}>
            {label}
            <Right>
                <DatePicker
                    style={styles.datePicker}
                    customStyles={styleDatePicker}
                    date={props.value}
                    minDate={addDays(1)(new Date())}
                    placeholder={props.placeholder}
                    format={'DD/MM/YYYY'}
                    confirmBtnText={props.confirmButtonText}
                    cancelBtnText={props.cancelButtonText}
                    showIcon={false}
                    onDateChange={props.onChange}
                >
                    <Text>
                        {props.value}
                    </Text>
                </DatePicker>
            </Right>
        </Item>
    )
}

type Style = {
    label: TextStyle
    item: ViewStyle
    datePicker: ViewStyle
    datePickerText: TextStyle
    placeholderText: TextStyle
    btnConfirm: ViewStyle
    btnTextConfirm: TextStyle
    required: TextStyle
}
const styles = StyleSheet.create<Style>({
    label          : {
        fontSize: 16
    },
    item           : {
        marginBottom     : 15,
        marginLeft       : 0,
        borderBottomWidth: 0.5,
        borderBottomColor: 'rgb(200, 200, 200)',
        paddingBottom    : 10
    },
    datePicker     : {
        width      : '100%',
        borderWidth: 0,
        alignItems : 'flex-end'
    },
    datePickerText : {
        color   : 'black',
        fontSize: 17
    },
    placeholderText: {
        color   : AppTheme.spindle,
        fontSize: 17
    },
    btnConfirm     : {
        backgroundColor: '#007aff'
    },
    btnTextConfirm : {
        color: 'white'
    },
    required: {
        color: 'red'
    }
})
const styleDatePicker = {
    dateInput      : styles.datePicker,
    dateText       : styles.datePickerText,
    placeholderText: styles.placeholderText,
    btnConfirm     : styles.btnConfirm,
    btnTextConfirm : styles.btnTextConfirm
}
