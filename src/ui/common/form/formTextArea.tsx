import { Item, Label } from 'native-base'
import { isEmpty, isNil } from 'ramda'
import React, { FunctionComponent } from 'react'
import { StyleSheet, Text, TextInput, TextStyle, ViewStyle } from 'react-native'

type Props = {
    label: string
    value: string
    onChange: (value: string) => void
    error?: string
}

export const FormTextArea: FunctionComponent<Props> = ({ ...props }) => {
    const label = isEmpty(props.error) || isNil(props.error)
        ? <Label>{props.label}</Label>
        : <Label>{props.label} <Text style={styles.required}>*</Text></Label>
    return (
        <Item stackedLabel={true} style={styles.item}>
            {label}
            <TextInput
                style={styles.textArea}
                value={props.value}
                multiline={true}
                onChangeText={props.onChange}
            />
        </Item>
    )
}

type Style = {
    item: ViewStyle
    textArea: TextStyle
    required: TextStyle
}
const styles = StyleSheet.create<Style>({
    item    : {
        marginBottom     : 15,
        marginLeft       : 0,
        borderBottomWidth: 0.5,
        borderBottomColor: 'rgb(200, 200, 200)'
    },
    textArea: {
        width         : '100%',
        marginVertical: 5,
        fontSize      : 16,
        height        : 500
    },
    required: {
        color: 'red'
    }
})
