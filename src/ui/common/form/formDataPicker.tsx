import { Item, Label, Picker, Right } from 'native-base'
import { isEmpty, isNil } from 'ramda'
import React, { FunctionComponent } from 'react'
import { Platform, Text } from 'react-native'
import { StyleSheet, TextStyle, ViewStyle } from 'react-native'
import { AppTheme } from '../../../assets/appTheme'
import { ModalHeader } from '../modalheader/modalHeader'

export type DataPickerValues = Array<{ label: string, value: string }>

type Props = {
    label: string
    placeholder?: string
    values: DataPickerValues
    value: string
    onChange: (value: string) => void
    error?: string
}

export const FormDataPicker: FunctionComponent<Props> = ({ ...props }) => {
    const label = isEmpty(props.error) || isNil(props.error)
        ? <Label style={styles.label}>{props.label}</Label>
        : <Label style={styles.label}>{props.label} <Text style={styles.required}>*</Text></Label>

    const durationPickerHeader = (backAction: () => void) =>
        <ModalHeader onLeftClick={backAction} leftIcon={'close-circle-outline'}/>

    const items = props.values
        .map((item, idx) => <Picker.Item key={idx} label={item.label} value={item.value}/>)

    return (
        <Item inlineLabel={true} style={styles.item}>
            {label}
            <Right>
                <Picker
                    mode="dialog"
                    style={{ width: Platform.OS === 'ios' ? undefined : 135 }}
                    placeholder={props.placeholder}
                    placeholderStyle={styles.placeholder}
                    textStyle={styles.text}
                    renderHeader={durationPickerHeader}
                    selectedValue={props.value}
                    onValueChange={props.onChange}
                >
                    {items}
                </Picker>
            </Right>
        </Item>
    )
}

type Style = {
    label: TextStyle
    item: ViewStyle
    placeholder: TextStyle
    text: TextStyle
    required: TextStyle
}
const styles = StyleSheet.create<Style>({
    label      : {
        fontSize: 16
    },
    item       : {
        marginBottom     : 15,
        marginLeft       : 0,
        borderBottomWidth: 0.5,
        borderBottomColor: 'rgb(200, 200, 200)',
        paddingBottom    : 10
    },
    placeholder: {
        color      : AppTheme.spindle,
        marginRight: -15
    },
    text       : {
        marginRight: -15
    },
    required   : {
        color: 'red'
    }
})
