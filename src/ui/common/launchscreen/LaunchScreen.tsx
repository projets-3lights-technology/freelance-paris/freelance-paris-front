import React, { FunctionComponent } from 'react'
import { Image, ImageStyle, StyleSheet, View, ViewStyle } from 'react-native'
import { AppAssets } from '../../../assets/appAssets'
import { AppTheme } from '../../../assets/appTheme'

export const LaunchScreen: FunctionComponent = () => {
    return (
        <View style={styles.container}>
            <Image source={AppAssets.logo} resizeMode={'contain'} style={styles.logo}/>
        </View>
    )
}

type Style = {
    container: ViewStyle
    logo: ImageStyle
}
const styles = StyleSheet.create<Style>({
    container: {
        backgroundColor: AppTheme.gunPowder,
        width          : '100%',
        height         : '100%',
        display        : 'flex',
        justifyContent : 'center',
        alignItems     : 'center'
    },
    logo     : {
        width : 200,
        height: 200
    }
})
