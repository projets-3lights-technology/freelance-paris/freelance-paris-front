import {
    Body,
    Button,
    Content,
    Footer,
    Header,
    Icon,
    Left,
    List,
    ListItem,
    Right,
    Separator,
    Text,
    Thumbnail
} from 'native-base'
import React, { PureComponent } from 'react'
import { BackHandler, StyleSheet, TextStyle, ViewStyle } from 'react-native'
import { AppTheme } from '../../../assets/appTheme'
import { Props } from './index'

export class SideMenuContainer extends PureComponent<Props> {

    componentDidMount(): void {
        BackHandler.addEventListener('hardwareBackPress', this.props.quitSideMenu)
    }

    render() {
        if (this.props.isLoading || this.props.memberInformation.isEmpty())
            return null

        const memberInformation = this.props.memberInformation.get()
        return (
            <React.Fragment>
                <Header style={styles.header} iosBarStyle={'light-content'} androidStatusBarColor={AppTheme.mulledWine}>
                    <Body>
                        <Text style={styles.text}>{this.props.t('freelanceParis')}</Text>
                    </Body>
                    <Right>
                        <Button transparent={true} onPress={this.handleCloseMissionProfile}>
                            <Icon style={styles.icon} name="close-circle-outline"/>
                        </Button>
                    </Right>
                </Header>

                <Content scrollEnabled={false}>
                    <List>
                        <ListItem avatar={true} noBorder={true} style={styles.memberInformation}>
                            <Left>
                                <Thumbnail source={memberInformation.avatar}/>
                            </Left>
                            <Body>
                                <Text note={true}>{this.props.capitalize('hello')},</Text>
                                <Text>{memberInformation.fullName}</Text>
                            </Body>
                            <Right style={styles.logoutArea}>
                                <Button transparent={true} onPress={this.logout}>
                                    <Icon style={styles.logoutIcon} type={'FontAwesome'} name="power-off"/>
                                </Button>
                            </Right>
                        </ListItem>

                        <Separator bordered={true}/>

                        {this.props.isAuthenticated && <React.Fragment>
                            <ListItem icon={true} onPress={this.props.openMemberProfile}>
                                <Left>
                                    <Button style={{ backgroundColor: 'blue' }} activeOpacity={1}>
                                        <Icon type={'FontAwesome'} name="cog"/>
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>{this.props.capitalize('my_profile')}</Text>
                                </Body>
                            </ListItem>

                            <ListItem icon={true} noBorder={true} onPress={this.props.openMissionCreation}>
                                <Left>
                                    <Button style={{ backgroundColor: 'green' }} activeOpacity={1}>
                                        <Icon type={'FontAwesome'} name="pencil"/>
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>{this.props.capitalize('create_new_mission')}</Text>
                                </Body>
                            </ListItem>

                            <Separator bordered={true}/>
                        </React.Fragment>}
                    </List>
                </Content>

                <Footer style={styles.footer}>
                    <Body>
                        <Text note={true} onPress={this.props.contactUs}>{this.props.t('contact_us')}</Text>
                    </Body>
                    <Right>
                        <Text note={true}>v1.0</Text>
                    </Right>
                </Footer>
            </React.Fragment>
        )
    }

    private handleCloseMissionProfile = () => {
        this.props.quitSideMenu()
        BackHandler.removeEventListener('hardwareBackPress', this.props.quitSideMenu)
    }

    private logout = () => {
        this.props.logout()
        BackHandler.removeEventListener('hardwareBackPress', this.props.quitSideMenu)
    }

}

type Styles = {
    header: ViewStyle
    memberInformation: ViewStyle
    text: TextStyle
    icon: TextStyle,
    logoutArea: ViewStyle
    logoutIcon: TextStyle
    footer: ViewStyle
}
const styles = StyleSheet.create<Styles>({
    header           : {
        backgroundColor: AppTheme.mulledWine
    },
    memberInformation: {
        paddingBottom: 5
    },
    text             : {
        color     : 'white',
        fontWeight: 'bold'
    },
    icon             : {
        color: 'white'
    },
    logoutArea       : {
        alignSelf: 'center'
    },
    logoutIcon       : {
        color: 'rgba(200, 50, 0, 0.5)'
    },
    footer           : {
        height           : 30,
        paddingHorizontal: 20,
        backgroundColor  : 'rgb(248, 248, 248)'
    }
})
