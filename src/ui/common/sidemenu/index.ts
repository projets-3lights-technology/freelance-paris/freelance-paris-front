import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { Optional } from 'typescript-optional'
import { I18nKeys } from '../../../app/common/domain/translations/i18.keys'
import * as gatekeeperActions from '../../../app/common/usecases/gatekeeper/gatekeeper.actions'
import * as navigationActions from '../../../app/common/usecases/navigation/navigation.actions'
import {
    capitalize,
    getMemberInformation,
    getMemberInformationIsLoading,
    isAuthenticated,
    t
} from '../../../configuration/redux/rootReducer.redux'
import { AppState } from '../../../configuration/redux/rootState.redux'
import { MemberPresenter } from '../../login/presenters/member.present'
import { MemberVM } from '../../login/viewmodels/member.viewmodel'
import { SideMenuContainer } from './sideMenu'

type StateProps = {
    t: (key: I18nKeys) => string
    capitalize: (key: I18nKeys) => string
    isLoading: boolean
    memberInformation: Optional<MemberVM>
    isAuthenticated: boolean
}
type DispatchProps = {
    logout: () => void
    quitSideMenu: () => void
    openMissionCreation: () => void
    openMemberProfile: () => void
    contactUs: () => void
}
type OwnProps = {}
export type Props = StateProps & DispatchProps & OwnProps

const mapStateToProps = (state: AppState): StateProps => ({
    t                : (key: I18nKeys) => t(state)(key),
    capitalize       : (key: I18nKeys) => capitalize(state)(key),
    isLoading        : getMemberInformationIsLoading(state),
    memberInformation: MemberPresenter.present(getMemberInformation(state)),
    isAuthenticated  : isAuthenticated(state)
})

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => bindActionCreators(
    {
        logout             : gatekeeperActions.Actions.logOut,
        quitSideMenu       : navigationActions.Actions.quitSideMenu,
        openMissionCreation: navigationActions.Actions.openMissionCreation,
        openMemberProfile  : navigationActions.Actions.openMemberProfile,
        contactUs          : navigationActions.Actions.contactUs
    },
    dispatch
)

export const SideMenu = connect<StateProps, DispatchProps, OwnProps, AppState>(
    mapStateToProps,
    mapDispatchToProps
)(SideMenuContainer)
