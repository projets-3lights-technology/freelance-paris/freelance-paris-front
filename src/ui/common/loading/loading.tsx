import React, { FunctionComponent } from 'react'
import { ActivityIndicator, StyleSheet, View, ViewStyle } from 'react-native'
import { AppTheme } from '../../../assets/appTheme'

export const Loading: FunctionComponent = () => (
    <View style={styles.container}><ActivityIndicator size={'large'} color={AppTheme.mulledWine}/></View>
)

type Styles = {
    container: ViewStyle
}
const styles = StyleSheet.create<Styles>({
    container: {
        flex          : 1,
        justifyContent: 'center',
        alignItems    : 'center'
    }
})
