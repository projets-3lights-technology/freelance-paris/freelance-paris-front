module.exports = {
    preset: 'react-native',
    rootDir: '..',
    resetMocks: true,
    setupFiles: [
        '<rootDir>/configuration/jest.setup.js'
    ],
    snapshotSerializers: [
        'enzyme-to-json/serializer'
    ],
    moduleFileExtensions: [
        'ts',
        'tsx',
        'js'
    ],
    moduleNameMapper: {
        '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/configuration/jest.assetsTransformer.js',
        '\\.(css|less)$': '<rootDir>/configuration/jest.assetsTransformer.js'
    },
    transform: {
        '^.+\\.js$': '<rootDir>/node_modules/react-native/jest/preprocessor.js',
        '\\.(ts|tsx)$': 'ts-jest'
    },
    transformIgnorePatterns: [
        '<rootDir>/node_modules/(?!native-base|native-base-shoutem-AppTheme|react-native|react-navigation)'
    ],
    testMatch: [
        '<rootDir>/__tests__/**/?(*.)+(spec|test).ts?(x)'
    ],
    testPathIgnorePatterns: [
        '\\.snap$',
        '<rootDir>/node_modules/'
    ],
    collectCoverageFrom: [
        '<rootDir>/src/**/*.{ts,tsx}',
        '!<rootDir>/src/**/inmemory/*.ts',
        '!<rootDir>/src/configuration/**/*.ts'
    ],
    coveragePathIgnorePatterns: [
        '<rootDir>/__tests__/'
    ],
    coverageDirectory: '<rootDir>/jest/coverage',
    coverageReporters: [
        'html',
        'text',
        'text-summary'
    ]
}
