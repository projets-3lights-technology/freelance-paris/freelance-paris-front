import * as shell from 'shelljs'

type PlayTrack = 'internal' | 'alpha' | 'beta' | 'rollout' | 'production'

const APP_VERSION_DEFAULT = '1.0.0'
const APP_BUILD_NUMBER_DEFAULT = '12'

const APP_PLAY_TRACK_DEFAULT = 'internal'

const extractArg = (arg: string) => (defaultValue: string): any => {
    const modeArg = process.argv.filter(a => a.match(`${arg}:`))[0]
    return modeArg ? modeArg.split(':')[1] : defaultValue
}

const APP_VERSION = extractArg('appVersion')(APP_VERSION_DEFAULT)
const APP_BUILD_NUMBER = extractArg('appBuildNumber')(APP_BUILD_NUMBER_DEFAULT)

const APP_ANDROID_DIR = 'android/'
const APP_PLAY_TRACK: PlayTrack = extractArg('track')(APP_PLAY_TRACK_DEFAULT)
const APP_ENV_FILE = '../configuration/env/.env.' + extractArg('mode')('production')

shell.echo('=== START GOOGLE PLAY ===')

shell.echo('=== UPDATE gradle properties')
shell.sed('-i', /^.*BUILD_VERSION_NAME.*$/, 'BUILD_VERSION_NAME=' + APP_VERSION, APP_ANDROID_DIR + '/gradle.properties')
shell.sed('-i', /^.*BUILD_VERSION_CODE.*$/, 'BUILD_VERSION_CODE=' + APP_BUILD_NUMBER, APP_ANDROID_DIR + '/gradle.properties')
shell.sed('-i', /^.*PLAY_TRACK.*$/, 'PLAY_TRACK=' + APP_PLAY_TRACK, APP_ANDROID_DIR + '/gradle.properties')

shell.echo('** UPDATE SUCCEEDED **')

shell.echo('=== SEND TO GOOGLE PLAY')
shell.cd(APP_ANDROID_DIR)
shell.exec('ENVFILE=' + APP_ENV_FILE + ' ./gradlew publishApkRelease')

shell.exec('git commit -anm "Bump Android version to ' + APP_VERSION + ' (' + APP_BUILD_NUMBER + ')" --allow-empty')
shell.exec('git push origin HEAD')

shell.echo('=== FIN GOOGLE PLAY ===')

shell.exit(0)
