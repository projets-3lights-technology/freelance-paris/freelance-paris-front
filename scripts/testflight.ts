import * as shell from 'shelljs'

const APP_VERSION_DEFAULT = '1.0.0'
const APP_BUILD_NUMBER_DEFAULT = '2'

const extractArg = (arg: string) => (defaultValue: string): string => {
    const modeArg = process.argv.filter(a => a.match(`${arg}:`))[0]
    return modeArg ? modeArg.split(':')[1] : defaultValue
}

const APP_VERSION = extractArg('appVersion')(APP_VERSION_DEFAULT)
const APP_BUILD_NUMBER = extractArg('appBuildNumber')(APP_BUILD_NUMBER_DEFAULT)

const APP_IOS_DIR = 'ios/'
const APP_WORKSPACE = 'freelanceparis'
const APP_PROD_SCHEME = 'freelanceparis-' + extractArg('mode')('production')
const APP_NAME = 'Freelance-Paris'

const APP_ITUNES_LOGIN = 'mickael.wegerich@me.com'
const APP_ITUNES_PWD = 'G1z1taw23'

shell.echo('=== START TESTFLIGHT ===')

shell.echo('=== UPDATE info.plist')
shell.exec('/usr/libexec/Plistbuddy ' +
    '-c "Set CFBundleShortVersionString ' + APP_VERSION + '" ' +
    '-c "Set CFBundleVersion ' + APP_BUILD_NUMBER + '" ' +
    '' + APP_IOS_DIR + APP_WORKSPACE + '/Info.plist')
shell.echo('** UPDATE SUCCEEDED **')

shell.echo('=== CREATE Archive')
shell.exec('xcodebuild -workspace ' + APP_IOS_DIR + APP_WORKSPACE + '.xcworkspace ' +
    '-scheme ' + APP_PROD_SCHEME + ' -sdk iphoneos -configuration AppStoreDistribution archive ' +
    '-archivePath ' + APP_IOS_DIR + 'build/' + APP_NAME + '.xcarchive')

shell.echo('=== CREATE .ipa')
shell.exec('xcodebuild -exportArchive -allowProvisioningUpdates ' +
    '-archivePath ' + APP_IOS_DIR + 'build/' + APP_NAME + '.xcarchive ' +
    '-exportOptionsPlist ' + APP_IOS_DIR + 'configuration/exportOptions.plist ' +
    '-exportPath ' + APP_IOS_DIR + 'build/')

shell.echo('=== SEND TO TESTFLIGHT')
shell.exec('/Applications/Xcode.app/Contents/Applications/Application\\ Loader.app/Contents/Frameworks/ITunesSoftwareService.framework/Support/altool ' +
    '--upload-app -f ' + APP_IOS_DIR + 'build/' + APP_PROD_SCHEME + '.ipa ' +
    '-u ' + APP_ITUNES_LOGIN + ' ' +
    '-p ' + APP_ITUNES_PWD)

shell.exec('git commit -anm "Bump iOS version to ' + APP_VERSION + ' (' + APP_BUILD_NUMBER + ')" --allow-empty')
shell.exec('git push origin HEAD')

shell.echo('=== FIN TESTFLIGHT ===')

shell.exit(0)
