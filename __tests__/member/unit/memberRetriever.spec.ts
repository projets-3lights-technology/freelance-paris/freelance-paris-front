import lolex from 'lolex'
import { compose, lensPath, set } from 'ramda'
import { Store } from 'redux'
import { InMemoryLocalStorage } from '../../../src/app/common/adapters/gateways/inmemory/inMemory.localStorage'
import { Token } from '../../../src/app/common/domain/entities/token'
import { appAnalytics } from '../../../src/app/common/usecases/analytics/app.analytics'
import * as gatekeeperActions from '../../../src/app/common/usecases/gatekeeper/gatekeeper.actions'
import { InMemoryMemberRepository } from '../../../src/app/member/adapters/gateways/inmemory/inMemory.memberRepository'
import { Member } from '../../../src/app/member/domain/entities/member'
import { MemberInformationState } from '../../../src/app/member/domain/MemberState.redux'
import * as memberInformationActions from '../../../src/app/member/usecases/memberinformation/memberInformation.actions'
import { appEpics } from '../../../src/configuration/redux/rootEpicMiddleware.redux'
import {
    AppActionsType,
    getMemberInformation,
    getMemberInformationIsLoading
} from '../../../src/configuration/redux/rootReducer.redux'
import { AppState, UIState } from '../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../src/configuration/redux/rootStore.redux'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../testMiddlewareDependencies.redux'
import { StubMemberBuilder } from '../stubMember.builder'

describe('Member retriever', () => {
    let store: Store<AppState, AppActionsType>
    let initialState: AppState
    let inMemoryMemberRepository: InMemoryMemberRepository
    let inMemoryLocalStorage: InMemoryLocalStorage
    let clock: lolex.Clock
    const now = '2018-10-19T20:50:10.000+02:00'

    beforeEach(() => {
        clock = lolex.install({ now: new Date(now), shouldAdvanceTime: true })
        const testDependencies = testEpicsDependencies
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        inMemoryMemberRepository = testDependencies.dependencies.memberRepository as InMemoryMemberRepository
        inMemoryLocalStorage = testDependencies.dependencies.localStorage as InMemoryLocalStorage
        initialState = store.getState()
    })

    afterEach(() => {
        clock.uninstall()
    })

    describe('While fetching', () => {
        it('Informs that the retrieval of member information is on loading', done => {
            expect(getMemberInformationIsLoading(store.getState())).toBeFalsy()

            expectFinalMemberState({
                ...initialState.domain.memberInformation,
                isLoading: true
            }, () => {
                expect(getMemberInformationIsLoading(store.getState())).toBeTruthy()
            }, 1, done)

            dispatchMemberRetrieverActions(memberInformationActions.Actions.fetchMemberInformation())
        })
    })

    it('Fetch member information', done => {
        const member: Member = new StubMemberBuilder()
            .withId('10')
            .withFirstName('first name')
            .withLastName('last name')
            .withEmail('email')
            .withAvatar('avatar')
            .build()
        saveToken(new Token('access_token', 86400))

        expectFinalMemberState({
            ...initialState.domain.memberInformation,
            byId     : {
                [member.id]: {
                    id       : member.id,
                    firstName: member.firstName,
                    lastName : member.lastName,
                    email    : member.email,
                    avatar   : member.avatar
                }
            },
            allIds   : member.id,
            isLoading: false
        }, () => {
            getMemberInformation(store.getState()).ifPresent(value => {
                expect(value).toEqual({
                    id       : '10',
                    firstName: 'first name',
                    lastName : 'last name',
                    email    : 'email',
                    avatar   : 'avatar'
                })
            })
            expect(getMemberInformationIsLoading(store.getState())).toBeFalsy()
        }, 2, done, {
            gatekeeper: {
                token: {
                    access_token: 'access_token',
                    expires_at  : '2018-10-20T19:50:10.000+02:00'
                }
            }
        })

        dispatchMemberRetrieverActions(memberInformationActions.Actions.fetchMemberInformation())
        inMemoryMemberRepository.populateMemberInformation(member)
    })

    it('Notify if there is an error during the fetching', done => {
        expectFinalMemberState({
            ...initialState.domain.memberInformation,
            error: 'error'
        }, null, 2, done)
        dispatchMemberRetrieverActions(memberInformationActions.Actions.fetchMemberInformation())
        inMemoryMemberRepository.memberInformationOnError('error')
    })

    function saveToken(token: Token) {
        store.dispatch(gatekeeperActions.Actions.logIn(token))
        inMemoryLocalStorage.triggerSaveToken()
    }

    function dispatchMemberRetrieverActions(action: memberInformationActions.Actions) {
        store.dispatch<memberInformationActions.Actions>(action)
    }

    function expectFinalMemberState(expectedMemberInformationState: MemberInformationState,
                                    selectors: null | (() => void),
                                    eventCount: number,
                                    done: () => void,
                                    extendState: any = {}) {
        let event = 0
        store.subscribe(() => {
            if (++event === eventCount) {
                expectState({
                    ui    : appStateWithoutNav(),
                    domain: {
                        ...initialState.domain,
                        ...extendState,
                        memberInformation: expectedMemberInformationState
                    }
                })
                if (selectors)
                    selectors()
                done()
            }
        })
    }

    function expectState(expectedState: AppState) {
        const sanitize = compose(
            set(lensPath(['ui', 'navigation']), {}),
            set(lensPath(['ui', 'userNavigation']), null)
        )

        expect(sanitize(store.getState())).toEqual(expectedState)
    }

    function appStateWithoutNav(): UIState {
        return {
            ...initialState.ui,
            userNavigation: null,
            navigation    : {}
        }
    }
})
