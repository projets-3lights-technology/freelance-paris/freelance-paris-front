import { Store } from 'redux'
import { appAnalytics } from '../../../src/app/common/usecases/analytics/app.analytics'
import { InMemoryMemberRepository } from '../../../src/app/member/adapters/gateways/inmemory/inMemory.memberRepository'
import { Member } from '../../../src/app/member/domain/entities/member'
import { MemberForModification } from '../../../src/app/member/domain/entities/memberForModification'
import { MemberForModificationBuilder } from '../../../src/app/member/domain/entities/memberForModification.builder'
import { MemberModificationState } from '../../../src/app/member/domain/MemberState.redux'
import * as memberInformationActions from '../../../src/app/member/usecases/memberinformation/memberInformation.actions'
import * as memberModificationActions from '../../../src/app/member/usecases/membermodification/memberModification.actions'
import { appEpics } from '../../../src/configuration/redux/rootEpicMiddleware.redux'
import {
    AppActionsType,
    getMemberModificationHasError,
    getMemberModificationIsLoading,
    getMemberUnderModification,
    getMemberUnderModificationError
} from '../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../src/configuration/redux/rootStore.redux'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../testMiddlewareDependencies.redux'
import { StubMemberBuilder } from '../stubMember.builder'

describe('Member modification', () => {
    let store: Store<AppState, AppActionsType>
    let initialState: AppState
    let inMemoryMemberRepository: InMemoryMemberRepository

    beforeEach(() => {
        const testDependencies = testEpicsDependencies
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        inMemoryMemberRepository = testDependencies.dependencies.memberRepository as InMemoryMemberRepository
        initialState = store.getState()
    })

    describe('Member modification', () => {
        it('Change any information about user', done => {
            expectFinalMemberState({
                ...initialState.domain.memberModification,
                memberUnderModification     : {
                    email: 'email@email.com'
                },
                memberUnderModificationError: {
                    email: ''
                }
            }, () => {
                expect(getMemberModificationHasError(store.getState())).toBeFalsy()
                expect(getMemberUnderModification(store.getState())).toEqual({
                    email: 'email@email.com'
                })
            }, 1, done)

            fillAllMemberInformation()
        })

        describe('Validation', () => {
            it('Email is required', done => {
                expectFinalMemberState({
                    ...initialState.domain.memberModification,
                    memberUnderModificationError: {
                        email: 'required'
                    }
                }, () => {
                    expect(getMemberModificationHasError(store.getState())).toBeTruthy()
                    expect(getMemberUnderModificationError(store.getState())).toEqual({
                        email: 'required'
                    })
                }, 2, done)

                dispatchMemberModificationActions(memberModificationActions.Actions.updateMember())
                inMemoryMemberRepository.triggerMemberModification()
                expect(inMemoryMemberRepository.updateWith).toBeNull()
            })

            it('Email is invalid', done => {
                fillAllMemberInformation('invalid email')

                expectFinalMemberState({
                    ...initialState.domain.memberModification,
                    memberUnderModification     : {
                        email: 'invalid email'
                    },
                    memberUnderModificationError: {
                        email: 'invalid'
                    }
                }, () => {
                    expect(getMemberModificationHasError(store.getState())).toBeTruthy()
                    expect(getMemberUnderModificationError(store.getState())).toEqual({
                        email: 'invalid'
                    })
                }, 2, done)

                dispatchMemberModificationActions(memberModificationActions.Actions.updateMember())
                inMemoryMemberRepository.triggerMemberModification()
                expect(inMemoryMemberRepository.updateWith).toBeNull()
            })
        })

        it('Reset all information about the mission', done => {
            fillAllMemberInformation()
            expect(getMemberUnderModification(store.getState())).toEqual({
                email: 'email@email.com'
            })

            expectFinalMemberState({
                ...initialState.domain.memberModification
            }, null, 1, done)

            dispatchMemberModificationActions(memberModificationActions.Actions.resetMemberModification())
        })
    })

    describe('While creating', () => {
        it('Inform that the member modification is on loading', done => {
            expect(getMemberModificationIsLoading(store.getState())).toBeFalsy()
            expectFinalMemberState({
                ...initialState.domain.memberModification,
                isLoading: true
            }, null, 1, done)
            dispatchMemberModificationActions(memberModificationActions.Actions.updateMember())
        })
    })

    describe('After updating member', () => {
        beforeEach(() => {
            const member: Member = new StubMemberBuilder().withId('10').build()
            fetchMember(member)
            fillAllMemberInformation()
        })

        it('Update the current member', done => {
            const memberForModification = new MemberForModificationBuilder()
                .withId('10')
                .withEmail('email@email.com')
                .build()

            expectFinalMemberState({
                ...initialState.domain.memberModification
            }, null, 3, done, {
                memberInformation: {
                    ...initialState.domain.memberInformation,
                    allIds: '10',
                    byId  : {
                        10: {
                            id       : '10',
                            firstName: 'first name',
                            lastName : 'last name',
                            email    : 'email@email.com',
                            avatar   : 'avatar'
                        }
                    }
                }
            })

            dispatchMemberModificationActions(memberModificationActions.Actions.updateMember())
            inMemoryMemberRepository.triggerMemberModification()
            expectMemberForModification(memberForModification)
        })

        it('Notify if there is an error during the updating', done => {
            expectFinalMemberState({
                ...initialState.domain.memberModification,
                memberUnderModification: {
                    email: 'email@email.com'
                },
                error                  : 'error'
            }, null, 2, done, {
                memberInformation: {
                    ...initialState.domain.memberInformation,
                    allIds: '10',
                    byId  : {
                        10: {
                            id       : '10',
                            firstName: 'first name',
                            lastName : 'last name',
                            email    : 'email',
                            avatar   : 'avatar'
                        }
                    }
                }
            })

            dispatchMemberModificationActions(memberModificationActions.Actions.updateMember())
            inMemoryMemberRepository.updateMemberOnError('error')
        })

        function fetchMember(member: Member) {
            store.dispatch(memberInformationActions.Actions.fetchMemberInformation())
            inMemoryMemberRepository.populateMemberInformation(member)
        }
    })

    function dispatchMemberModificationActions(actions: memberModificationActions.Actions) {
        store.dispatch<memberModificationActions.Actions>(actions)
    }

    function expectFinalMemberState(expectedMemberState: MemberModificationState,
                                    selectors: null | (() => void),
                                    eventCount: number,
                                    done: () => void,
                                    extendState: any = {}) {
        let event = 0
        store.subscribe(() => {
            if (++event === eventCount) {
                expectState({
                    ...initialState,
                    domain: {
                        ...initialState.domain,
                        ...extendState,
                        memberModification: expectedMemberState
                    }
                })
                if (selectors)
                    selectors()
                done()
            }
        })
    }

    function expectState(expectedState: AppState) {
        expect(store.getState()).toEqual(expectedState)
    }

    function fillAllMemberInformation(email: string = 'email@email.com') {
        dispatchMemberModificationActions(memberModificationActions.Actions.changeMemberInformation('email', email))
    }

    function expectMemberForModification(expectedMemberForModification: MemberForModification) {
        expect(inMemoryMemberRepository.updateWith.id).toEqual(expectedMemberForModification.id)
        expect(inMemoryMemberRepository.updateWith.email).toEqual(expectedMemberForModification.email)
    }
})
