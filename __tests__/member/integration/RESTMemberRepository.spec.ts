import spyOn = jest.spyOn
import { of } from 'rxjs'
import { ajax } from 'rxjs/ajax'
import { InMemoryLocalStorage } from '../../../src/app/common/adapters/gateways/inmemory/inMemory.localStorage'
import { AuthorizedHttpClient } from '../../../src/app/common/adapters/gateways/real/AuthorizedHttpClient'
import { ObservableHttpClient } from '../../../src/app/common/adapters/gateways/real/observable.httpClient'
import { Token } from '../../../src/app/common/domain/entities/token'
import { MemberMeDTO } from '../../../src/app/member/adapters/gateways/real/DTO/MemberMeDTO'
import { RESTMemberRepository } from '../../../src/app/member/adapters/gateways/real/REST.memberRepository'
import { MemberBuilder } from '../../../src/app/member/domain/entities/member.builder'
import { MemberRepository } from '../../../src/app/member/domain/MemberRepository'
import { InMemoryConfig } from '../../testMiddlewareDependencies.redux'
import { StubMissionForModificationBuilder } from '../stubMemberForModification.builder'

describe('Integration | Member repository', () => {
    let memberRepository: MemberRepository

    beforeEach(() => {
        const inMemoryLocalStorage = new InMemoryLocalStorage(false, new Token('access_token', 10))
        const httpClient: AuthorizedHttpClient = new ObservableHttpClient(inMemoryLocalStorage)
        memberRepository = new RESTMemberRepository(httpClient, new InMemoryConfig())
    })

    it('Retrieve member information', done => {
        const fakeMember: MemberMeDTO = {
            id        : 'id',
            first_name: 'first name',
            last_name : 'last name',
            email     : 'email',
            avatar    : 'avatar'
        }

        spyOn(ajax, 'getJSON').mockReturnValue(of(fakeMember))

        memberRepository.getMemberInformation().subscribe(member => {
            expect(member).toEqual(new MemberBuilder()
                .withId('id')
                .withFirstName('first name')
                .withLastName('last name')
                .withEmail('email')
                .withAvatar('avatar')
                .build())
            expect(ajax.getJSON).toHaveBeenCalledTimes(1)
            expect(ajax.getJSON).toHaveBeenCalledWith(
                'https://api_url.com/me',
                {
                    'Content-Type' : 'application/json',
                    'Authorization': 'Bearer access_token'
                }
            )
            done()
        })
    })

    it('Update member information', done => {
        const memberForModification = new StubMissionForModificationBuilder()
            .withEmail('new email')
            .build()

        spyOn(ajax, 'patch').mockReturnValue(of(undefined))

        memberRepository.update(memberForModification).subscribe(() => {
            expect(ajax.patch).toHaveBeenCalledTimes(1)
            expect(ajax.patch).toHaveBeenCalledWith(
                'https://api_url.com/me/mail',
                { value: 'new email' },
                {
                    'Content-Type' : 'application/x-www-form-urlencoded',
                    'Authorization': 'Bearer access_token'
                }
            )
            done()
        })
    })

    it('Unsubscribe', done => {
        spyOn(ajax, 'delete').mockReturnValue(of(undefined))

        memberRepository.unsubscribe().subscribe(() => {
            expect(ajax.delete).toHaveBeenCalledTimes(1)
            expect(ajax.delete).toHaveBeenCalledWith(
                'https://api_url.com/me',
                {
                    'Content-Type' : 'application/json',
                    'Authorization': 'Bearer access_token'
                }
            )
            done()
        })
    })
})
