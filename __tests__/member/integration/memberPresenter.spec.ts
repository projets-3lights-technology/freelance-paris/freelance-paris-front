import { Optional } from 'typescript-optional'
import { MemberInformationById } from '../../../src/app/member/domain/MemberState.redux'
import { AppAssets } from '../../../src/assets/appAssets'
import { MemberPresenter } from '../../../src/ui/login/presenters/member.present'
import { MemberVM } from '../../../src/ui/login/viewmodels/member.viewmodel'

describe('Integration | Member presenter', () => {
    it('Presents member information', () => {
        const memberInformation: MemberInformationById = {
            id       : 'kNoRpiOaf5',
            firstName: 'first name',
            lastName : 'last name',
            email    : 'email',
            avatar   : 'avatar'
        }

        const presentedMemberInformation: Optional<MemberVM> = MemberPresenter.present(Optional.ofNullable(memberInformation))

        presentedMemberInformation.ifPresent(value => {
            expect(value.id).toEqual('kNoRpiOaf5')
            expect(value.fullName).toEqual('first name last name')
            expect(value.email).toEqual('email')
            expect(value.avatar).toEqual({ uri: 'avatar' })
        })
    })

    it('Presents member information with default avatar', () => {
        const memberInformation: MemberInformationById = {
            id       : 'kNoRpiOaf5',
            firstName: 'first name',
            lastName : 'last name',
            email    : 'email',
            avatar   : ''
        }

        const presentedMemberInformation: Optional<MemberVM> = MemberPresenter.present(Optional.ofNullable(memberInformation))

        presentedMemberInformation.ifPresent(value => expect(value.avatar).toEqual(AppAssets.userDefault))
    })
})
