import { MemberForModificationBuilder } from '../../src/app/member/domain/entities/memberForModification.builder'

export class StubMissionForModificationBuilder extends MemberForModificationBuilder {

    protected _id: string = '1'
    protected _email: string = 'email'

}
