import { MemberVMBuilder } from '../../src/ui/login/viewmodels/memberVM.builder'

export class StubMemberVMBuilder extends MemberVMBuilder {

    protected _id: string = '1'
    protected _firstName: string = 'first name'
    protected _lastName: string = 'last name'
    protected _email: string = 'email'
    protected _avatar: string = 'avatar'

}
