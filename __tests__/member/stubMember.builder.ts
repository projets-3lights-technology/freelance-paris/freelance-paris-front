import { MemberBuilder } from '../../src/app/member/domain/entities/member.builder'

export class StubMemberBuilder extends MemberBuilder {

    protected _id: string = 'id'
    protected _firstName: string = 'first name'
    protected _lastName: string = 'last name'
    protected _email: string = 'email'
    protected _avatar: string = 'avatar'

}
