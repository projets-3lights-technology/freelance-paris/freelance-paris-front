import { InMemoryAnalyticsLogger } from '../src/app/common/adapters/gateways/inmemory/inMemory.analyticsLogger'
import { InMemoryLocalStorage } from '../src/app/common/adapters/gateways/inmemory/inMemory.localStorage'
import { InMemoryUrlOpener } from '../src/app/common/adapters/gateways/inmemory/inMemory.urlOpener'
import { InMemoryMemberRepository } from '../src/app/member/adapters/gateways/inmemory/inMemory.memberRepository'
import { InMemoryMessageSender } from '../src/app/missions/adapters/gateways/inmemory/InMemory.messageSender'
import { InMemoryMissionsRepository } from '../src/app/missions/adapters/gateways/inmemory/inMemory.missionsRepository'
import { AppConfiguration, ConfigurationKeys } from '../src/configuration/environment/AppConfiguration'
import { AnalyticsDependencies } from '../src/configuration/redux/rootAnalyticsMiddleware.redux'
import { EpicsDependencies } from '../src/configuration/redux/rootEpicMiddleware.redux'

export class InMemoryConfig implements AppConfiguration {

    get(key: ConfigurationKeys): string {
        switch (key) {
            case 'MODE':
                return 'test'

            case 'MAIL_CONTACT':
                return 'support@example.com'

            case 'FREELANCE_API_URL':
                return 'https://api_url.com/'

            default:
                return ''
        }
    }

}

export const testEpicsDependencies: EpicsDependencies = {
    dependencies: {
        configuration     : new InMemoryConfig(),
        missionsRepository: new InMemoryMissionsRepository([], []),
        messageSender     : new InMemoryMessageSender(false),
        localStorage      : new InMemoryLocalStorage(false, null),
        memberRepository  : new InMemoryMemberRepository(),
        urlOpener         : new InMemoryUrlOpener()
    }
}

export const testAnalyticsDependencies: AnalyticsDependencies = {
    analyticsLogger: new InMemoryAnalyticsLogger()
}
