import { shallow, ShallowWrapper } from 'enzyme'
import React from 'react'
import { BackHandler, BackHandlerStatic } from 'react-native'
import { Store } from 'redux'
import { appAnalytics } from '../../../../src/app/common/usecases/analytics/app.analytics'
import * as navigationActions from '../../../../src/app/common/usecases/navigation/navigation.actions'
import { MissionContractType } from '../../../../src/app/missions/domain/entities/missionContractType'
import { MissionDetails } from '../../../../src/app/missions/domain/entities/missionDetails'
import * as missionDetailsActions from '../../../../src/app/missions/usecases/missiondetails/missionDetails.actions'
import { appEpics } from '../../../../src/configuration/redux/rootEpicMiddleware.redux'
import { AppActionsType } from '../../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../../src/configuration/redux/rootStore.redux'
import { Loading } from '../../../../src/ui/common/loading/loading'
import { ModalHeader } from '../../../../src/ui/common/modalheader/modalHeader'
import { MissionDetailsPage } from '../../../../src/ui/missions/missiondetails'
import { MissionDetailsCard } from '../../../../src/ui/missions/missiondetails/missionDetailsCard'
import { StubMissionDetailsBuilder } from '../../../missions/stubMissionDetails.builder'
import { StubMissionDetailsVMBuilder } from '../../../missions/stubMissionDetailsVM.builder'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../../testMiddlewareDependencies.redux'

describe('Component | Missions Details', () => {
    let store: Store<AppState, AppActionsType>
    let missionDetailsConnected: ShallowWrapper
    let missionDetailsWrapper: ShallowWrapper
    let backHandler: BackHandlerStatic

    beforeEach(() => {
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testEpicsDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        backHandler = BackHandler

        jest.spyOn(store, 'dispatch')
        jest.spyOn(backHandler, 'addEventListener')
        jest.spyOn(backHandler, 'removeEventListener')
        store.dispatch(navigationActions.Actions.openMissionDetails('missionId'))
    })

    it('Loading mission details', done => {
        expectFinaleWrapper(() => {
            expectStoreDispatching({
                type   : missionDetailsActions.FETCH_MISSION_DETAILS,
                payload: 'missionId'
            })
            expect(missionDetailsWrapper.find(Loading).exists()).toBeTruthy()
            expect(missionDetailsWrapper.find(ModalHeader).exists()).toBeTruthy()
            expect(backHandler.addEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
        }, 1, done)

        store.dispatch(missionDetailsActions.Actions.fetchMissionDetails('missionId'))
    })

    describe('After fetching mission details', () => {
        const missionDetails: MissionDetails = new StubMissionDetailsBuilder()
            .withId('missionId')
            .withContractType(MissionContractType.FREELANCE)
            .build()

        it('Display information', done => {
            expectFinaleWrapper(() => {
                const missionCard = missionDetailsWrapper.find(MissionDetailsCard)
                const item = new StubMissionDetailsVMBuilder()
                    .withId(missionDetails.id)
                    .withName(missionDetails.name)
                    .withDescription(missionDetails.description)
                    .build()

                expect(missionCard.exists()).toBeTruthy()
                expect(missionCard.props()).toEqual({
                    mission           : item,
                    openMissionContact: expect.any(Function),
                    t                 : expect.any(Function),
                    capitalize        : expect.any(Function),
                    canDisplayContact : true
                })
            }, 1, done)

            store.dispatch(missionDetailsActions.Actions.missionDetailsFetched(missionDetails))
        })

        it('Access to the chat of the mission', done => {
            expectFinaleWrapper(() => {
                const missionCard = missionDetailsWrapper.find(MissionDetailsCard)

                missionCard.props().openMissionContact()
                expectStoreDispatching({
                    type: navigationActions.OTHER,
                    name: navigationActions.OPEN_MISSION_DETAILS_CONTACT
                })
            }, 1, done)

            store.dispatch(missionDetailsActions.Actions.missionDetailsFetched(missionDetails))
        })

        it('Quit mission details', done => {
            expectFinaleWrapper(() => {
                missionDetailsWrapper.find(ModalHeader).simulate('leftClick')

                expectStoreDispatching({
                    type: navigationActions.BACK,
                    name: navigationActions.QUIT_MISSION_DETAILS
                })

                missionDetailsWrapper.unmount()
                expect(backHandler.removeEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
                expectStoreDispatching({
                    type: missionDetailsActions.ABORT_MISSION_DETAILS_FETCHING
                })
            }, 1, done)

            store.dispatch(missionDetailsActions.Actions.missionDetailsFetched(missionDetails))
        })
    })

    function expectFinaleWrapper(fn: () => void, eventCount: number, done: () => void) {
        let event = 0
        store.subscribe(() => {
            if (++event === eventCount) {
                runComponent()
                fn()
                done()
            }
        })
    }

    function expectStoreDispatching(expectedAction: any) {
        expect(store.dispatch).toHaveBeenCalledWith(expectedAction)
    }

    function runComponent() {
        missionDetailsConnected = shallow(<MissionDetailsPage/>, { context: { store } })
        missionDetailsWrapper = missionDetailsConnected.dive()
    }
})
