import { shallow, ShallowWrapper } from 'enzyme'
import { Button, Textarea } from 'native-base'
import React from 'react'
import { MessageContactForm } from '../../../../../src/ui/missions/missiondetails/missioncontact/messageContactForm'
import Mock = jest.Mock

describe('Component | Mission contact form', () => {
    let capitalize: Mock
    const onChangObject: Mock = jest.fn()
    const onChangeMessage: Mock = jest.fn()
    const onCancel: Mock = jest.fn()
    const onSubmit: Mock = jest.fn()

    let messageContactFormWrapper: ShallowWrapper

    beforeEach(() => {
        capitalize = jest.fn().mockImplementation((key: string) => key.charAt(0).toUpperCase() + key.slice(1))
        messageContactFormWrapper = shallow(<MessageContactForm
            capitalize={capitalize}
            onChangeObject={onChangObject}
            onChangeMessage={onChangeMessage}
            onSubmit={onSubmit}
            onCancel={onCancel}
        />)
    })

    it('Display form', () => {
        expect(messageContactFormWrapper).toMatchSnapshot()
    })

    it('Change message', () => {
        messageContactFormWrapper.find(Textarea).simulate('changeText', 'message')

        expect(onChangeMessage).toHaveBeenCalledTimes(1)
        expect(onChangeMessage).toHaveBeenCalledWith('message')
    })

    it('Close it', () => {
        messageContactFormWrapper.find(Button).at(0).simulate('press')

        expect(onCancel).toHaveBeenCalledTimes(1)
    })

    it('Submit it', () => {
        messageContactFormWrapper.find(Button).at(1).simulate('press')

        expect(onSubmit).toHaveBeenCalledTimes(1)
    })
})
