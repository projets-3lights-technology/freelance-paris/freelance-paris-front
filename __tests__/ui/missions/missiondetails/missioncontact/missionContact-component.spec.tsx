import { shallow, ShallowWrapper } from 'enzyme'
import React from 'react'
import { BackHandler, BackHandlerStatic } from 'react-native'
import Modal from 'react-native-modal'
import { Store } from 'redux'
import { appAnalytics } from '../../../../../src/app/common/usecases/analytics/app.analytics'
import * as navigationActions from '../../../../../src/app/common/usecases/navigation/navigation.actions'
import * as memberInformationActions from '../../../../../src/app/member/usecases/memberinformation/memberInformation.actions'
import * as missionContactActions from '../../../../../src/app/missions/usecases/missioncontact/missionContact.actions'
import { appEpics } from '../../../../../src/configuration/redux/rootEpicMiddleware.redux'
import { AppActionsType } from '../../../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../../../src/configuration/redux/rootStore.redux'
import { MissionContactModal } from '../../../../../src/ui/missions/missiondetails/missioncontact'
import { MessageContactForm } from '../../../../../src/ui/missions/missiondetails/missioncontact/messageContactForm'
import { MissionContactSuccessful } from '../../../../../src/ui/missions/missiondetails/missioncontact/missionContactSuccessful'
import { StubMemberBuilder } from '../../../../member/stubMember.builder'
import { StubMessageBuilder } from '../../../../missions/stubMessage.builder'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../../../testMiddlewareDependencies.redux'

describe('Component | Missions Contact', () => {
    let store: Store<AppState, AppActionsType>
    let missionContactConnected: ShallowWrapper
    let missionContactWrapper: ShallowWrapper
    let backHandler: BackHandlerStatic

    beforeEach(() => {
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testEpicsDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        backHandler = BackHandler

        jest.spyOn(store, 'dispatch')
        jest.spyOn(backHandler, 'addEventListener')
        jest.spyOn(backHandler, 'removeEventListener')
        store.dispatch(navigationActions.Actions.openMissionDetails('missionId'))
        store.dispatch(memberInformationActions.Actions.memberInformationFetched(new StubMemberBuilder().withId('senderId').build()))
        store.dispatch(navigationActions.Actions.openMissionContact())
        runComponent()
    })

    it('Display mission contact', () => {
        const missionContact = missionContactWrapper.find(Modal)
        expect(missionContact.props().isVisible).toBeTruthy()
        expect(backHandler.addEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
    })

    describe('Quit mission contact', () => {
        it('From modal back drop', () => {
            missionContactWrapper.find(Modal).simulate('backdropPress')

            expectStoreDispatching({
                type: navigationActions.OTHER,
                name: navigationActions.QUIT_MISSION_DETAILS_CONTACT
            })
            expect(backHandler.removeEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
        })

        it('From back button', () => {
            missionContactWrapper.find(Modal).simulate('backButtonPress')

            expectStoreDispatching({
                type: navigationActions.OTHER,
                name: navigationActions.QUIT_MISSION_DETAILS_CONTACT
            })
            expect(backHandler.removeEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
        })

        it('From button', () => {
            missionContactWrapper.find(MessageContactForm).simulate('cancel')

            expectStoreDispatching({
                type: navigationActions.OTHER,
                name: navigationActions.QUIT_MISSION_DETAILS_CONTACT
            })
            expect(backHandler.removeEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
        })
    })

    describe('Send a contact', () => {
        beforeEach(() => {
            missionContactWrapper.setState({
                object : 'object',
                message: 'message'
            })
            expect(missionContactWrapper.find(MissionContactSuccessful).exists()).toBeFalsy()
            missionContactWrapper.find(MessageContactForm).simulate('submit')
        })

        it('Everything is good', () => {
            expectStoreDispatching({
                type   : missionContactActions.SEND_MISSION_CONTACT,
                payload: new StubMessageBuilder()
                    .withMissionId('missionId')
                    .withSenderId('senderId')
                    .withObject('object')
                    .withMessage('message')
                    .build()
            })
            expect(missionContactWrapper.state()).toEqual({
                object : 'object',
                message: 'message',
                isSent : true
            })

            expect(missionContactWrapper.find(MissionContactSuccessful).exists()).toBeTruthy()
        })

        it('Close modal', () => {
            missionContactWrapper.find(MissionContactSuccessful).simulate('close')
            expectStoreDispatching({
                type: navigationActions.OTHER,
                name: navigationActions.QUIT_MISSION_DETAILS_CONTACT
            })
        })
    })

    function expectStoreDispatching(expectedAction: any) {
        expect(store.dispatch).toHaveBeenCalledWith(expectedAction)
    }

    function runComponent() {
        missionContactConnected = shallow(<MissionContactModal/>, { context: { store } })
        missionContactWrapper = missionContactConnected.dive()
    }
})
