import { shallow } from 'enzyme'
import { Button } from 'native-base'
import React from 'react'
import { MissionContactSuccessful } from '../../../../../src/ui/missions/missiondetails/missioncontact/missionContactSuccessful'
import Mock = jest.Mock

describe('Component | Mission contact successful', () => {
    let t: Mock
    let capitalize: Mock
    const onClose: Mock = jest.fn()

    beforeEach(() => {
        t = jest.fn().mockImplementation((key: string) => key)
        capitalize = jest.fn().mockImplementation((key: string) => key.charAt(0).toUpperCase() + key.slice(1))
    })

    it('Display message', () => {
        const wrapper = shallow(<MissionContactSuccessful
            t={t}
            capitalize={capitalize}
            onClose={onClose}
        />)

        expect(wrapper).toMatchSnapshot()
    })

    it('Close it', () => {
        const wrapper = shallow(<MissionContactSuccessful
            t={t}
            capitalize={capitalize}
            onClose={onClose}
        />)

        wrapper.find(Button).simulate('press')

        expect(onClose).toHaveBeenCalledTimes(1)
    })
})
