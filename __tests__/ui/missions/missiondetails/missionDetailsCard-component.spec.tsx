import { shallow } from 'enzyme'
import { Button } from 'native-base'
import React from 'react'
import { MissionDetailsCard } from '../../../../src/ui/missions/missiondetails/missionDetailsCard'
import { MissionDetailsVM } from '../../../../src/ui/missions/viewmodels/missionDetails.viewmodel'
import Mock = jest.Mock
import { StubMissionDetailsVMBuilder } from '../../../missions/stubMissionDetailsVM.builder'

describe('Component | Missions Details card', () => {
    let mission: MissionDetailsVM
    let openChatOfTheMission: Mock
    let t: Mock
    let capitalize: Mock

    beforeEach(() => {
        mission = new StubMissionDetailsVMBuilder().build()
        openChatOfTheMission = jest.fn()
        t = jest.fn().mockImplementation((key: string) => key)
        capitalize = jest.fn().mockImplementation((key: string) => key.charAt(0).toUpperCase() + key.slice(1))
    })

    it('Display an office mission from listing', () => {
        const wrapper = shallow(<MissionDetailsCard
            mission={mission}
            openMissionContact={openChatOfTheMission}
            t={t}
            capitalize={capitalize}
            canDisplayContact={true}
        />)

        expect(wrapper).toMatchSnapshot()
    })

    it('Display a remote mission from listing', () => {
        mission = new StubMissionDetailsVMBuilder().withRemoteIsPossible(true).build()
        const wrapper = shallow(<MissionDetailsCard
            mission={mission}
            openMissionContact={openChatOfTheMission}
            t={t}
            capitalize={capitalize}
            canDisplayContact={true}
        />)

        expect(wrapper).toMatchSnapshot()
    })

    it('Display an already contacted mission from listing', () => {
        mission = new StubMissionDetailsVMBuilder().withAlreadyContacted(true).build()
        const wrapper = shallow(<MissionDetailsCard
            mission={mission}
            openMissionContact={openChatOfTheMission}
            t={t}
            capitalize={capitalize}
            canDisplayContact={true}
        />)

        expect(wrapper).toMatchSnapshot()
    })

    it('Display a mission details for guest user', () => {
        mission = new StubMissionDetailsVMBuilder().withAlreadyContacted(true).build()
        const wrapper = shallow(<MissionDetailsCard
            mission={mission}
            openMissionContact={openChatOfTheMission}
            t={t}
            capitalize={capitalize}
            canDisplayContact={false}
        />)

        expect(wrapper).toMatchSnapshot()
    })

    it('Inform when the user wants more information about the mission', () => {
        const wrapper = shallow(<MissionDetailsCard
            mission={mission}
            openMissionContact={openChatOfTheMission}
            t={t}
            capitalize={capitalize}
            canDisplayContact={true}
        />)

        wrapper.find(Button).simulate('press')

        expect(openChatOfTheMission).toHaveBeenCalledTimes(1)
    })
})
