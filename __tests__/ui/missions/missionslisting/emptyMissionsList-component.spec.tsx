import { shallow } from 'enzyme'
import React from 'react'
import { EmptyMissionsList } from '../../../../src/ui/missions/missionslisting/item/EmptyMissionsList'

describe('Component | Empty mission list', () => {
    it('Render user indication', () => {
        const wrapper = shallow(<EmptyMissionsList message={'message'}/>)

        expect(wrapper).toMatchSnapshot()
    })
})
