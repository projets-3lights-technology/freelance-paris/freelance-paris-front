import { shallow, ShallowWrapper } from 'enzyme'
import React from 'react'
import { FlatList } from 'react-native'
import { Store } from 'redux'
import { appAnalytics } from '../../../../src/app/common/usecases/analytics/app.analytics'
import { InMemoryMemberRepository } from '../../../../src/app/member/adapters/gateways/inmemory/inMemory.memberRepository'
import { Member } from '../../../../src/app/member/domain/entities/member'
import * as memberInformationActions from '../../../../src/app/member/usecases/memberinformation/memberInformation.actions'
import { MissionHeader } from '../../../../src/app/missions/domain/entities/missionHeader'
import * as missionListingActions from '../../../../src/app/missions/usecases/missionslisting/missionsListing.actions'
import { appEpics } from '../../../../src/configuration/redux/rootEpicMiddleware.redux'
import { AppActionsType } from '../../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../../src/configuration/redux/rootStore.redux'
import { ApplicationHeader } from '../../../../src/ui/common/applicationheader'
import { Loading } from '../../../../src/ui/common/loading/loading'
import { MissionsListingPage } from '../../../../src/ui/missions/missionslisting'
import { StubMemberBuilder } from '../../../member/stubMember.builder'
import { StubMissionHeaderBuilder } from '../../../missions/stubMissionHeader.builder'
import { StubMissionHeaderVMBuilder } from '../../../missions/stubMissionHeaderVM.builder'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../../testMiddlewareDependencies.redux'

describe('Component | Missions listing', () => {
    let store: Store<AppState, AppActionsType>
    let missionsListingConnected: ShallowWrapper
    let missionsListingWrapper: ShallowWrapper
    let inMemoryMemberRepository: InMemoryMemberRepository

    beforeEach(() => {
        const testDependencies = testEpicsDependencies
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        inMemoryMemberRepository = testDependencies.dependencies.memberRepository as InMemoryMemberRepository

        jest.spyOn(store, 'dispatch')
    })

    it('Loading all available missions', done => {
        expectFinaleWrapper(() => {
            expect(store.dispatch).toHaveBeenCalledWith({ type: missionListingActions.FETCH_ALL_MISSIONS })
            expect(missionsListingWrapper.find(Loading).exists()).toBeTruthy()
        }, 1, done)

        store.dispatch(missionListingActions.Actions.fetchAllMissions())
    })

    describe('After fetching some missions', () => {
        const someMissionsHeader: MissionHeader[] = [
            new StubMissionHeaderBuilder().build(),
            new StubMissionHeaderBuilder().withId('2').build()
        ]

        it('Display missions listing', done => {
            expectFinaleWrapper(() => {
                const missionsList = missionsListingWrapper.find(FlatList)
                const item = new StubMissionHeaderVMBuilder().build()
                const missionsListingItemProps = missionsList.prop('renderItem')({ item }).props

                expect(missionsList.exists()).toBeTruthy()
                expect(missionsList.prop('data')).toHaveLength(2)
                expect(missionsListingItemProps).toEqual({
                    mission        : item,
                    currentMemberId: 'id'
                })
            }, 1, done)

            store.dispatch(missionListingActions.Actions.allMissionsFetched(someMissionsHeader))
        })
    })

    function expectFinaleWrapper(fn: () => void, eventCount: number, done: () => void) {
        let event = 0
        store.subscribe(() => {
            if (++event === eventCount) {
                runComponent()
                fn()
                done()
            }
        })
    }

    function runComponent() {
        const member: Member = new StubMemberBuilder().build()
        store.dispatch(memberInformationActions.Actions.fetchMemberInformation())
        inMemoryMemberRepository.populateMemberInformation(member)

        missionsListingConnected = shallow(<MissionsListingPage/>, { context: { store } })
        missionsListingWrapper = missionsListingConnected.dive()
        expect(missionsListingWrapper.find(ApplicationHeader).exists()).toBeTruthy()
    }
})
