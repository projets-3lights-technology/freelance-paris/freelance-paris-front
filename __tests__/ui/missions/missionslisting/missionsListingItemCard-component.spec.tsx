import { shallow } from 'enzyme'
import React from 'react'
import { MissionsListingItemCard } from '../../../../src/ui/missions/missionslisting/item/elements/missionsListingItemCard'
import { MissionHeaderVM } from '../../../../src/ui/missions/viewmodels/missionHeader.viewmodel'
import { StubMissionHeaderVMBuilder } from '../../../missions/stubMissionHeaderVM.builder'

describe('Component | Empty mission list', () => {
    const onPress: jest.Mock = jest.fn()
    const onShowPopover: jest.Mock = jest.fn()
    const onRemove: jest.Mock = jest.fn()
    const onCancel: jest.Mock = jest.fn()
    const capitalize: jest.Mock = jest.fn()
    let mission: MissionHeaderVM

    beforeEach(() => {
        mission = new StubMissionHeaderVMBuilder().build()
    })

    it('Render a standard mission card', () => {
        const wrapper = shallow(<MissionsListingItemCard
            mission={mission}
            onPress={onPress}
            onShowPopover={onShowPopover}
            isTheAuthor={false}
            isPopoverVisible={false}
            onRemove={onRemove}
            onCancel={onCancel}
            capitalize={capitalize}
        />)

        expect(wrapper).toMatchSnapshot()
    })

    it('Render an authored mission card', () => {
        const wrapper = shallow(<MissionsListingItemCard
            mission={mission}
            onPress={onPress}
            onShowPopover={onShowPopover}
            isTheAuthor={true}
            isPopoverVisible={false}
            onRemove={onRemove}
            onCancel={onCancel}
            capitalize={capitalize}
        />)

        expect(wrapper).toMatchSnapshot()
    })
})
