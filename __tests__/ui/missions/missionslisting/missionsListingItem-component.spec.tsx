import { shallow, ShallowWrapper } from 'enzyme'
import React from 'react'
import { Store } from 'redux'
import { appAnalytics } from '../../../../src/app/common/usecases/analytics/app.analytics'
import * as navigationActions from '../../../../src/app/common/usecases/navigation/navigation.actions'
import * as missionListingActions from '../../../../src/app/missions/usecases/missionslisting/missionsListing.actions'
import { appEpics } from '../../../../src/configuration/redux/rootEpicMiddleware.redux'
import { AppActionsType } from '../../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../../src/configuration/redux/rootStore.redux'
import { AppPageName } from '../../../../src/configuration/withNavigation/appPageName.navigation'
import { MissionsListingItem } from '../../../../src/ui/missions/missionslisting/item'
import { MissionHeaderVM } from '../../../../src/ui/missions/viewmodels/missionHeader.viewmodel'
import { StubMissionHeaderVMBuilder } from '../../../missions/stubMissionHeaderVM.builder'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../../testMiddlewareDependencies.redux'

describe('Component | Missions listing item', () => {
    let store: Store<AppState, AppActionsType>
    let missionsListingItemConnected: ShallowWrapper
    let missionsListingItemWrapper: ShallowWrapper
    const currentMemberId = '1'

    beforeEach(() => {
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testEpicsDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })

        jest.spyOn(store, 'dispatch')
    })

    describe('Display a mission from listing', () => {
        it('Without actions', () => {
            runComponent(new StubMissionHeaderVMBuilder().build())

            expect(missionsListingItemWrapper).toMatchSnapshot()
        })

        it('With actions', () => {
            const authorSMission = new StubMissionHeaderVMBuilder().withAuthorId('1').build()
            runComponent(authorSMission)

            expect(missionsListingItemWrapper).toMatchSnapshot()
        })
    })

    it('Inform when the user wants more information about the mission', () => {
        const mission = new StubMissionHeaderVMBuilder().build()
        runComponent(mission)

        missionsListingItemWrapper.simulate('press')

        expect(store.dispatch).toHaveBeenCalledTimes(1)
        expect(store.dispatch).toHaveBeenCalledWith({
            type     : navigationActions.NAVIGATE,
            name     : navigationActions.OPEN_MISSION_DETAILS,
            routeName: AppPageName.MISSION_DETAILS,
            params   : {
                id: mission.id
            }
        })
    })

    it('Inform when the user wants to remove the mission', () => {
        const authorSMission = new StubMissionHeaderVMBuilder().withAuthorId('1').build()
        runComponent(authorSMission)

        missionsListingItemWrapper.simulate('remove')

        expect(store.dispatch).toHaveBeenCalledTimes(1)
        expect(store.dispatch).toHaveBeenCalledWith({
            type   : missionListingActions.REMOVE_MISSION,
            payload: authorSMission.id
        })
    })

    it('Inform when the user wants to show mission actions', () => {
        const mission = new StubMissionHeaderVMBuilder().build()
        runComponent(mission)

        missionsListingItemWrapper.simulate('showPopover')

        expect(store.dispatch).toHaveBeenCalledTimes(1)
        expect(store.dispatch).toHaveBeenCalledWith({
            type     : navigationActions.OTHER,
            name     : navigationActions.OPEN_MISSION_ACTIONS,
            routeName: AppPageName.MISSIONS,
            params   : {
                id: mission.id
            }
        })
    })

    it('Inform when the user wants to hide mission actions', () => {
        const authorSMission = new StubMissionHeaderVMBuilder().withAuthorId('1').build()
        runComponent(authorSMission)

        missionsListingItemWrapper.simulate('cancel')

        expect(store.dispatch).toHaveBeenCalledTimes(1)
        expect(store.dispatch).toHaveBeenCalledWith({
            type: navigationActions.OTHER,
            name: navigationActions.QUIT_MISSION_ACTIONS
        })
    })

    function runComponent(mission: MissionHeaderVM) {
        missionsListingItemConnected = shallow(<MissionsListingItem
            mission={mission}
            currentMemberId={currentMemberId}
        />, { context: { store } })
        missionsListingItemWrapper = missionsListingItemConnected.dive()
    }
})
