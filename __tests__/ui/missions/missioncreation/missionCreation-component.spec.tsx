import { shallow, ShallowWrapper } from 'enzyme'
import { Button, Icon } from 'native-base'
import React from 'react'
import { BackHandler, BackHandlerStatic } from 'react-native'
import { Store } from 'redux'
import { appAnalytics } from '../../../../src/app/common/usecases/analytics/app.analytics'
import * as navigationActions from '../../../../src/app/common/usecases/navigation/navigation.actions'
import * as missionCreationActions from '../../../../src/app/missions/usecases/missioncreation/missionCreation.actions'
import { appEpics } from '../../../../src/configuration/redux/rootEpicMiddleware.redux'
import { AppActionsType } from '../../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../../src/configuration/redux/rootStore.redux'
import { FormInputText } from '../../../../src/ui/common/form/formInputText'
import { ModalHeader } from '../../../../src/ui/common/modalheader/modalHeader'
import { MissionCreationPage } from '../../../../src/ui/missions/missioncreation'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../../testMiddlewareDependencies.redux'

describe('Component | Mission creation', () => {
    let store: Store<AppState, AppActionsType>
    let missionCreationConnected: ShallowWrapper
    let missionCreationWrapper: ShallowWrapper
    let backHandler: BackHandlerStatic

    beforeEach(() => {
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testEpicsDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        backHandler = BackHandler

        jest.spyOn(store, 'dispatch')
        jest.spyOn(backHandler, 'addEventListener')
        jest.spyOn(backHandler, 'removeEventListener')
    })

    it('Display form', () => {
        runComponent()
        expect(backHandler.addEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
    })

    describe('Display information', () => {
        it('Change information', () => {
            runComponent()
            missionCreationWrapper.find(FormInputText).at(0).simulate('change', 'title')

            expectStoreDispatching({
                type   : missionCreationActions.CHANGE_MISSION_INFORMATION,
                payload: {
                    field: 'name',
                    value: 'title'
                }
            })
        })

        it('Open more information', () => {
            runComponent()
            missionCreationWrapper.find(Icon).simulate('press')

            expectStoreDispatching({
                type: navigationActions.OTHER,
                name: navigationActions.OPEN_INFORMATION_TEMPLATE
            })
        })

        describe('Submit form', () => {
            it('Change with valid information', done => {
                expectFinaleWrapper(() => {
                    missionCreationWrapper.find(Button).at(0).simulate('press')

                    expectStoreDispatching({
                        type: missionCreationActions.PUBLISH_MISSION
                    })
                    expectStoreDispatching({
                        type: navigationActions.BACK,
                        name: navigationActions.QUIT_MISSION_CREATION
                    })
                }, 9, done)

                store.dispatch(missionCreationActions.Actions.changeMissionInformation('name', 'name'))
                store.dispatch(missionCreationActions.Actions.changeMissionInformation('averageDailyRate', '666'))
                store.dispatch(missionCreationActions.Actions.changeMissionInformation('customer', 'customer'))
                store.dispatch(missionCreationActions.Actions.changeMissionInformation('location', 'location'))
                store.dispatch(missionCreationActions.Actions.changeMissionStartDate('19/10/2018'))
                store.dispatch(missionCreationActions.Actions.changeMissionInformation('durationInMonths', '-1'))
                store.dispatch(missionCreationActions.Actions.changeMissionInformation('remoteIsPossible', true))
                store.dispatch(missionCreationActions.Actions.changeMissionInformation('description', 'lorem ipsum'))
                store.dispatch(missionCreationActions.Actions.changeMissionInformation('contractType', 'other'))
            })

            it('Change with invalid information', done => {
                expectFinaleWrapper(() => {
                    missionCreationWrapper.find(Button).at(0).simulate('press')

                    expectStoreDispatching({
                        type: missionCreationActions.PUBLISH_MISSION
                    })
                }, 1, done)

                store.dispatch(missionCreationActions.Actions.changeMissionInformation('name', 'title'))
            })
        })

        it('Cancel', () => {
            runComponent()
            missionCreationWrapper.find(Button).at(1).simulate('press')
            expectStoreDispatching({
                type: navigationActions.BACK,
                name: navigationActions.QUIT_MISSION_CREATION
            })
            expect(backHandler.removeEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
        })
    })

    it('Close it', () => {
        runComponent()
        missionCreationWrapper.find(ModalHeader).simulate('leftClick')
        expectStoreDispatching({
            type: navigationActions.BACK,
            name: navigationActions.QUIT_MISSION_CREATION
        })
        expect(backHandler.removeEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
    })

    function expectFinaleWrapper(fn: () => void, eventCount: number, done: () => void) {
        let event = 0
        store.subscribe(() => {
            if (++event === eventCount) {
                runComponent()
                fn()
                done()
            }
        })
    }

    function expectStoreDispatching(expectedAction: any) {
        expect(store.dispatch).toHaveBeenCalledWith(expectedAction)
    }

    function runComponent() {
        missionCreationConnected = shallow(<MissionCreationPage/>, { context: { store } })
        missionCreationWrapper = missionCreationConnected.dive()
    }
})
