import { shallow, ShallowWrapper } from 'enzyme'
import React from 'react'
import { BackHandler, BackHandlerStatic } from 'react-native'
import Modal from 'react-native-modal'
import { Store } from 'redux'
import { appAnalytics } from '../../../../src/app/common/usecases/analytics/app.analytics'
import * as navigationActions from '../../../../src/app/common/usecases/navigation/navigation.actions'
import { appEpics } from '../../../../src/configuration/redux/rootEpicMiddleware.redux'
import { AppActionsType } from '../../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../../src/configuration/redux/rootStore.redux'
import { InformationTemplateModal } from '../../../../src/ui/missions/missioncreation/informationtemplate'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../../testMiddlewareDependencies.redux'

describe('Component | Information template', () => {
    let store: Store<AppState, AppActionsType>
    let informationTemplateConnected: ShallowWrapper
    let informationTemplateWrapper: ShallowWrapper
    let backHandler: BackHandlerStatic

    beforeEach(() => {
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testEpicsDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        backHandler = BackHandler

        jest.spyOn(store, 'dispatch')
        jest.spyOn(backHandler, 'addEventListener')
        jest.spyOn(backHandler, 'removeEventListener')
    })

    it('Display information', () => {
        runComponent()
        expect(backHandler.addEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
    })

    it('Close it', () => {
        runComponent()
        informationTemplateWrapper.find(Modal).simulate('backdropPress')
        expectStoreDispatching({
            type: navigationActions.OTHER,
            name: navigationActions.QUIT_INFORMATION_TEMPLATE
        })
        expect(backHandler.removeEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
    })

    it('Close it by back button', () => {
        runComponent()
        informationTemplateWrapper.find(Modal).simulate('backButtonPress')
        expectStoreDispatching({
            type: navigationActions.OTHER,
            name: navigationActions.QUIT_INFORMATION_TEMPLATE
        })
        expect(backHandler.removeEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
    })

    function expectStoreDispatching(expectedAction: any) {
        expect(store.dispatch).toHaveBeenCalledWith(expectedAction)
    }

    function runComponent() {
        informationTemplateConnected = shallow(<InformationTemplateModal/>, { context: { store } })
        informationTemplateWrapper = informationTemplateConnected.dive()
    }
})
