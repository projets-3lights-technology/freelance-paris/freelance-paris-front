import { shallow, ShallowWrapper } from 'enzyme'
import { Button } from 'native-base'
import React from 'react'
import { Store } from 'redux'
import { appAnalytics } from '../../../src/app/common/usecases/analytics/app.analytics'
import * as navigationActions from '../../../src/app/common/usecases/navigation/navigation.actions'
import { appEpics } from '../../../src/configuration/redux/rootEpicMiddleware.redux'
import { AppActionsType } from '../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../src/configuration/redux/rootStore.redux'
import { AppPageName } from '../../../src/configuration/withNavigation/appPageName.navigation'
import { LoginPage } from '../../../src/ui/login'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../testMiddlewareDependencies.redux'

jest.mock('../../../src/configuration/environment/envVar.appConfiguration')

describe('Component | Login container', () => {
    let store: Store<AppState, AppActionsType>
    let loginStoreConnected: ShallowWrapper
    let loginWrapper: ShallowWrapper

    beforeEach(() => {
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testEpicsDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })

        jest.spyOn(store, 'dispatch')
    })

    xit('User can log to the application', () => {
        runComponent()
        loginWrapper.find(Button).simulate('press')
        expectStoreDispatching({
            type     : navigationActions.NAVIGATE,
            name     : navigationActions.NAVIGATE_TO_MAIN_PAGE,
            routeName: AppPageName.MAIN_PAGE
        })
    })

    function expectStoreDispatching(expectedAction: any) {
        expect(store.dispatch).toHaveBeenCalledWith(expectedAction)
    }

    function runComponent() {
        loginStoreConnected = shallow(<LoginPage/>, { context: { store } })
        loginWrapper = loginStoreConnected.dive()
    }
})
