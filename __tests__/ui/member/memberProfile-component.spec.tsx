import { shallow } from 'enzyme'
import { Button } from 'native-base'
import React from 'react'
import { BackHandler, BackHandlerStatic } from 'react-native'
import { Store } from 'redux'
import { appAnalytics } from '../../../src/app/common/usecases/analytics/app.analytics'
import * as gatekeeperActions from '../../../src/app/common/usecases/gatekeeper/gatekeeper.actions'
import * as navigationActions from '../../../src/app/common/usecases/navigation/navigation.actions'
import { InMemoryMemberRepository } from '../../../src/app/member/adapters/gateways/inmemory/inMemory.memberRepository'
import { Member } from '../../../src/app/member/domain/entities/member'
import * as memberInformationActions from '../../../src/app/member/usecases/memberinformation/memberInformation.actions'
import * as memberModificationActions from '../../../src/app/member/usecases/membermodification/memberModification.actions'
import { appEpics } from '../../../src/configuration/redux/rootEpicMiddleware.redux'
import { AppActionsType } from '../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../src/configuration/redux/rootStore.redux'
import { FormInputText } from '../../../src/ui/common/form/formInputText'
import { ModalHeader } from '../../../src/ui/common/modalheader/modalHeader'
import { MemberProfilePage } from '../../../src/ui/member'
import { StubMemberBuilder } from '../../member/stubMember.builder'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../testMiddlewareDependencies.redux'

describe('Component | Member profile', () => {
    let store: Store<AppState, AppActionsType>
    let memberProfileConnected: any
    let memberProfileWrapper: any
    let inMemoryMemberRepository: InMemoryMemberRepository
    let backHandler: BackHandlerStatic

    beforeEach(() => {
        const testDependencies = testEpicsDependencies
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        inMemoryMemberRepository = testDependencies.dependencies.memberRepository as InMemoryMemberRepository
        backHandler = BackHandler

        jest.spyOn(store, 'dispatch')
        jest.spyOn(backHandler, 'addEventListener')
        jest.spyOn(backHandler, 'removeEventListener')
        store.dispatch(navigationActions.Actions.openMemberProfile())
    })

    describe('Display information', () => {
        it('By default', () => {
            runComponent()
            expect(memberProfileWrapper.find(FormInputText).prop('value')).toEqual('email')
            expect(backHandler.addEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
        })

        it('Change information', () => {
            runComponent()
            memberProfileWrapper.find(FormInputText).simulate('change', 'new email')

            expectStoreDispatching({
                type   : memberModificationActions.CHANGE_MEMBER_INFORMATION,
                payload: {
                    field: 'email',
                    value: 'new email'
                }
            })
        })

        describe('Submit form', () => {
            it('Change with valid information', done => {
                expectFinaleWrapper(() => {
                    memberProfileWrapper.find(Button).at(0).simulate('press')

                    expectStoreDispatching({
                        type: memberModificationActions.UPDATE_MEMBER
                    })
                    expectStoreDispatching({
                        type: navigationActions.BACK,
                        name: navigationActions.QUIT_MEMBER_PROFILE
                    })
                }, 1, done)

                store.dispatch(memberModificationActions.Actions.changeMemberInformation('email', 'new.email@gmail.com'))
            })

            it('Change with invalid information', done => {
                expectFinaleWrapper(() => {
                    memberProfileWrapper.find(Button).at(0).simulate('press')

                    expectStoreDispatching({
                        type: memberModificationActions.UPDATE_MEMBER
                    })
                }, 1, done)

                store.dispatch(memberModificationActions.Actions.changeMemberInformation('email', 'new email'))
            })
        })

        it('Cancel', () => {
            runComponent()
            memberProfileWrapper.find(Button).at(1).simulate('press')
            expectStoreDispatching({
                type: navigationActions.BACK,
                name: navigationActions.QUIT_MEMBER_PROFILE
            })
            expect(backHandler.removeEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
        })
    })

    it('Close it', () => {
        runComponent()
        memberProfileWrapper.find(ModalHeader).simulate('leftClick')
        expectStoreDispatching({
            type: navigationActions.BACK,
            name: navigationActions.QUIT_MEMBER_PROFILE
        })
        expect(backHandler.removeEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
    })

    it('Unsubscribe member', () => {
        runComponent()
        memberProfileWrapper.find(Button).at(2).simulate('press')
        expectStoreDispatching({
            type: gatekeeperActions.UNSUBSCRIBE_MEMBER
        })
        expect(backHandler.removeEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
    })

    function expectFinaleWrapper(fn: () => void, eventCount: number, done: () => void) {
        let event = 0
        store.subscribe(() => {
            if (++event === eventCount) {
                runComponent()
                fn()
                done()
            }
        })
    }

    function expectStoreDispatching(expectedAction: any) {
        expect(store.dispatch).toHaveBeenCalledWith(expectedAction)
    }

    function runComponent() {
        const member: Member = new StubMemberBuilder().build()
        store.dispatch(memberInformationActions.Actions.fetchMemberInformation())
        inMemoryMemberRepository.populateMemberInformation(member)

        memberProfileConnected = shallow(<MemberProfilePage/>, { context: { store } })
        memberProfileWrapper = memberProfileConnected.dive()
    }
})
