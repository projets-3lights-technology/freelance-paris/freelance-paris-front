import { shallow } from 'enzyme'
import React from 'react'
import { LaunchScreen } from '../../../../src/ui/common/launchscreen/LaunchScreen'

describe('Component | Launch screen', () => {
    it('Render the launch screen', () => {
        const wrapper = shallow(<LaunchScreen/>)

        expect(wrapper).toMatchSnapshot()
    })
})
