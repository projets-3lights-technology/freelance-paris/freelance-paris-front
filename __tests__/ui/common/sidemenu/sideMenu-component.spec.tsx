import { shallow, ShallowWrapper } from 'enzyme'
import { Button, Content, Footer, Header } from 'native-base'
import React from 'react'
import { BackHandler, BackHandlerStatic } from 'react-native'
import { Store } from 'redux'
import { appAnalytics } from '../../../../src/app/common/usecases/analytics/app.analytics'
import * as gatekeeperActions from '../../../../src/app/common/usecases/gatekeeper/gatekeeper.actions'
import * as navigationActions from '../../../../src/app/common/usecases/navigation/navigation.actions'
import { InMemoryMemberRepository } from '../../../../src/app/member/adapters/gateways/inmemory/inMemory.memberRepository'
import { Member } from '../../../../src/app/member/domain/entities/member'
import * as memberInformationActions from '../../../../src/app/member/usecases/memberinformation/memberInformation.actions'
import { appEpics } from '../../../../src/configuration/redux/rootEpicMiddleware.redux'
import { AppActionsType } from '../../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../../src/configuration/redux/rootStore.redux'
import { SideMenu } from '../../../../src/ui/common/sidemenu'
import { StubMemberBuilder } from '../../../member/stubMember.builder'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../../testMiddlewareDependencies.redux'

describe('Component | Side menu', () => {
    let store: Store<AppState, AppActionsType>
    let memberProfileConnected: ShallowWrapper
    let memberProfileWrapper: ShallowWrapper
    let inMemoryMemberRepository: InMemoryMemberRepository
    let backHandler: BackHandlerStatic

    beforeEach(() => {
        const testDependencies = testEpicsDependencies
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        inMemoryMemberRepository = testDependencies.dependencies.memberRepository as InMemoryMemberRepository
        backHandler = BackHandler

        jest.spyOn(store, 'dispatch')
        jest.spyOn(backHandler, 'addEventListener')
        jest.spyOn(backHandler, 'removeEventListener')
    })

    it('Close it', () => {
        runComponent()
        memberProfileWrapper.find(Button).at(0).simulate('press')
        expectStoreDispatching({
            type: navigationActions.CLOSE_DRAWER,
            name: navigationActions.QUIT_SIDE_MENU
        })
        expect(backHandler.removeEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
    })

    it('Loading some information', () => {
        memberProfileConnected = shallow(<SideMenu/>, { context: { store } })
        memberProfileWrapper = memberProfileConnected.dive()
        store.dispatch(memberInformationActions.Actions.fetchMemberInformation())

        expect(memberProfileWrapper.find(Header).exists()).toBeFalsy()
        expect(backHandler.addEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
    })

    describe('After fetching information', () => {
        beforeEach(() => {
            runComponent()
        })

        it('Display side menu', () => {
            expect(memberProfileWrapper.find(Header).exists()).toBeTruthy()
            expect(memberProfileWrapper.find(Content).exists()).toBeTruthy()
            expect(memberProfileWrapper.find(Footer).exists()).toBeTruthy()
        })

        it('Log out', () => {
            memberProfileWrapper.find(Button).at(1).simulate('press')
            expectStoreDispatching({
                type: gatekeeperActions.LOGOUT_MEMBER
            })
            expect(backHandler.removeEventListener).toHaveBeenCalledWith('hardwareBackPress', expect.any(Function))
        })
    })

    function expectStoreDispatching(expectedAction: any) {
        expect(store.dispatch).toHaveBeenCalledWith(expectedAction)
    }

    function runComponent() {
        const member: Member = new StubMemberBuilder().build()
        store.dispatch(memberInformationActions.Actions.fetchMemberInformation())
        inMemoryMemberRepository.populateMemberInformation(member)

        memberProfileConnected = shallow(<SideMenu/>, { context: { store } })
        memberProfileWrapper = memberProfileConnected.dive()
    }
})
