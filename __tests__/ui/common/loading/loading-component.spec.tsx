import { shallow } from 'enzyme'
import React from 'react'
import { Loading } from '../../../../src/ui/common/loading/loading'

describe('Component | Loading', () => {
    it('Display an activity indicator', () => {
        const wrapper = shallow(<Loading/>)

        expect(wrapper).toMatchSnapshot()
    })
})
