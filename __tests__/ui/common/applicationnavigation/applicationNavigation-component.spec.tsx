import { shallow, ShallowWrapper } from 'enzyme'
import { Button } from 'native-base'
import React from 'react'
import { Store } from 'redux'
import { appAnalytics } from '../../../../src/app/common/usecases/analytics/app.analytics'
import * as navigationActions from '../../../../src/app/common/usecases/navigation/navigation.actions'
import { appEpics } from '../../../../src/configuration/redux/rootEpicMiddleware.redux'
import { AppActionsType } from '../../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../../src/configuration/redux/rootStore.redux'
import { ApplicationNavigation } from '../../../../src/ui/common/applicationnavigation'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../../testMiddlewareDependencies.redux'

describe('Component | Application navigation', () => {
    let store: Store<AppState, AppActionsType>
    let applicationNavigationConnected: ShallowWrapper
    let applicationNavigationWrapper: ShallowWrapper
    let missionsButton: ShallowWrapper

    beforeEach(() => {
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testEpicsDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })

        jest.spyOn(store, 'dispatch')
    })

    describe('Select missions tab', () => {
        it('When the user is on missions listing', done => {
            expectFinaleWrapper(() => {
                expect(missionsButton.prop('active')).toBeTruthy()
            }, 1, done)

            store.dispatch(navigationActions.Actions.navigateToMissions())
        })

        it('When the user is on mission details', done => {
            expectFinaleWrapper(() => {
                expect(missionsButton.prop('active')).toBeTruthy()
            }, 1, done)

            store.dispatch(navigationActions.Actions.openMissionDetails('1'))
        })
    })

    function expectFinaleWrapper(fn: () => void, eventCount: number, done: () => void) {
        let event = 0
        store.subscribe(() => {
            if (++event === eventCount) {
                runComponent()
                fn()
                done()
            }
        })
    }

    function runComponent() {
        applicationNavigationConnected = shallow(<ApplicationNavigation/>, { context: { store } })
        applicationNavigationWrapper = applicationNavigationConnected.dive()
        missionsButton = applicationNavigationWrapper.find(Button).at(0)
    }
})
