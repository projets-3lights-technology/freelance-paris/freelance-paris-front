import { shallow, ShallowWrapper } from 'enzyme'
import { Button } from 'native-base'
import React from 'react'
import { Store } from 'redux'
import { appAnalytics } from '../../../../src/app/common/usecases/analytics/app.analytics'
import * as navigationActions from '../../../../src/app/common/usecases/navigation/navigation.actions'
import { appEpics } from '../../../../src/configuration/redux/rootEpicMiddleware.redux'
import { AppActionsType } from '../../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../../src/configuration/redux/rootStore.redux'
import { AppPageName } from '../../../../src/configuration/withNavigation/appPageName.navigation'
import { ApplicationHeader } from '../../../../src/ui/common/applicationheader'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../../testMiddlewareDependencies.redux'

describe('Component | Application header', () => {
    let store: Store<AppState, AppActionsType>
    let applicationHeaderConnected: ShallowWrapper
    let applicationHeaderWrapper: ShallowWrapper
    let profileButton: ShallowWrapper

    beforeEach(() => {
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testEpicsDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })

        jest.spyOn(store, 'dispatch')
        runComponent()
    })

    it('Display the application header', () => {
        expect(applicationHeaderWrapper).toMatchSnapshot()
    })

    it('Open user profile', () => {
        profileButton.simulate('press')
        expectStoreDispatching({
            type     : navigationActions.OPEN_DRAWER,
            name     : navigationActions.OPEN_SIDE_MENU,
            routeName: AppPageName.SIDE_MENU
        })
    })

    function expectStoreDispatching(expectedAction: any) {
        expect(store.dispatch).toHaveBeenCalledWith(expectedAction)
    }

    function runComponent() {
        applicationHeaderConnected = shallow(<ApplicationHeader title={'title'}/>, { context: { store } })
        applicationHeaderWrapper = applicationHeaderConnected.dive()
        profileButton = applicationHeaderWrapper.find(Button).at(0)
    }
})
