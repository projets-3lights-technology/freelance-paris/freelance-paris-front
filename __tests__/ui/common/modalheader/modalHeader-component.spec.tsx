import { shallow } from 'enzyme'
import { Button } from 'native-base'
import React from 'react'
import { ModalHeader } from '../../../../src/ui/common/modalheader/modalHeader'
import Mock = jest.Mock

describe('Component | Modal header', () => {
    let onLeftClick: Mock

    beforeEach(() => {
        onLeftClick = jest.fn()
    })

    it('Display a mission from listing', () => {
        const wrapper = shallow(<ModalHeader leftIcon={'close-circle-outline'} onLeftClick={onLeftClick}/>)

        expect(wrapper).toMatchSnapshot()
    })

    it('Inform when the user wants more information about the mission', () => {
        const wrapper = shallow(<ModalHeader leftIcon={'close-circle-outline'} onLeftClick={onLeftClick}/>)

        wrapper.find(Button).simulate('press')

        expect(onLeftClick).toHaveBeenCalledTimes(1)
    })
})
