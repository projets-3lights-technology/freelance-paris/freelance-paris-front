import { shallow } from 'enzyme'
import Mock = jest.Mock
import lolex from 'lolex'
import React from 'react'
import DatePicker from 'react-native-datepicker'
import { FormDatePicker } from '../../../../src/ui/common/form/formDatePicker'

describe('Component | Form date picker', () => {
    let onChange: Mock
    let clock: lolex.Clock
    const now = '2018-10-10T19:50:10.000Z'

    beforeEach(() => {
        clock = lolex.install({ now: new Date(now) })
        onChange = jest.fn()
    })

    afterEach(() => {
        clock.uninstall()
    })

    describe('Display a date picker', () => {
        it('Display a date picker without selected date', () => {
            const wrapper = shallow(<FormDatePicker
                label={'date picker'}
                placeholder={'Date'}
                confirmButtonText={'Confirm'}
                cancelButtonText={'Cancel'}
                value={''}
                onChange={onChange}
            />)

            expect(wrapper).toMatchSnapshot()
        })

        it('Display a date picker with a selected date', () => {
            const wrapper = shallow(<FormDatePicker
                label={'date picker'}
                placeholder={'Date'}
                confirmButtonText={'Confirm'}
                cancelButtonText={'Cancel'}
                value={'29/10/2018'}
                onChange={onChange}
            />)

            expect(wrapper).toMatchSnapshot()
        })

        it('Display a date picker on error', () => {
            const wrapper = shallow(<FormDatePicker
                label={'date picker'}
                placeholder={'Date'}
                confirmButtonText={'Confirm'}
                cancelButtonText={'Cancel'}
                value={''}
                error={'error'}
                onChange={onChange}
            />)

            expect(wrapper).toMatchSnapshot()
        })
    })

    it('Inform when the user select a date', () => {
        const wrapper = shallow(<FormDatePicker
            label={'date picker'}
            placeholder={'Date'}
            confirmButtonText={'Confirm'}
            cancelButtonText={'Cancel'}
            value={''}
            onChange={onChange}
        />)

        wrapper.find(DatePicker).simulate('dateChange', 'date')

        expect(onChange).toHaveBeenCalledTimes(1)
        expect(onChange).toHaveBeenCalledWith('date')
    })
})
