import { shallow } from 'enzyme'
import React from 'react'
import { TextInput } from 'react-native'
import { FormInputText } from '../../../../src/ui/common/form/formInputText'
import Mock = jest.Mock

describe('Component | Form input text', () => {
    let onChange: Mock

    beforeEach(() => {
        onChange = jest.fn()
    })

    describe('Display an input text', () => {
        it('Display an input text without value', () => {
            const wrapper = shallow(<FormInputText
                label={'Text'}
                value={''}
                onChange={onChange}
            />).dive()

            expect(wrapper).toMatchSnapshot()
        })

        it('Display an input text with value', () => {
            const wrapper = shallow(<FormInputText
                label={'Text'}
                value={'1'}
                onChange={onChange}
            />).dive()

            expect(wrapper).toMatchSnapshot()
        })

        it('Display an input text on error', () => {
            const wrapper = shallow(<FormInputText
                label={'Text'}
                value={''}
                error={'error'}
                type={'email-address'}
                onChange={onChange}
            />).dive()

            expect(wrapper).toMatchSnapshot()
        })
    })

    it('Inform when the user types a text', () => {
        const wrapper = shallow(<FormInputText
            label={'Text'}
            value={''}
            onChange={onChange}
        />).dive()

        wrapper.find(TextInput).simulate('changeText', '2')

        expect(onChange).toHaveBeenCalledTimes(1)
        expect(onChange).toHaveBeenCalledWith('2')
    })
})
