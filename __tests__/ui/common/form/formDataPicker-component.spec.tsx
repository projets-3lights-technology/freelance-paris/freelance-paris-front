import { shallow } from 'enzyme'
import { Picker } from 'native-base'
import React from 'react'
import { DataPickerValues, FormDataPicker } from '../../../../src/ui/common/form/formDataPicker'
import Mock = jest.Mock

describe('Component | Form data picker', () => {
    let onChange: Mock
    let values: DataPickerValues

    beforeEach(() => {
        onChange = jest.fn()
        values = [
            { label: 'label 1', value: '1' }
        ]
    })

    describe('Display a picker', () => {
        it('Without a selected item', () => {
            const wrapper = shallow(<FormDataPicker
                label={'Duration'}
                placeholder={'Duration'}
                values={values}
                value={''}
                onChange={onChange}
            />)

            expect(wrapper).toMatchSnapshot()
        })

        it('With a selected item', () => {
            const wrapper = shallow(<FormDataPicker
                label={'Duration'}
                placeholder={'Duration'}
                values={values}
                value={'1'}
                onChange={onChange}
            />)

            expect(wrapper).toMatchSnapshot()
        })

        it('On error', () => {
            const wrapper = shallow(<FormDataPicker
                label={'Duration'}
                placeholder={'Duration'}
                values={values}
                value={''}
                error={'error'}
                onChange={onChange}
            />)

            expect(wrapper).toMatchSnapshot()
        })
    })

    it('Inform when the user select an item', () => {
        const wrapper = shallow(<FormDataPicker
            label={'Duration'}
            placeholder={'Duration'}
            values={values}
            value={''}
            onChange={onChange}
        />)

        wrapper.find(Picker).simulate('valueChange', 'duration')

        expect(onChange).toHaveBeenCalledTimes(1)
        expect(onChange).toHaveBeenCalledWith('duration')
    })
})
