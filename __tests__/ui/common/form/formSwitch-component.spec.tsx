import { shallow } from 'enzyme'
import React from 'react'
import { Switch } from 'react-native'
import { FormSwitch } from '../../../../src/ui/common/form/formSwitch'
import Mock = jest.Mock

describe('Component | Form switch', () => {
    let onChange: Mock

    beforeEach(() => {
        onChange = jest.fn()
    })

    describe('Display a switch', () => {
        it('Display disactivated switch', () => {
            const wrapper = shallow(<FormSwitch
                label={'Switch'}
                value={false}
                onChange={onChange}
            />)

            expect(wrapper).toMatchSnapshot()
        })

        it('Display activated switch', () => {
            const wrapper = shallow(<FormSwitch
                label={'Switch'}
                value={true}
                onChange={onChange}
            />)

            expect(wrapper).toMatchSnapshot()
        })

        it('Display switch on error', () => {
            const wrapper = shallow(<FormSwitch
                label={'Switch'}
                value={false}
                error={'error'}
                onChange={onChange}
            />)

            expect(wrapper).toMatchSnapshot()
        })
    })

    it('Inform when the user clicks on the switch', () => {
        const wrapper = shallow(<FormSwitch
            label={'Switch'}
            value={false}
            onChange={onChange}
        />)

        wrapper.find(Switch).simulate('valueChange', true)

        expect(onChange).toHaveBeenCalledTimes(1)
        expect(onChange).toHaveBeenCalledWith(true)
    })
})
