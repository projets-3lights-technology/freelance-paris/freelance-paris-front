import { shallow } from 'enzyme'
import React from 'react'
import { TextInput } from 'react-native'
import { FormInputNumeric } from '../../../../src/ui/common/form/formInputNumeric'
import Mock = jest.Mock

describe('Component | Form input numeric', () => {
    let onChange: Mock

    beforeEach(() => {
        onChange = jest.fn()
    })

    describe('Display an input numeric', () => {
        it('Display an input numeric without value', () => {
            const wrapper = shallow(<FormInputNumeric
                label={'Numeric'}
                value={''}
                onChange={onChange}
            />)

            expect(wrapper).toMatchSnapshot()
        })

        it('Display an input numeric with value', () => {
            const wrapper = shallow(<FormInputNumeric
                label={'Numeric'}
                value={'1'}
                onChange={onChange}
            />)

            expect(wrapper).toMatchSnapshot()
        })

        it('Display an input numeric on error', () => {
            const wrapper = shallow(<FormInputNumeric
                label={'Numeric'}
                value={''}
                error={'error'}
                onChange={onChange}
            />)

            expect(wrapper).toMatchSnapshot()
        })
    })

    it('Inform when the user types a number', () => {
        const wrapper = shallow(<FormInputNumeric
            label={'Numeric'}
            value={''}
            onChange={onChange}
        />)

        wrapper.find(TextInput).simulate('changeText', '2')

        expect(onChange).toHaveBeenCalledTimes(1)
        expect(onChange).toHaveBeenCalledWith('2')
    })
})
