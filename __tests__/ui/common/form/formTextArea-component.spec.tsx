import { shallow } from 'enzyme'
import React from 'react'
import { TextInput } from 'react-native'
import { FormTextArea } from '../../../../src/ui/common/form/formTextArea'
import Mock = jest.Mock

describe('Component | Form text area', () => {
    let onChange: Mock

    beforeEach(() => {
        onChange = jest.fn()
    })

    describe('Display a text area', () => {
        it('Display a text area without value', () => {
            const wrapper = shallow(<FormTextArea
                label={'Text area'}
                value={''}
                onChange={onChange}
            />)

            expect(wrapper).toMatchSnapshot()
        })

        it('Display a text area with value', () => {
            const wrapper = shallow(<FormTextArea
                label={'Text area'}
                value={'text'}
                onChange={onChange}
            />)

            expect(wrapper).toMatchSnapshot()
        })

        it('Display a text area on error', () => {
            const wrapper = shallow(<FormTextArea
                label={'Text area'}
                value={''}
                error={'error'}
                onChange={onChange}
            />)

            expect(wrapper).toMatchSnapshot()
        })
    })

    it('Inform when the user types a text', () => {
        const wrapper = shallow(<FormTextArea
            label={'Text area'}
            value={''}
            onChange={onChange}
        />)

        wrapper.find(TextInput).simulate('changeText', 'text')

        expect(onChange).toHaveBeenCalledTimes(1)
        expect(onChange).toHaveBeenCalledWith('text')
    })
})
