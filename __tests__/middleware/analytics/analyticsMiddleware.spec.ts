import { applyMiddleware, createStore, Store } from 'redux'
import { createAnalyticsMiddleware } from '../../../src/configuration/redux/analyticsMiddleware.redux'

interface TestStore {
    test: string
}

interface TestAnalyticsDependencies {
    analytics: {
        log: () => void
    }
}

const ACTION_WITHOUT_PAYLOAD = 'ACTION_WITHOUT_PAYLOAD'
const ACTION_WITH_PAYLOAD = 'ACTION_WITH_PAYLOAD'

const actionWithoutPayload = ({
    type: ACTION_WITHOUT_PAYLOAD
})

const actionWithPayload = ({
    type   : ACTION_WITH_PAYLOAD,
    payload: 'hello'
})

describe('Analytics Middleware', () => {
    let store: Store<TestStore>
    const initialState: TestStore = { test: 'Test' }

    describe('Without dependencies', () => {
        let analytic1: jest.Mock
        let analytic2: jest.Mock
        let analytics: jest.Mock[]

        beforeEach(() => {
            analytic1 = jest.fn()
            analytic2 = jest.fn()
            analytics = [analytic1, analytic2]
            const testReducer = (state = initialState, action: any): TestStore => {
                switch (action.type) {
                    case ACTION_WITH_PAYLOAD:
                        return { test: action.payload }
                    case ACTION_WITHOUT_PAYLOAD:
                    default:
                        return state
                }
            }
            store = createStore(
                testReducer,
                applyMiddleware(
                    createAnalyticsMiddleware({ analytics })
                )
            )
        })

        it('Without payload', () => {
            store.dispatch(actionWithoutPayload)
            expect(store.getState()).toEqual({ test: 'Test' })
            expect(analytic1).toHaveBeenCalledTimes(1)
            expect(analytic2).toHaveBeenCalledTimes(1)
        })

        it('With payload', () => {
            store.dispatch(actionWithPayload)
            expect(store.getState()).toEqual({ test: 'hello' })
            expect(analytic1).toHaveBeenCalledTimes(1)
            expect(analytic2).toHaveBeenCalledTimes(1)
        })
    })

    describe('With dependencies', () => {
        const analytic = (action: any, state: TestStore, dependencies?: TestAnalyticsDependencies): void => {
            dependencies.analytics.log()
        }
        let analyticsDependencies: TestAnalyticsDependencies

        beforeEach(() => {
            analyticsDependencies = {
                analytics: {
                    log: jest.fn()
                }
            }
            const testReducer = (state = initialState, action: any): TestStore => {
                switch (action.type) {
                    case ACTION_WITH_PAYLOAD:
                        return { test: action.payload }
                    case ACTION_WITHOUT_PAYLOAD:
                    default:
                        return state
                }
            }
            store = createStore(
                testReducer,
                applyMiddleware(
                    createAnalyticsMiddleware({
                        analytics   : [analytic],
                        dependencies: analyticsDependencies
                    })
                )
            )
        })

        it('Without payload', () => {
            store.dispatch(actionWithoutPayload)
            expect(store.getState()).toEqual({ test: 'Test' })
            expect(analyticsDependencies.analytics.log).toHaveBeenCalledTimes(1)
        })

        it('With payload', () => {
            store.dispatch(actionWithPayload)
            expect(store.getState()).toEqual({ test: 'hello' })
            expect(analyticsDependencies.analytics.log).toHaveBeenCalledTimes(1)
        })
    })
})
