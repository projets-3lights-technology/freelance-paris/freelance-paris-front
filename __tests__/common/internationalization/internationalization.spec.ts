import { lensPath, set } from 'ramda'
import { Store } from 'redux'
import { InternationalizationState } from '../../../src/app/common/domain/InternationalizationState.redux'
import { i18nEn } from '../../../src/app/common/domain/translations/i18n.en'
import { i18nFr } from '../../../src/app/common/domain/translations/i18n.fr'
import { appAnalytics } from '../../../src/app/common/usecases/analytics/app.analytics'
import * as i18n from '../../../src/app/common/usecases/internationalization/internationalization.actions'
import { appEpics } from '../../../src/configuration/redux/rootEpicMiddleware.redux'
import { AppActionsType, capitalize, t } from '../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../src/configuration/redux/rootStore.redux'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../testMiddlewareDependencies.redux'

describe('Application translation', () => {
    let store: Store<AppState, AppActionsType>
    let initialState: AppState

    beforeEach(() => {
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testEpicsDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        initialState = store.getState()
    })

    it('Initialize', () => {
        expectState({
            ...initialState,
            ui: {
                ...initialState.ui,
                navigation          : {},
                internationalization: {
                    lang        : 'fr',
                    translations: {
                        fr: i18nFr,
                        en: i18nEn
                    }
                }
            }
        })
    })

    it('Translate', () => {
        expect(t(store.getState())('hello')).toEqual('bonjour')
    })

    it('Capitalize', () => {
        expect(capitalize(store.getState())('hello')).toEqual('Bonjour')
    })

    it('Lang switching', done => {
        expectFinalInternationalizationState({
            ...initialState.ui.internationalization,
            lang: 'en'
        }, () => {
            expect(t(store.getState())('hello')).toEqual('hello')
        }, 1, done)

        store.dispatch(i18n.Actions.switchLang('en'))
    })

    function expectFinalInternationalizationState(expectedInternationalizationState: InternationalizationState,
                                                  selectors: null | (() => void),
                                                  eventCount: number,
                                                  done: () => void) {
        let event = 0
        store.subscribe(() => {
            if (++event === eventCount) {
                expectState({
                    ...initialState,
                    ui: {
                        ...initialState.ui,
                        navigation          : {},
                        internationalization: expectedInternationalizationState
                    }
                })
                if (selectors)
                    selectors()
                done()
            }
        })
    }

    function expectState(expectedState: AppState) {
        expect(set(lensPath(['ui', 'navigation']), {})(store.getState())).toEqual(expectedState)
    }
})
