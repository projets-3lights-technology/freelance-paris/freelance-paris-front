import { Linking } from 'react-native'
import { LinkingUrlOpener } from '../../../../src/app/common/adapters/gateways/real/Linking.urlOpener'
import { UrlOpener } from '../../../../src/app/common/domain/urlopener/UrlOpener'

describe('Integration | Linking', () => {
    it('Open url', () => {
        spyOn(Linking, 'openURL').and.returnValue(Promise.resolve())
        const urlOpener: UrlOpener = new LinkingUrlOpener()

        urlOpener.openUrl('toto')

        expect(Linking.openURL).toHaveBeenCalledTimes(1)
        expect(Linking.openURL).toHaveBeenCalledWith('toto')
    })
})
