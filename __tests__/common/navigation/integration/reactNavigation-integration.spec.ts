import { appAnalytics } from '../../../../src/app/common/usecases/analytics/app.analytics'
import { appEpics } from '../../../../src/configuration/redux/rootEpicMiddleware.redux'
import { ReduxStore } from '../../../../src/configuration/redux/rootStore.redux'
import { AppNavigationLayout } from '../../../../src/configuration/withNavigation/appPagesLayout.navigation'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../../testMiddlewareDependencies.redux'

describe('Integration | React Navigation', () => {
    it('Layout application navigation registration', () => {
        new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testEpicsDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        expect(AppNavigationLayout.router).toMatchSnapshot()
    })
})
