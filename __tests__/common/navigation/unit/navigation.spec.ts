import { lensPath, set } from 'ramda'
import { Store } from 'redux'
import { InMemoryUrlOpener } from '../../../../src/app/common/adapters/gateways/inmemory/inMemory.urlOpener'
import { UserNavigationState } from '../../../../src/app/common/domain/UserNavigationState.redux'
import { appAnalytics } from '../../../../src/app/common/usecases/analytics/app.analytics'
import * as navigationActions from '../../../../src/app/common/usecases/navigation/navigation.actions'
import { appEpics } from '../../../../src/configuration/redux/rootEpicMiddleware.redux'
import {
    AppActionsType,
    getCurrentPage,
    getMissionDetailsId, isInformationTemplateOpen,
    isMissionActionsOpen,
    isMissionContactOpen,
    isOnMissions
} from '../../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../../src/configuration/redux/rootStore.redux'
import { AppPageName } from '../../../../src/configuration/withNavigation/appPageName.navigation'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../../testMiddlewareDependencies.redux'

describe('Application navigation', () => {
    let store: Store<AppState, AppActionsType>
    let initialState: AppState
    let inMemoryUrlOpener: InMemoryUrlOpener

    beforeEach(() => {
        const testDependencies = testEpicsDependencies
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        inMemoryUrlOpener = testDependencies.dependencies.urlOpener as InMemoryUrlOpener
        initialState = store.getState()
    })

    it('Initialize at the starting page', () => {
        expectState({
            ...initialState,
            ui: {
                ...initialState.ui,
                navigation    : {},
                userNavigation: {
                    currentPage              : AppPageName.LOGIN,
                    missionListing           : {
                        id                  : '',
                        isMissionActionsOpen: false
                    },
                    missionDetails           : {
                        id                  : '',
                        isMissionContactOpen: false
                    },
                    isInformationTemplateOpen: false
                }
            }
        })
    })

    describe('In the disconnected area', () => {
        beforeEach(() => {
            expectState({
                ...initialState,
                ui: {
                    ...initialState.ui,
                    navigation: {}
                }
            })
        })

        it('Enter to the connected area', done => {
            expectFinalNavigationState({
                ...initialState.ui.userNavigation,
                currentPage: AppPageName.MISSIONS
            }, () => {
                expectCurrentPage(AppPageName.MISSIONS)
            }, 1, done)

            navigate(navigationActions.Actions.navigateToMainPage())
        })

        it('Enter to the connected area', done => {
            expectFinalNavigationState({
                ...initialState.ui.userNavigation
            }, () => {
                expectCurrentPage(AppPageName.LOGIN)
            }, 2, done)

            navigate(navigationActions.Actions.navigateToMainPage())
            navigate(navigationActions.Actions.startApplication())
        })
    })

    describe('In the connected area', () => {
        it('Navigate to missions', done => {
            expectFinalNavigationState({
                ...initialState.ui.userNavigation,
                currentPage: AppPageName.MISSIONS
            }, () => {
                expectCurrentPage(AppPageName.MISSIONS)
                expect(isOnMissions(store.getState())).toBeTruthy()
            }, 1, done)

            navigate(navigationActions.Actions.navigateToMissions())
        })

        it('Open available actions on the mission', done => {
            navigate(navigationActions.Actions.navigateToMissions())

            expectFinalNavigationState({
                ...initialState.ui.userNavigation,
                currentPage   : AppPageName.MISSIONS,
                missionListing: {
                    id                  : 'id',
                    isMissionActionsOpen: true
                }
            }, () => {
                expect(isMissionActionsOpen(store.getState())('id')).toBeTruthy()
                expect(isMissionActionsOpen(store.getState())('id2')).toBeFalsy()
            }, 1, done)

            navigate(navigationActions.Actions.openMissionActions('id'))
        })

        it('Close available actions on the mission', done => {
            navigate(navigationActions.Actions.navigateToMissions())
            navigate(navigationActions.Actions.openMissionActions('id'))

            expectFinalNavigationState({
                ...initialState.ui.userNavigation,
                currentPage   : AppPageName.MISSIONS,
                missionListing: {
                    id                  : '',
                    isMissionActionsOpen: false
                }
            }, () => {
                expect(isMissionActionsOpen(store.getState())('id')).toBeFalsy()
                expect(isMissionActionsOpen(store.getState())('id2')).toBeFalsy()
            }, 1, done)

            navigate(navigationActions.Actions.quitMissionActions())
        })

        it('Open the details of a mission', done => {
            expectFinalNavigationState({
                ...initialState.ui.userNavigation,
                currentPage   : AppPageName.MISSION_DETAILS,
                missionDetails: {
                    id                  : 'missionId',
                    isMissionContactOpen: false
                }
            }, () => {
                expectCurrentPage(AppPageName.MISSION_DETAILS)
                expect(getMissionDetailsId(store.getState())).toEqual('missionId')
                expect(isOnMissions(store.getState())).toBeTruthy()
            }, 1, done)

            navigate(navigationActions.Actions.openMissionDetails('missionId'))
        })

        describe('In mission details', () => {
            beforeEach(() => {
                navigate(navigationActions.Actions.openMissionDetails('missionId'))
            })

            it('Close the details of a mission', done => {
                expectFinalNavigationState({
                    ...initialState.ui.userNavigation,
                    currentPage: AppPageName.MISSIONS
                }, () => {
                    expectCurrentPage(AppPageName.MISSIONS)
                    expect(isOnMissions(store.getState())).toBeTruthy()
                }, 1, done)

                navigate(navigationActions.Actions.quitMissionDetails())
            })

            it('Open mission contact', done => {
                expectFinalNavigationState({
                    ...initialState.ui.userNavigation,
                    currentPage   : AppPageName.MISSION_DETAILS,
                    missionDetails: {
                        id                  : 'missionId',
                        isMissionContactOpen: true
                    }
                }, () => {
                    expectCurrentPage(AppPageName.MISSION_DETAILS)
                    expect(isOnMissions(store.getState())).toBeTruthy()
                    expect(isMissionContactOpen(store.getState())).toBeTruthy()
                }, 1, done)

                navigate(navigationActions.Actions.openMissionContact())
            })

            it('Close mission contact', done => {
                expectFinalNavigationState({
                    ...initialState.ui.userNavigation,
                    currentPage   : AppPageName.MISSION_DETAILS,
                    missionDetails: {
                        id                  : 'missionId',
                        isMissionContactOpen: false
                    }
                }, () => {
                    expectCurrentPage(AppPageName.MISSION_DETAILS)
                    expect(isOnMissions(store.getState())).toBeTruthy()
                    expect(isMissionContactOpen(store.getState())).toBeFalsy()
                }, 2, done)

                navigate(navigationActions.Actions.openMissionContact())
                navigate(navigationActions.Actions.quitMissionContact())
            })
        })

        describe('Side menu', () => {
            beforeEach(() => {
                navigate(navigationActions.Actions.navigateToMissions())
            })

            it('Open it', done => {
                expectFinalNavigationState({
                    ...initialState.ui.userNavigation,
                    currentPage: AppPageName.MISSIONS
                }, () => {
                    expectCurrentPage(AppPageName.MISSIONS)
                }, 1, done)

                navigate(navigationActions.Actions.openSideMenu())
            })

            it('Close it', done => {
                expectFinalNavigationState({
                    ...initialState.ui.userNavigation,
                    currentPage: AppPageName.MISSIONS
                }, () => {
                    expectCurrentPage(AppPageName.MISSIONS)
                }, 2, done)

                navigate(navigationActions.Actions.openSideMenu())
                navigate(navigationActions.Actions.quitSideMenu())
            })

            it('Open mission creation', done => {
                expectFinalNavigationState({
                    ...initialState.ui.userNavigation,
                    currentPage: AppPageName.MISSION_CREATION
                }, null, 2, done)

                navigate(navigationActions.Actions.navigateToMissions())
                navigate(navigationActions.Actions.openMissionCreation())
            })

            describe('In mission creation', () => {
                beforeEach(() => {
                    navigate(navigationActions.Actions.navigateToMissions())
                    navigate(navigationActions.Actions.openMissionCreation())
                })

                it('Close mission creation', done => {
                    expectFinalNavigationState({
                        ...initialState.ui.userNavigation,
                        currentPage: AppPageName.MISSIONS
                    }, null, 1, done)

                    navigate(navigationActions.Actions.quitMissionCreation())
                })

                it('Open information template', done => {
                    expectFinalNavigationState({
                        ...initialState.ui.userNavigation,
                        currentPage              : AppPageName.MISSION_CREATION,
                        isInformationTemplateOpen: true
                    }, () => {
                        expectCurrentPage(AppPageName.MISSION_CREATION)
                        expect(isInformationTemplateOpen(store.getState())).toBeTruthy()
                    }, 1, done)

                    navigate(navigationActions.Actions.openInformationTemplate())
                })

                it('Close information template', done => {
                    expectFinalNavigationState({
                        ...initialState.ui.userNavigation,
                        currentPage: AppPageName.MISSION_CREATION
                    }, () => {
                        expectCurrentPage(AppPageName.MISSION_CREATION)
                        expect(isInformationTemplateOpen(store.getState())).toBeFalsy()
                    }, 2, done)

                    navigate(navigationActions.Actions.openInformationTemplate())
                    navigate(navigationActions.Actions.quitInformationTemplate())
                })
            })

            it('Open member information', done => {
                expectFinalNavigationState({
                    ...initialState.ui.userNavigation,
                    currentPage: AppPageName.MEMBER_PROFILE
                }, null, 2, done)

                navigate(navigationActions.Actions.navigateToMissions())
                navigate(navigationActions.Actions.openMemberProfile())
            })

            it('Close member information', done => {
                expectFinalNavigationState({
                    ...initialState.ui.userNavigation,
                    currentPage: AppPageName.MISSIONS
                }, null, 3, done)

                navigate(navigationActions.Actions.navigateToMissions())
                navigate(navigationActions.Actions.openMemberProfile())
                navigate(navigationActions.Actions.quitMemberProfile())
            })

            it('Log out', done => {
                expectFinalNavigationState({
                    ...initialState.ui.userNavigation,
                    currentPage: AppPageName.LOGIN
                }, () => {
                    expectCurrentPage(AppPageName.LOGIN)
                }, 1, done)

                navigate(navigationActions.Actions.quitApplication())
            })

            it('Contact us', done => {
                expectFinalNavigationState({
                    ...initialState.ui.userNavigation,
                    currentPage: AppPageName.MISSIONS
                }, null, 1, done)

                navigate(navigationActions.Actions.contactUs())
                inMemoryUrlOpener.triggerOpen()
                expect(inMemoryUrlOpener.openWith).toEqual('mailto:support@example.com')
            })
        })
    })

    function navigate(actions: navigationActions.Actions) {
        store.dispatch<navigationActions.Actions>(actions)
    }

    function expectFinalNavigationState(expectedNavigationState: UserNavigationState,
                                        selectors: null | (() => void),
                                        eventCount: number,
                                        done: () => void) {
        let event = 0
        store.subscribe(() => {
            if (++event === eventCount) {
                expectState({
                    ...initialState,
                    ui: {
                        ...initialState.ui,
                        navigation    : {},
                        userNavigation: expectedNavigationState
                    }
                })
                if (selectors)
                    selectors()
                done()
            }
        })
    }

    function expectCurrentPage(expectedPage: string) {
        expect(getCurrentPage(store.getState())).toEqual(expectedPage)
    }

    function expectState(expectedState: AppState) {
        expect(set(lensPath(['ui', 'navigation']), {})(store.getState())).toEqual(expectedState)
    }
})
