import { Store } from 'redux'
import { InMemoryAnalyticsLogger } from '../../../src/app/common/adapters/gateways/inmemory/inMemory.analyticsLogger'
import { AnalyticsOption } from '../../../src/app/common/domain/analyticslogger/AnalyticsLogger'
import { AppEventName } from '../../../src/app/common/domain/analyticslogger/appEventName.analytics'
import { AppScreenName } from '../../../src/app/common/domain/analyticslogger/appScreenName.analytics'
import { appAnalytics } from '../../../src/app/common/usecases/analytics/app.analytics'
import * as navigationActions from '../../../src/app/common/usecases/navigation/navigation.actions'
import * as memberModificationActions from '../../../src/app/member/usecases/membermodification/memberModification.actions'
import * as missionContactActions from '../../../src/app/missions/usecases/missioncontact/missionContact.actions'
import * as missionCreationActions from '../../../src/app/missions/usecases/missioncreation/missionCreation.actions'
import * as missionsListingActions from '../../../src/app/missions/usecases/missionslisting/missionsListing.actions'
import { appEpics } from '../../../src/configuration/redux/rootEpicMiddleware.redux'
import { AppActionsType } from '../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../src/configuration/redux/rootStore.redux'
import { StubMessageBuilder } from '../../missions/stubMessage.builder'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../testMiddlewareDependencies.redux'

describe('Application analytics', () => {
    let store: Store<AppState, AppActionsType>
    let inMemoryAnalyticsLogger: InMemoryAnalyticsLogger

    beforeEach(() => {
        const testDependencies = testAnalyticsDependencies
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testEpicsDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testDependencies }
            })
        inMemoryAnalyticsLogger = testDependencies.analyticsLogger as InMemoryAnalyticsLogger
    })

    describe('In the disconnected area', () => {
        it('Start the application', () => {
            navigate(navigationActions.Actions.startApplication())
            expectScreenLogged(AppScreenName.LOGIN)
        })

        it('Open sign in modal', () => {
            navigate(navigationActions.Actions.openSignIn())
            expectEventLogged(AppEventName.OPEN_SIGN_IN_MODAL)
        })

        it('Enter to the connected area', () => {
            navigate(navigationActions.Actions.navigateToMainPage())
            expectScreenLogged(AppScreenName.MAIN_PAGE)
            expectEventLogged(AppEventName.CONNECT_MEMBER)
        })
    })

    describe('In the connected area', () => {
        it('Navigate to missions', () => {
            navigate(navigationActions.Actions.navigateToMissions())
            expectScreenLogged(AppScreenName.MISSIONS)
        })

        it('Remove a mission', () => {
            store.dispatch(missionsListingActions.Actions.removeMission('missionId'))
            expectEventLogged(AppEventName.REMOVE_MISSION, { mission_id: 'missionId' })
        })

        it('Open the details of a mission', () => {
            navigate(navigationActions.Actions.openMissionDetails('missionId'))
            expectScreenLogged(AppScreenName.MISSION_DETAILS)
            expectEventLogged(AppEventName.OPEN_MISSION_DETAILS, { mission_id: 'missionId' })
        })

        describe('In mission details', () => {
            it('Close the details of a mission', () => {
                navigate(navigationActions.Actions.quitMissionDetails())
                expectEventLogged(AppEventName.CLOSE_MISSION_DETAILS)
            })

            it('Open mission contact', () => {
                navigate(navigationActions.Actions.openMissionContact())
                expectScreenLogged(AppScreenName.MISSION_DETAILS_CONTACT)
                expectEventLogged(AppEventName.OPEN_MISSION_DETAILS_CONTACT)
            })

            it('Send a mission contact', () => {
                const message = new StubMessageBuilder().build()
                store.dispatch(missionContactActions.Actions.sendMissionContact(message))
                expectEventLogged(AppEventName.SEND_MISSION_CONTACT, { mission_id: message.missionId })
            })

            it('Close mission contact', () => {
                navigate(navigationActions.Actions.quitMissionContact())
                expectEventLogged(AppEventName.CLOSE_MISSION_DETAILS_CONTACT)
            })
        })

        describe('Side menu', () => {
            it('Open it', () => {
                navigate(navigationActions.Actions.openSideMenu())
                expectEventLogged(AppEventName.OPEN_SIDE_MENU)
            })

            it('Close it', () => {
                navigate(navigationActions.Actions.quitSideMenu())
                expectEventLogged(AppEventName.CLOSE_SIDE_MENU)
            })

            it('Open mission creation', () => {
                navigate(navigationActions.Actions.openMissionCreation())
                expectScreenLogged(AppScreenName.MISSION_CREATION)
                expectEventLogged(AppEventName.OPEN_MISSION_CREATION)
            })

            describe('In mission creation', () => {
                it('Close mission creation', () => {
                    navigate(navigationActions.Actions.quitMissionCreation())
                    expectEventLogged(AppEventName.CLOSE_MISSION_CREATION)
                })

                it('Publish mission', () => {
                    store.dispatch(missionCreationActions.Actions.publishMission())
                    expectEventLogged(AppEventName.PUBLISH_MISSION)
                })

                it('Open information template', () => {
                    navigate(navigationActions.Actions.openInformationTemplate())
                    expectEventLogged(AppEventName.OPEN_INFORMATION_TEMPLATE)
                })

                it('Close information template', () => {
                    navigate(navigationActions.Actions.quitInformationTemplate())
                    expectEventLogged(AppEventName.CLOSE_INFORMATION_TEMPLATE)
                })
            })

            it('Open member information', () => {
                navigate(navigationActions.Actions.openMemberProfile())
                expectScreenLogged(AppScreenName.MEMBER_PROFILE)
                expectEventLogged(AppEventName.OPEN_MEMBER_PROFILE)
            })

            it('Update member information', () => {
                store.dispatch(memberModificationActions.Actions.updateMember())
                expectEventLogged(AppEventName.UPDATE_MEMBER_INFORMATION)
            })

            it('Close member information', () => {
                navigate(navigationActions.Actions.quitMemberProfile())
                expectEventLogged(AppEventName.CLOSE_MEMBER_PROFILE)
            })

            it('Logout', () => {
                navigate(navigationActions.Actions.quitApplication())
                expectEventLogged(AppEventName.LOG_OUT)
            })

            it('Contact us', () => {
                navigate(navigationActions.Actions.contactUs())
                expectEventLogged(AppEventName.CONTACT_US)
            })
        })
    })

    function navigate(actions: navigationActions.Actions) {
        store.dispatch<navigationActions.Actions>(actions)
    }

    function expectScreenLogged(expectedName: AppScreenName) {
        expect(inMemoryAnalyticsLogger.screenLoggedWith).toEqual(expectedName)
    }

    function expectEventLogged(expectedName: AppEventName, options?: AnalyticsOption) {
        expect(inMemoryAnalyticsLogger.eventLoggedWith).toEqual(expectedName)
        if (options)
            expect(inMemoryAnalyticsLogger.eventOptionsLoggedWith).toEqual(options)
    }
})
