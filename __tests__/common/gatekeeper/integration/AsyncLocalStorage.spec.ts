import spyOn = jest.spyOn
import lolex from 'lolex'
import { AsyncStorage } from 'react-native'
import { AsyncLocalStorage } from '../../../../src/app/common/adapters/gateways/real/Async.localStorage'
import { StoredTokenDTO } from '../../../../src/app/common/adapters/gateways/real/DTO/StoredTokenDTO'
import { Token } from '../../../../src/app/common/domain/entities/token'
import { LocalStorage } from '../../../../src/app/common/domain/storage/LocalStorage'

describe('Integration | Async local storage', () => {
    let localStorage: LocalStorage
    let clock: lolex.Clock
    const now = '2018-10-19T20:50:10.000+02:00'
    const token: Token = new Token('access_token', 86400)
    const storedToken: StoredTokenDTO = {
        access_token: 'access_token',
        expires_at  : '2018-10-20T19:50:10.000+02:00'
    }

    beforeEach(() => {
        clock = lolex.install({ now: new Date(now), shouldAdvanceTime: true })
        localStorage = new AsyncLocalStorage()
    })

    afterEach(() => {
        clock.uninstall()
    })

    it('Save in the storage', done => {
        spyOn(AsyncStorage, 'setItem').mockReturnValue(Promise.resolve(undefined))

        localStorage.saveToken(token).subscribe(() => {
            expect(AsyncStorage.setItem).toHaveBeenCalledTimes(1)
            expect(AsyncStorage.setItem).toHaveBeenCalledWith(
                'access_token',
                JSON.stringify(storedToken)
            )
            done()
        })
    })

    describe('Retrieve from the storage', () => {
        it('An item is already in the storage', done => {
            spyOn(AsyncStorage, 'getItem')
                .mockReturnValue(Promise.resolve(JSON.stringify(storedToken)))

            localStorage.getToken().subscribe(t => {
                expect(t).toEqual(Token.create(storedToken.access_token, storedToken.expires_at))
                expect(AsyncStorage.getItem).toHaveBeenCalledTimes(1)
                expect(AsyncStorage.getItem).toHaveBeenCalledWith('access_token')
                done()
            })
        })

        it('An item is not in the storage', done => {
            spyOn(AsyncStorage, 'getItem')
                .mockReturnValue(Promise.resolve(null))

            localStorage.getToken().subscribe(t => {
                expect(t).toEqual(Token.create('', ''))
                expect(AsyncStorage.getItem).toHaveBeenCalledTimes(1)
                expect(AsyncStorage.getItem).toHaveBeenCalledWith('access_token')
                done()
            })
        })
    })

    it('Remove in the storage', done => {
        spyOn(AsyncStorage, 'removeItem')
            .mockReturnValue(Promise.resolve(undefined))

        localStorage.removeToken().subscribe(() => {
            expect(AsyncStorage.removeItem).toHaveBeenCalledTimes(1)
            expect(AsyncStorage.removeItem).toHaveBeenCalledWith('access_token')
            done()
        })
    })
})
