import lolex from 'lolex'
import { lensPath, set } from 'ramda'
import { Store } from 'redux'
import { InMemoryLocalStorage } from '../../../../src/app/common/adapters/gateways/inmemory/inMemory.localStorage'
import { Token } from '../../../../src/app/common/domain/entities/token'
import { appAnalytics } from '../../../../src/app/common/usecases/analytics/app.analytics'
import * as gatekeeperActions from '../../../../src/app/common/usecases/gatekeeper/gatekeeper.actions'
import * as navigationActions from '../../../../src/app/common/usecases/navigation/navigation.actions'
import { InMemoryMemberRepository } from '../../../../src/app/member/adapters/gateways/inmemory/inMemory.memberRepository'
import { Member } from '../../../../src/app/member/domain/entities/member'
import { appEpics } from '../../../../src/configuration/redux/rootEpicMiddleware.redux'
import { AppActionsType, canAutoConnect, isAuthenticated } from '../../../../src/configuration/redux/rootReducer.redux'
import { AppState, DomainState, UIState } from '../../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../../src/configuration/redux/rootStore.redux'
import { AppPageName } from '../../../../src/configuration/withNavigation/appPageName.navigation'
import { StubMemberBuilder } from '../../../member/stubMember.builder'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../../testMiddlewareDependencies.redux'

describe('Gatekeeper', () => {
    let store: Store<AppState, AppActionsType>
    let initialState: AppState
    let inMemoryMemberRepository: InMemoryMemberRepository
    let inMemoryLocalStorage: InMemoryLocalStorage
    let clock: lolex.Clock
    const now = '2018-10-19T20:50:10.000+02:00'
    const token: Token = new Token('access_token', 86400)

    beforeEach(() => {
        clock = lolex.install({ now: new Date(now), shouldAdvanceTime: true })
        const testDependencies = testEpicsDependencies
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        inMemoryMemberRepository = testDependencies.dependencies.memberRepository as InMemoryMemberRepository
        inMemoryLocalStorage = testDependencies.dependencies.localStorage as InMemoryLocalStorage
        initialState = store.getState()
    })

    afterEach(() => {
        clock.uninstall()
    })

    it('Login', done => {
        const member: Member = new StubMemberBuilder()
            .withId('10')
            .withFirstName('first name')
            .withLastName('last name')
            .withEmail('email')
            .withAvatar('avatar')
            .build()

        expectFinalMemberState({
            ...initialState.domain,
            gatekeeper       : {
                token: {
                    access_token: 'access_token',
                    expires_at  : '2018-10-20T19:50:10.000+02:00'
                }
            },
            memberInformation: {
                ...initialState.domain.memberInformation,
                byId     : {
                    [member.id]: {
                        id       : member.id,
                        firstName: member.firstName,
                        lastName : member.lastName,
                        email    : member.email,
                        avatar   : member.avatar
                    }
                },
                allIds   : member.id,
                isLoading: false
            }
        }, () => {
            expect(isAuthenticated(store.getState())).toBeTruthy()
        }, 5, done, AppPageName.MISSIONS)

        dispatchGatekeeperActions(gatekeeperActions.Actions.logIn(token))
        inMemoryLocalStorage.triggerSaveToken()
        expect(inMemoryLocalStorage.saveWith).toEqual(token)
        inMemoryMemberRepository.populateMemberInformation(member)
    })

    it('Login in guest', done => {
        const member: Member = new StubMemberBuilder()
            .withId('10')
            .withFirstName('first name')
            .withLastName('last name')
            .withEmail('email')
            .withAvatar('avatar')
            .build()

        expectFinalMemberState({
            ...initialState.domain,
            gatekeeper       : {
                token: {
                    access_token: 'guest',
                    expires_at  : '2018-10-19T19:50:10.000+02:00'
                }
            },
            memberInformation: {
                ...initialState.domain.memberInformation,
                byId     : {
                    [member.id]: {
                        id       : member.id,
                        firstName: member.firstName,
                        lastName : member.lastName,
                        email    : member.email,
                        avatar   : member.avatar
                    }
                },
                allIds   : member.id,
                isLoading: false
            }
        }, () => {
            expect(isAuthenticated(store.getState())).toBeFalsy()
        }, 5, done, AppPageName.MISSIONS)

        dispatchGatekeeperActions(gatekeeperActions.Actions.logInGuest())
        inMemoryLocalStorage.triggerSaveToken()
        expect(inMemoryLocalStorage.saveWith).toEqual(Token.GUEST)
        inMemoryMemberRepository.populateMemberInformation(member)
    })

    describe('Get user identifier', () => {
        it('No one is saved', done => {
            inMemoryLocalStorage.saveToken(null)

            expectFinalMemberState({
                ...initialState.domain
            }, null, 1, done)

            dispatchGatekeeperActions(gatekeeperActions.Actions.autoLogin())
        })

        it('Identifier is expired', done => {
            navigate(navigationActions.Actions.navigateToMainPage())
            inMemoryLocalStorage.saveToken(new Token('access_token', -86400))

            expectFinalMemberState({
                ...initialState.domain,
                gatekeeper: {
                    token: {
                        access_token: 'access_token',
                        expires_at  : '2018-10-18T19:50:10.000+02:00'
                    }
                }
            }, null, 3, done)

            dispatchGatekeeperActions(gatekeeperActions.Actions.autoLogin())
        })

        it('It is already saved', done => {
            const member: Member = new StubMemberBuilder()
                .withId('10')
                .withFirstName('first name')
                .withLastName('last name')
                .withEmail('email')
                .withAvatar('avatar')
                .build()
            inMemoryLocalStorage.saveToken(token)

            expectFinalMemberState({
                ...initialState.domain,
                gatekeeper       : {
                    token: {
                        access_token: 'access_token',
                        expires_at  : '2018-10-20T19:50:10.000+02:00'
                    }
                },
                memberInformation: {
                    ...initialState.domain.memberInformation,
                    byId     : {
                        [member.id]: {
                            id       : member.id,
                            firstName: member.firstName,
                            lastName : member.lastName,
                            email    : member.email,
                            avatar   : member.avatar
                        }
                    },
                    allIds   : member.id,
                    isLoading: false
                }
            }, () => {
                expect(canAutoConnect(store.getState())).toBeTruthy()
            }, 5, done, AppPageName.MISSIONS)

            dispatchGatekeeperActions(gatekeeperActions.Actions.autoLogin())
            inMemoryLocalStorage.triggerSaveToken()
            expect(inMemoryLocalStorage.saveWith).toEqual(token)
            inMemoryMemberRepository.populateMemberInformation(member)
        })
    })

    it('Logout', done => {
        navigate(navigationActions.Actions.navigateToMainPage())

        expectFinalMemberState({
            ...initialState.domain
        }, () => {
            expect(canAutoConnect(store.getState())).toBeFalsy()
        }, 2, done)

        dispatchGatekeeperActions(gatekeeperActions.Actions.logOut())
        inMemoryLocalStorage.triggerRemoveToken()
        expect(inMemoryLocalStorage.isRemoved).toBeTruthy()
    })

    it('Unsubscribe member', done => {
        navigate(navigationActions.Actions.navigateToMainPage())

        expectFinalMemberState({
            ...initialState.domain
        }, () => {
            expect(canAutoConnect(store.getState())).toBeFalsy()
        }, 2, done)

        dispatchGatekeeperActions(gatekeeperActions.Actions.unsubscribe())
        inMemoryMemberRepository.triggerUnsubscribeMember()
        expect(inMemoryMemberRepository.isUnsubscribe).toBeTruthy()
        inMemoryLocalStorage.triggerRemoveToken()
        expect(inMemoryLocalStorage.isRemoved).toBeTruthy()
    })

    function dispatchGatekeeperActions(action: gatekeeperActions.Actions) {
        store.dispatch<gatekeeperActions.Actions>(action)
    }

    function navigate(actions: navigationActions.Actions) {
        store.dispatch<navigationActions.Actions>(actions)
    }

    function expectFinalMemberState(expectedDomainState: DomainState,
                                    selectors: null | (() => void),
                                    eventCount: number,
                                    done: () => void,
                                    expectedCurrentPage: string = AppPageName.LOGIN) {
        let event = 0
        store.subscribe(() => {
            if (++event === eventCount) {
                expectState({
                    ui    : appStateWithoutNav(expectedCurrentPage),
                    domain: expectedDomainState
                })
                if (selectors)
                    selectors()
                done()
            }
        })
    }

    function expectState(expectedState: AppState) {
        expect(set(lensPath(['ui', 'navigation']), {})(store.getState())).toEqual(expectedState)
    }

    function appStateWithoutNav(expectedCurrentPage: string): UIState {
        return {
            ...initialState.ui,
            userNavigation: {
                ...initialState.ui.userNavigation,
                currentPage: expectedCurrentPage
            },
            navigation    : {}
        }
    }
})
