import { MissionHeaderVMBuilder } from '../../src/ui/missions/viewmodels/missionHeaderVM.builder'

export class StubMissionHeaderVMBuilder extends MissionHeaderVMBuilder {

    protected _id: string = '1'
    protected _name: string = 'name'
    protected _description: string = 'lorem ipsum'
    protected _averageDailyRate: number = 666

}
