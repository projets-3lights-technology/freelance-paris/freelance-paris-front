import { MissionContractType } from '../../src/app/missions/domain/entities/missionContractType'
import { MissionForCreationBuilder } from '../../src/app/missions/domain/entities/missionForCreation.builder'

export class StubMissionForCreationBuilder extends MissionForCreationBuilder {

    protected _id: string = '1'
    protected _name: string = 'name'
    protected _description: string = 'lorem ipsum'
    protected _averageDailyRate: number = 666
    protected _customer: string = 'customer'
    protected _location: string = 'location'
    protected _startDate: string = '2018-09-30'
    protected _durationInMonths: string = '1'
    protected _remoteIsPossible: boolean = false
    protected _authorId: string = 'authorId'
    protected _contractType: MissionContractType = MissionContractType.CDD

}
