import { MissionHeaderBuilder } from '../../src/app/missions/domain/entities/missionHeader.builder'

export class StubMissionHeaderBuilder extends MissionHeaderBuilder {

    protected _id: string = '1'
    protected _name: string = 'name'
    protected _description: string = 'Lorem Ipsum'
    protected _averageDailyRate: number = 666
    protected _authorId: string = 'authorId'

}
