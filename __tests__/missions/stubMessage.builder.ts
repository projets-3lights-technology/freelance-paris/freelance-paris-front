import { MessageBuilder } from '../../src/app/missions/domain/entities/message.builder'

export class StubMessageBuilder extends MessageBuilder {

    protected _missionId: string = 'missionId'
    protected _senderId: string = 'senderId'
    protected _object: string = 'object'
    protected _message: string = 'message'

}
