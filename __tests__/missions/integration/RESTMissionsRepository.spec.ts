import spyOn = jest.spyOn
import { of } from 'rxjs'
import { ajax } from 'rxjs/ajax'
import { InMemoryLocalStorage } from '../../../src/app/common/adapters/gateways/inmemory/inMemory.localStorage'
import { AuthorizedHttpClient } from '../../../src/app/common/adapters/gateways/real/AuthorizedHttpClient'
import { ObservableHttpClient } from '../../../src/app/common/adapters/gateways/real/observable.httpClient'
import { Token } from '../../../src/app/common/domain/entities/token'
import { MissionDetailsDTO } from '../../../src/app/missions/adapters/gateways/real/DTO/MissionDetailsDTO'
import { MissionHeaderDTO } from '../../../src/app/missions/adapters/gateways/real/DTO/MissionHeaderDTO'
import { RESTMissionsRepository } from '../../../src/app/missions/adapters/gateways/real/REST.missionsRepository'
import { MissionContractType } from '../../../src/app/missions/domain/entities/missionContractType'
import { MissionDetailsBuilder } from '../../../src/app/missions/domain/entities/missionDetails.builder'
import { MissionHeaderBuilder } from '../../../src/app/missions/domain/entities/missionHeader.builder'
import { MissionsRepository } from '../../../src/app/missions/domain/MissionsRepository'
import { InMemoryConfig } from '../../testMiddlewareDependencies.redux'
import { StubMissionForCreationBuilder } from '../stubMissionForCreation.builder'

describe('Integration | Mission repository', () => {
    let missionsRepository: MissionsRepository

    beforeEach(() => {
        const inMemoryLocalStorage = new InMemoryLocalStorage(false, new Token('access_token', 10))
        const httpClient: AuthorizedHttpClient = new ObservableHttpClient(inMemoryLocalStorage)
        missionsRepository = new RESTMissionsRepository(httpClient, new InMemoryConfig())
    })

    it('Retrieve all mission', done => {
        const fakeMission: MissionHeaderDTO = {
            id                : 'id',
            name              : 'name',
            description       : 'description',
            average_daily_rate: 123,
            author_id         : '10'
        }

        spyOn(ajax, 'getJSON').mockReturnValue(of([fakeMission]))

        missionsRepository.findAll().subscribe(mission => {
            expect(mission).toEqual([new MissionHeaderBuilder()
                .withId('id')
                .withName('name')
                .withDescription('description')
                .withAverageDailyRate(123)
                .withAuthorId('10')
                .build()])
            expect(ajax.getJSON).toHaveBeenCalledTimes(1)
            expect(ajax.getJSON).toHaveBeenCalledWith(
                'https://api_url.com/missions',
                {
                    'Content-Type' : 'application/json',
                    'Authorization': 'Bearer access_token'
                }
            )
            done()
        })
    })

    it('Get mission details', done => {
        const fakeMission: MissionDetailsDTO = {
            id                : 'id',
            name              : 'name',
            description       : 'description',
            average_daily_rate: 123,
            customer          : 'customer',
            location          : 'location',
            start_date        : '2018-12-26T19:32:30.000+01:00',
            duration_in_months: '2',
            remote_is_possible: true,
            contract_type     : 'FREELANCE',
            already_contacted : true
        }

        spyOn(ajax, 'getJSON').mockReturnValue(of(fakeMission))

        missionsRepository.getById('id').subscribe(mission => {
            expect(mission).toEqual(new MissionDetailsBuilder()
                .withId('id')
                .withName('name')
                .withDescription('description')
                .withAverageDailyRate(123)
                .withCustomer('customer')
                .withLocation('location')
                .withStartDate('2018-12-26T19:32:30.000+01:00')
                .withDurationInMonths('2')
                .withRemoteIsPossible(true)
                .withContractType(MissionContractType.FREELANCE)
                .withAlreadyContacted(true)
                .build())
            expect(ajax.getJSON).toHaveBeenCalledTimes(1)
            expect(ajax.getJSON).toHaveBeenCalledWith(
                'https://api_url.com/missions/id',
                {
                    'Content-Type' : 'application/json',
                    'Authorization': 'Bearer access_token'
                }
            )
            done()
        })
    })

    it('Publish a new mission', done => {
        const missionForCreation = new StubMissionForCreationBuilder()
            .withId('id')
            .withName('name')
            .withDescription('description')
            .withAverageDailyRate(123)
            .withCustomer('customer')
            .withLocation('location')
            .withStartDate('26/12/2018')
            .withDurationInMonths('2')
            .withRemoteIsPossible(true)
            .withAuthorId('10')
            .withContractType(MissionContractType.FREELANCE)
            .build()

        spyOn(ajax, 'post').mockReturnValue(of(undefined))

        missionsRepository.create(missionForCreation).subscribe(() => {
            expect(ajax.post).toHaveBeenCalledTimes(1)
            expect(ajax.post).toHaveBeenCalledWith(
                'https://api_url.com/missions',
                {
                    id                : 'id',
                    name              : 'name',
                    description       : 'description',
                    average_daily_rate: 123,
                    customer          : 'customer',
                    location          : 'location',
                    start_date        : '2018-12-26T00:00:00.000+01:00',
                    duration_in_months: '2',
                    remote_is_possible: true,
                    contract_type     : 'FREELANCE'
                },
                {
                    'Content-Type' : 'application/x-www-form-urlencoded',
                    'Authorization': 'Bearer access_token'
                }
            )
            done()
        })
    })

    it('Remove a mission', done => {
        spyOn(ajax, 'delete').mockReturnValue(of(undefined))

        missionsRepository.remove('id').subscribe(() => {
            expect(ajax.delete).toHaveBeenCalledTimes(1)
            expect(ajax.delete).toHaveBeenCalledWith(
                'https://api_url.com/missions/id',
                {
                    'Content-Type' : 'application/json',
                    'Authorization': 'Bearer access_token'
                }
            )
            done()
        })
    })
})
