import { Optional } from 'typescript-optional'
import { MissionContractType } from '../../../src/app/missions/domain/entities/missionContractType'
import { MissionDetailsById, MissionsHeadersById } from '../../../src/app/missions/domain/MissionsState.redux'
import { MissionDetailsPresenter } from '../../../src/ui/missions/presenters/missionDetails.presenter'
import { MissionsListingPresenter } from '../../../src/ui/missions/presenters/missionsListing.presenter'
import { MissionDetailsVM } from '../../../src/ui/missions/viewmodels/missionDetails.viewmodel'
import { MissionDetailsVMBuilder } from '../../../src/ui/missions/viewmodels/missionDetailsVM.builder'
import { MissionHeaderVM } from '../../../src/ui/missions/viewmodels/missionHeader.viewmodel'
import { MissionHeaderVMBuilder } from '../../../src/ui/missions/viewmodels/missionHeaderVM.builder'

describe('Integration | Missions presenters', () => {
    it('Presents some missions', () => {
        const someMissions: MissionsHeadersById[] = [
            {
                id              : '1',
                name            : 'name 1',
                description     : 'lorem ipsum',
                averageDailyRate: 111,
                authorId        : 'authorId'
            },
            {
                id              : '2',
                name            : 'name 2',
                description     : 'lorem ipsum',
                averageDailyRate: 222,
                authorId        : 'authorId2'
            }
        ]

        const presentedAllMissions: MissionHeaderVM[] = MissionsListingPresenter.present(someMissions)

        expect(presentedAllMissions).toEqual([
            new MissionHeaderVMBuilder()
                .withId('1')
                .withName('name 1')
                .withDescription('lorem ipsum')
                .withAverageDailyRate(111)
                .withAuthorId('authorId')
                .build(),
            new MissionHeaderVMBuilder()
                .withId('2')
                .withName('name 2')
                .withDescription('lorem ipsum')
                .withAverageDailyRate(222)
                .withAuthorId('authorId2')
                .build()
        ])
        expect(presentedAllMissions[0].isPublishedBy('authorId')).toBeTruthy()
        expect(presentedAllMissions[1].isPublishedBy('authorId')).toBeFalsy()
    })

    describe('Presents mission details', () => {
        const missionDetails: MissionDetailsById = {
            id              : '1',
            name            : 'name 1',
            description     : 'lorem ipsum',
            averageDailyRate: 666,
            customer        : 'customer',
            location        : 'location',
            startDate       : '2018-09-29',
            durationInMonths: '2',
            remoteIsPossible: true,
            contractType    : MissionContractType.FREELANCE,
            alreadyContacted: true
        }

        it('Standard information', () => {
            const presentedMissionDetails: Optional<MissionDetailsVM> = MissionDetailsPresenter.present(Optional.ofNullable(missionDetails))

            presentedMissionDetails.ifPresent(mission => {
                expect(mission).toEqual(new MissionDetailsVMBuilder()
                    .withId('1')
                    .withName('name 1')
                    .withDescription('lorem ipsum')
                    .withAverageDailyRate(666)
                    .withCustomer('customer')
                    .withLocation('location')
                    .withStartDate('2018-09-29')
                    .withDurationInMonths('2')
                    .withRemoteIsPossible(true)
                    .withContractType('freelance')
                    .withAlreadyContacted(true)
                    .build()
                )
                expect(mission.startDate).toEqual('29/09/2018')
            })
        })

        it('Duration is very short', () => {
            const veryShort: MissionDetailsById = {
                ...missionDetails,
                durationInMonths: '-1'
            }
            const presentedMissionDetails: Optional<MissionDetailsVM> = MissionDetailsPresenter.present(Optional.ofNullable(veryShort))

            presentedMissionDetails.ifPresent(mission => {
                expect(mission.durationInMonths).toEqual('< 1')
            })
        })

        it('Duration is very long', () => {
            const veryShort: MissionDetailsById = {
                ...missionDetails,
                durationInMonths: '12+'
            }
            const presentedMissionDetails: Optional<MissionDetailsVM> = MissionDetailsPresenter.present(Optional.ofNullable(veryShort))

            presentedMissionDetails.ifPresent(mission => {
                expect(mission.durationInMonths).toEqual('> 12')
            })
        })
    })
})
