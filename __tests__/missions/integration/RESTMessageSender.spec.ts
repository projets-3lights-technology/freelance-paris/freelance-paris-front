import spyOn = jest.spyOn
import { of } from 'rxjs'
import { ajax } from 'rxjs/ajax'
import { InMemoryLocalStorage } from '../../../src/app/common/adapters/gateways/inmemory/inMemory.localStorage'
import { AuthorizedHttpClient } from '../../../src/app/common/adapters/gateways/real/AuthorizedHttpClient'
import { ObservableHttpClient } from '../../../src/app/common/adapters/gateways/real/observable.httpClient'
import { Token } from '../../../src/app/common/domain/entities/token'
import { RESTMessageSender } from '../../../src/app/missions/adapters/gateways/real/REST.messageSender'
import { MessageSender } from '../../../src/app/missions/domain/MessageSender'
import { InMemoryConfig } from '../../testMiddlewareDependencies.redux'
import { StubMessageBuilder } from '../stubMessage.builder'

describe('Integration | Message sender', () => {
    let messageSender: MessageSender

    beforeEach(() => {
        const inMemoryLocalStorage = new InMemoryLocalStorage(false, new Token('access_token', 10))
        const httpClient: AuthorizedHttpClient = new ObservableHttpClient(inMemoryLocalStorage)
        messageSender = new RESTMessageSender(httpClient, new InMemoryConfig())
    })

    it('Send a new message', done => {
        const message = new StubMessageBuilder()
            .withMissionId('missionId')
            .withSenderId('senderId')
            .withObject('object')
            .withMessage('message')
            .build()

        spyOn(ajax, 'post').mockReturnValue(of(undefined))

        messageSender.send(message).subscribe(() => {
            expect(ajax.post).toHaveBeenCalledTimes(1)
            expect(ajax.post).toHaveBeenCalledWith(
                'https://api_url.com/missions/missionId/contact',
                {
                    subject: 'object',
                    message: 'message'
                },
                {
                    'Content-Type' : 'application/x-www-form-urlencoded',
                    'Authorization': 'Bearer access_token'
                }
            )
            done()
        })
    })
})
