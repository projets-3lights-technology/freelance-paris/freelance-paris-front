import { MissionContractType } from '../../src/app/missions/domain/entities/missionContractType'
import { MissionDetailsBuilder } from '../../src/app/missions/domain/entities/missionDetails.builder'

export class StubMissionDetailsBuilder extends MissionDetailsBuilder {

    protected _id: string = '1'
    protected _name: string = 'name'
    protected _description: string = 'lorem ipsum'
    protected _averageDailyRate: number = 666
    protected _customer: string = 'customer'
    protected _location: string = 'location'
    protected _startDate: string = '2018-09-29'
    protected _durationInMonths: string = '2'
    protected _remoteIsPossible: boolean = false
    protected _contractType: MissionContractType = MissionContractType.CDD
    protected _alreadyContacted: boolean = false

}
