import { Store } from 'redux'
import { appAnalytics } from '../../../src/app/common/usecases/analytics/app.analytics'
import { InMemoryMessageSender } from '../../../src/app/missions/adapters/gateways/inmemory/InMemory.messageSender'
import { Message } from '../../../src/app/missions/domain/entities/message'
import { MissionContactState } from '../../../src/app/missions/domain/MissionsState.redux'
import * as missionContactActions from '../../../src/app/missions/usecases/missioncontact/missionContact.actions'
import { appEpics } from '../../../src/configuration/redux/rootEpicMiddleware.redux'
import {
    AppActionsType,
    getMissionContactIsLoading,
    getMissionContactIsSent
} from '../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../src/configuration/redux/rootStore.redux'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../testMiddlewareDependencies.redux'
import { StubMessageBuilder } from '../stubMessage.builder'

describe('Mission contact', () => {
    let store: Store<AppState, AppActionsType>
    let initialState: AppState
    let inMemoryMessageSender: InMemoryMessageSender
    let message: Message

    beforeEach(() => {
        const testDependencies = testEpicsDependencies
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        inMemoryMessageSender = testDependencies.dependencies.messageSender as InMemoryMessageSender
        initialState = store.getState()

        message = new StubMessageBuilder().build()
    })

    describe('While contacting', () => {
        it('Inform that the mission contact is on sending', done => {
            expect(getMissionContactIsLoading(store.getState())).toBeFalsy()

            expectFinalMissionContactState({
                ...initialState.domain.missionContact,
                isLoading: true,
                isSent   : false
            }, () => {
                expect(getMissionContactIsSent(store.getState())).toBeFalsy()
            }, 1, done)

            dispatchMissionContactActions(missionContactActions.Actions.sendMissionContact(message))
        })
    })

    describe('After contacting', () => {
        it('Send a message', done => {
            expectFinalMissionContactState({
                ...initialState.domain.missionContact,
                isLoading: false,
                isSent   : true
            }, () => {
                expect(getMissionContactIsSent(store.getState())).toBeTruthy()
            }, 2, done)

            dispatchMissionContactActions(missionContactActions.Actions.sendMissionContact(message))
            inMemoryMessageSender.triggerContact()
            expect(inMemoryMessageSender.sendWith).toEqual(message)
        })

        it('Notify if there is an error during the contacting', done => {
            expectFinalMissionContactState({
                ...initialState.domain.missionContact,
                error : 'error'
            }, null, 3, done)

            dispatchMissionContactActions(missionContactActions.Actions.sendMissionContact(message))
            inMemoryMessageSender.triggerContact()
            inMemoryMessageSender.contactOnError('error')
        })
    })

    function dispatchMissionContactActions(actions: missionContactActions.Actions) {
        store.dispatch<missionContactActions.Actions>(actions)
    }

    function expectFinalMissionContactState(expectedMissionContactState: MissionContactState,
                                            selectors: null | (() => void),
                                            eventCount: number,
                                            done: () => void) {
        let event = 0
        store.subscribe(() => {
            if (++event === eventCount) {
                expectState({
                    ...initialState,
                    domain: {
                        ...initialState.domain,
                        missionContact: expectedMissionContactState
                    }
                })
                if (selectors)
                    selectors()
                done()
            }
        })
    }

    function expectState(expectedState: AppState) {
        expect(store.getState()).toEqual(expectedState)
    }
})
