import {
    ContractTypeTo,
    MissionContractType
} from '../../../src/app/missions/domain/entities/missionContractType'

describe('Mission contract type convertor', () => {
    it('Convert to string', () => {
        expect(ContractTypeTo.string(MissionContractType.APPRENTICESHIP)).toEqual('apprenticeship')
        expect(ContractTypeTo.string(MissionContractType.CDD)).toEqual('cdd')
        expect(ContractTypeTo.string(MissionContractType.CDI)).toEqual('cdi')
        expect(ContractTypeTo.string(MissionContractType.FREELANCE)).toEqual('freelance')
        expect(ContractTypeTo.string(MissionContractType.INTERIM)).toEqual('interim')
        expect(ContractTypeTo.string(MissionContractType.PORTAGE)).toEqual('portage')
        expect(ContractTypeTo.string(MissionContractType.OTHER)).toEqual('other')
    })

    it('Convert to enum', () => {
        expect(ContractTypeTo.enum('apprenticeship')).toEqual(MissionContractType.APPRENTICESHIP)
        expect(ContractTypeTo.enum('cdd')).toEqual(MissionContractType.CDD)
        expect(ContractTypeTo.enum('cdi')).toEqual(MissionContractType.CDI)
        expect(ContractTypeTo.enum('freelance')).toEqual(MissionContractType.FREELANCE)
        expect(ContractTypeTo.enum('interim')).toEqual(MissionContractType.INTERIM)
        expect(ContractTypeTo.enum('portage')).toEqual(MissionContractType.PORTAGE)
        expect(ContractTypeTo.enum('other')).toEqual(MissionContractType.OTHER)
    })
})
