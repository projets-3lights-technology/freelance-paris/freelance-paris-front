import { Store } from 'redux'
import { appAnalytics } from '../../../src/app/common/usecases/analytics/app.analytics'
import { InMemoryMemberRepository } from '../../../src/app/member/adapters/gateways/inmemory/inMemory.memberRepository'
import { Member } from '../../../src/app/member/domain/entities/member'
import * as memberInformationActions from '../../../src/app/member/usecases/memberinformation/memberInformation.actions'
import { InMemoryMissionsRepository } from '../../../src/app/missions/adapters/gateways/inmemory/inMemory.missionsRepository'
import { MissionContractType } from '../../../src/app/missions/domain/entities/missionContractType'
import { MissionForCreation } from '../../../src/app/missions/domain/entities/missionForCreation'
import { MissionForCreationBuilder } from '../../../src/app/missions/domain/entities/missionForCreation.builder'
import { MissionHeader } from '../../../src/app/missions/domain/entities/missionHeader'
import { MissionCreationState } from '../../../src/app/missions/domain/MissionsState.redux'
import * as missionCreationActions from '../../../src/app/missions/usecases/missioncreation/missionCreation.actions'
import { appEpics } from '../../../src/configuration/redux/rootEpicMiddleware.redux'
import {
    AppActionsType,
    getMissionCreationHasError,
    getMissionCreationIsLoading,
    getMissionUnderCreation,
    getMissionUnderCreationError
} from '../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../src/configuration/redux/rootStore.redux'
import { StubMemberBuilder } from '../../member/stubMember.builder'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../testMiddlewareDependencies.redux'
import { StubMissionHeaderBuilder } from '../stubMissionHeader.builder'

describe('Mission creation', () => {
    let store: Store<AppState, AppActionsType>
    let initialState: AppState
    let inMemoryMissionsRepository: InMemoryMissionsRepository
    let inMemoryMemberRepository: InMemoryMemberRepository

    beforeEach(() => {
        const testDependencies = testEpicsDependencies
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        inMemoryMissionsRepository = testDependencies.dependencies.missionsRepository as InMemoryMissionsRepository
        inMemoryMemberRepository = testDependencies.dependencies.memberRepository as InMemoryMemberRepository
        initialState = store.getState()
    })

    describe('Mission creation', () => {
        it('Change all information about mission', done => {
            expectFinalMissionState({
                ...initialState.domain.missionCreation,
                missionUnderCreation     : {
                    name            : 'name',
                    averageDailyRate: '666',
                    customer        : 'customer',
                    location        : 'location',
                    startDate       : '19/10/2018',
                    durationInMonths: '-1',
                    remoteIsPossible: true,
                    description     : 'lorem ipsum',
                    contractType    : 'other'
                },
                missionUnderCreationError: {
                    name            : '',
                    averageDailyRate: '',
                    location        : '',
                    startDate       : '',
                    durationInMonths: '',
                    description     : ''
                }
            }, () => {
                expect(getMissionCreationHasError(store.getState())).toBeFalsy()
                expect(getMissionUnderCreation(store.getState())).toEqual({
                    name            : 'name',
                    averageDailyRate: '666',
                    customer        : 'customer',
                    location        : 'location',
                    startDate       : '19/10/2018',
                    durationInMonths: '-1',
                    remoteIsPossible: true,
                    description     : 'lorem ipsum',
                    contractType    : 'other'
                })
            }, 9, done)

            fillAllMissionInformation()
        })

        describe('Validation', () => {
            it('All required fields do not be fill', done => {
                expectFinalMissionState({
                    ...initialState.domain.missionCreation,
                    missionUnderCreationError: {
                        name            : 'required',
                        averageDailyRate: 'required',
                        location        : 'required',
                        startDate       : 'required',
                        durationInMonths: 'required',
                        description     : 'required'
                    }
                }, () => {
                    expect(getMissionCreationHasError(store.getState())).toBeTruthy()
                    expect(getMissionUnderCreationError(store.getState())).toEqual({
                        name            : 'required',
                        averageDailyRate: 'required',
                        location        : 'required',
                        startDate       : 'required',
                        durationInMonths: 'required',
                        description     : 'required'
                    })
                }, 2, done)

                dispatchMissionCreationActions(missionCreationActions.Actions.publishMission())
                inMemoryMissionsRepository.triggerMissionCreation()
                expect(inMemoryMissionsRepository.createWith).toBeNull()
            })

            it('Avoid empty characters for description', done => {
                expectFinalMissionState({
                    ...initialState.domain.missionCreation,
                    missionUnderCreation     : {
                        name            : 'name',
                        averageDailyRate: '666',
                        customer        : 'customer',
                        location        : 'location',
                        startDate       : '19/10/2018',
                        durationInMonths: '-1',
                        remoteIsPossible: true,
                        description     : ' ',
                        contractType    : 'other'
                    },
                    missionUnderCreationError: {
                        name            : '',
                        averageDailyRate: '',
                        location        : '',
                        startDate       : '',
                        durationInMonths: '',
                        description     : 'required'
                    }
                }, () => {
                    expect(getMissionCreationHasError(store.getState())).toBeTruthy()
                    expect(getMissionUnderCreationError(store.getState())).toEqual({
                        name            : '',
                        averageDailyRate: '',
                        location        : '',
                        startDate       : '',
                        durationInMonths: '',
                        description     : 'required'
                    })
                }, 12, done)

                fillAllMissionInformation()
                dispatchMissionCreationActions(missionCreationActions.Actions.changeMissionInformation('description', ' '))
                dispatchMissionCreationActions(missionCreationActions.Actions.publishMission())
                inMemoryMissionsRepository.triggerMissionCreation()
                expect(inMemoryMissionsRepository.createWith).toBeNull()
            })
        })

        it('Reset all information about the mission', done => {
            fillAllMissionInformation()
            expect(getMissionUnderCreation(store.getState())).toEqual({
                name            : 'name',
                averageDailyRate: '666',
                customer        : 'customer',
                location        : 'location',
                startDate       : '19/10/2018',
                durationInMonths: '-1',
                remoteIsPossible: true,
                description     : 'lorem ipsum',
                contractType    : 'other'
            })

            expectFinalMissionState({
                ...initialState.domain.missionCreation
            }, null, 1, done)

            dispatchMissionCreationActions(missionCreationActions.Actions.resetMissionCreation())
        })
    })

    describe('While creating', () => {
        it('Inform that the mission creation is on loading', done => {
            expect(getMissionCreationIsLoading(store.getState())).toBeFalsy()

            expectFinalMissionState({
                ...initialState.domain.missionCreation,
                isLoading: true
            }, null, 1, done)

            dispatchMissionCreationActions(missionCreationActions.Actions.publishMission())
        })
    })

    describe('After creating mission', () => {
        beforeEach(() => {
            fillAllMissionInformation()
        })

        it('Create a new mission', done => {
            const member: Member = new StubMemberBuilder()
                .withId('10')
                .withFirstName('first name')
                .withLastName('last name')
                .withEmail('email')
                .withAvatar('avatar')
                .build()
            fetchMember(member)

            const missionForCreation = new MissionForCreationBuilder()
                .withName('name')
                .withAverageDailyRate(666)
                .withCustomer('customer')
                .withLocation('location')
                .withStartDate('19/10/2018')
                .withDurationInMonths('-1')
                .withRemoteIsPossible(true)
                .withDescription('lorem ipsum')
                .withAuthorId('10')
                .withContractType(MissionContractType.OTHER)
                .build()
            const mission: MissionHeader = new StubMissionHeaderBuilder()
                .withId('1')
                .withName('name')
                .withDescription('lorem ipsum')
                .withAuthorId('10')
                .build()

            expectFinalMissionState({
                ...initialState.domain.missionCreation
            }, null, 4, done, {
                missions         : {
                    ...initialState.domain.missions,
                    byId  : {
                        1: {
                            id              : '1',
                            name            : 'name',
                            description     : 'lorem ipsum',
                            averageDailyRate: 666,
                            authorId        : '10'
                        }
                    },
                    allIds: ['1']
                },
                memberInformation: {
                    ...initialState.domain.memberInformation,
                    allIds: '10',
                    byId  : {
                        10: {
                            avatar   : 'avatar',
                            email    : 'email',
                            firstName: 'first name',
                            id       : '10',
                            lastName : 'last name'
                        }
                    }
                }
            })

            dispatchMissionCreationActions(missionCreationActions.Actions.publishMission())
            inMemoryMissionsRepository.populateAllMissions([mission])
            inMemoryMissionsRepository.triggerMissionCreation()
            expectMissionForCreation(missionForCreation)
        })

        it('Notify if there is an error during the fetching', done => {
            expectFinalMissionState({
                ...initialState.domain.missionCreation,
                missionUnderCreation     : {
                    name            : 'name',
                    averageDailyRate: '666',
                    customer        : 'customer',
                    location        : 'location',
                    startDate       : '19/10/2018',
                    durationInMonths: '-1',
                    remoteIsPossible: true,
                    description     : 'lorem ipsum',
                    contractType    : 'other'
                },
                missionUnderCreationError: {
                    name            : '',
                    averageDailyRate: '',
                    location        : '',
                    startDate       : '',
                    durationInMonths: '',
                    description     : ''
                },
                error                    : 'error'
            }, null, 2, done)

            dispatchMissionCreationActions(missionCreationActions.Actions.publishMission())
            inMemoryMissionsRepository.createMissionOnError('error')
        })

        function fetchMember(member: Member) {
            store.dispatch(memberInformationActions.Actions.fetchMemberInformation())
            inMemoryMemberRepository.populateMemberInformation(member)
        }
    })

    function dispatchMissionCreationActions(actions: missionCreationActions.Actions) {
        store.dispatch<missionCreationActions.Actions>(actions)
    }

    function expectFinalMissionState(expectedMissionsState: MissionCreationState,
                                     selectors: null | (() => void),
                                     eventCount: number,
                                     done: () => void,
                                     extendState: any = {}) {
        let event = 0
        store.subscribe(() => {
            if (++event === eventCount) {
                expectState({
                    ...initialState,
                    domain: {
                        ...initialState.domain,
                        ...extendState,
                        missionCreation: expectedMissionsState
                    }
                })
                if (selectors)
                    selectors()
                done()
            }
        })
    }

    function expectState(expectedState: AppState) {
        expect(store.getState()).toEqual(expectedState)
    }

    function fillAllMissionInformation() {
        dispatchMissionCreationActions(missionCreationActions.Actions.changeMissionInformation('name', 'name'))
        dispatchMissionCreationActions(missionCreationActions.Actions.changeMissionInformation('averageDailyRate', '666'))
        dispatchMissionCreationActions(missionCreationActions.Actions.changeMissionInformation('customer', 'customer'))
        dispatchMissionCreationActions(missionCreationActions.Actions.changeMissionInformation('location', 'location'))
        dispatchMissionCreationActions(missionCreationActions.Actions.changeMissionStartDate('19/10/2018'))
        dispatchMissionCreationActions(missionCreationActions.Actions.changeMissionInformation('durationInMonths', '-1'))
        dispatchMissionCreationActions(missionCreationActions.Actions.changeMissionInformation('remoteIsPossible', true))
        dispatchMissionCreationActions(missionCreationActions.Actions.changeMissionInformation('description', 'lorem ipsum'))
        dispatchMissionCreationActions(missionCreationActions.Actions.changeMissionInformation('contractType', 'other'))
    }

    function expectMissionForCreation(expectedMissionForCreation: MissionForCreation) {
        expect(inMemoryMissionsRepository.createWith.name).toEqual(expectedMissionForCreation.name)
        expect(inMemoryMissionsRepository.createWith.averageDailyRate).toEqual(expectedMissionForCreation.averageDailyRate)
        expect(inMemoryMissionsRepository.createWith.customer).toEqual(expectedMissionForCreation.customer)
        expect(inMemoryMissionsRepository.createWith.location).toEqual(expectedMissionForCreation.location)
        expect(inMemoryMissionsRepository.createWith.startDate).toEqual(expectedMissionForCreation.startDate)
        expect(inMemoryMissionsRepository.createWith.durationInMonth).toEqual(expectedMissionForCreation.durationInMonth)
        expect(inMemoryMissionsRepository.createWith.remoteIsPossible).toEqual(expectedMissionForCreation.remoteIsPossible)
        expect(inMemoryMissionsRepository.createWith.description).toEqual(expectedMissionForCreation.description)
        expect(inMemoryMissionsRepository.createWith.authorId).toEqual(expectedMissionForCreation.authorId)
        expect(inMemoryMissionsRepository.createWith.contractType).toEqual(expectedMissionForCreation.contractType)
    }
})
