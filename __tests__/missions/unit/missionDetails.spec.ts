import { lensPath, set } from 'ramda'
import { Store } from 'redux'
import { appAnalytics } from '../../../src/app/common/usecases/analytics/app.analytics'
import * as navigationActions from '../../../src/app/common/usecases/navigation/navigation.actions'
import { InMemoryMissionsRepository } from '../../../src/app/missions/adapters/gateways/inmemory/inMemory.missionsRepository'
import { MissionContractType } from '../../../src/app/missions/domain/entities/missionContractType'
import { MissionDetails } from '../../../src/app/missions/domain/entities/missionDetails'
import { MissionDetailsState } from '../../../src/app/missions/domain/MissionsState.redux'
import * as missionDetailsActions from '../../../src/app/missions/usecases/missiondetails/missionDetails.actions'
import { appEpics } from '../../../src/configuration/redux/rootEpicMiddleware.redux'
import {
    AppActionsType,
    getMissionDetails,
    getMissionsDetailsIsLoading
} from '../../../src/configuration/redux/rootReducer.redux'
import { AppState, UIState } from '../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../src/configuration/redux/rootStore.redux'
import { AppPageName } from '../../../src/configuration/withNavigation/appPageName.navigation'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../testMiddlewareDependencies.redux'
import { StubMissionDetailsBuilder } from '../stubMissionDetails.builder'

describe('Mission details retriever', () => {
    let store: Store<AppState, AppActionsType>
    let initialState: AppState
    let inMemoryMissionsRepository: InMemoryMissionsRepository

    beforeEach(() => {
        const testDependencies = testEpicsDependencies
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        inMemoryMissionsRepository = testDependencies.dependencies.missionsRepository as InMemoryMissionsRepository
        initialState = store.getState()
    })

    describe('While fetching', () => {
        it('Inform that the retrieval of mission details is on loading', done => {
            expect(getMissionsDetailsIsLoading(store.getState())).toBeFalsy()

            expectFinalMissionsState({
                ...initialState.domain.missionDetails,
                isLoading: true
            }, null, 1, done)

            dispatchMissionDetailsRetrieverActions(missionDetailsActions.Actions.fetchMissionDetails('1'))
        })

        it('Abort missions fetching', done => {
            expectFinalMissionsState({
                ...initialState.domain.missionDetails
            }, null, 2, done)

            dispatchMissionDetailsRetrieverActions(missionDetailsActions.Actions.fetchMissionDetails('1'))
            dispatchMissionDetailsRetrieverActions(missionDetailsActions.Actions.abortMissionDetailsFetching())
        })
    })

    describe('After fetching details', () => {
        it('Fetch mission details', done => {
            const mission: MissionDetails = new StubMissionDetailsBuilder()
                .withId('1')
                .withName('name')
                .withDescription('lorem ipsum')
                .withContractType(MissionContractType.CDD)
                .withAlreadyContacted(true)
                .build()
            openMissionDetails('1')
            const extendedState = {
                ui: {
                    ...initialState.ui,
                    navigation    : {},
                    userNavigation: {
                        ...initialState.ui.userNavigation,
                        currentPage   : AppPageName.MISSION_DETAILS,
                        missionDetails: {
                            id                  : '1',
                            isMissionContactOpen: false
                        }
                    }
                }
            }

            expectFinalMissionsState({
                ...initialState.domain.missionDetails,
                byId     : {
                    [mission.id]: {
                        id              : mission.id,
                        name            : mission.name,
                        description     : mission.description,
                        averageDailyRate: mission.averageDailyRate,
                        customer        : mission.customer,
                        location        : mission.location,
                        startDate       : mission.startDate,
                        durationInMonths: mission.durationInMonths,
                        remoteIsPossible: mission.remoteIsPossible,
                        contractType    : mission.contractType,
                        alreadyContacted: true
                    }
                },
                isLoading: false
            }, () => {
                expect(getMissionsDetailsIsLoading(store.getState())).toBeFalsy()
                getMissionDetails(store.getState()).ifPresent(value =>
                    expect(value).toEqual({
                        id              : '1',
                        name            : 'name',
                        description     : 'lorem ipsum',
                        averageDailyRate: 666,
                        customer        : 'customer',
                        location        : 'location',
                        startDate       : '2018-09-29',
                        durationInMonths: '2',
                        remoteIsPossible: false,
                        contractType    : MissionContractType.CDD,
                        alreadyContacted: true
                    })
                )
            }, 2, done, extendedState)

            dispatchMissionDetailsRetrieverActions(missionDetailsActions.Actions.fetchMissionDetails('1'))
            inMemoryMissionsRepository.populateMissionDetails(mission)
        })

        it('Notify if there is an error during the fetching', done => {
            expectFinalMissionsState({
                ...initialState.domain.missionDetails,
                error: 'error'
            }, null, 2, done)
            dispatchMissionDetailsRetrieverActions(missionDetailsActions.Actions.fetchMissionDetails('1'))
            inMemoryMissionsRepository.missionDetailsOnError('error')
        })

        function openMissionDetails(id: string) {
            store.dispatch(navigationActions.Actions.openMissionDetails(id))
        }
    })

    function dispatchMissionDetailsRetrieverActions(action: missionDetailsActions.Actions) {
        store.dispatch<missionDetailsActions.Actions>(action)
    }

    function expectFinalMissionsState(expectedMissionsState: MissionDetailsState,
                                      selectors: null | (() => void),
                                      eventCount: number,
                                      done: () => void,
                                      extendState: any = {}) {
        let event = 0
        store.subscribe(() => {
            if (++event === eventCount) {
                expectState({
                    ui    : appStateWithoutNav(),
                    domain: {
                        ...initialState.domain,
                        missionDetails: expectedMissionsState
                    },
                    ...extendState
                })
                if (selectors)
                    selectors()
                done()
            }
        })
    }

    function expectState(expectedState: AppState) {
        expect(set(lensPath(['ui', 'navigation']), {})(store.getState())).toEqual(expectedState)
    }

    function appStateWithoutNav(): UIState {
        return {
            ...initialState.ui,
            navigation: {}
        }
    }
})
