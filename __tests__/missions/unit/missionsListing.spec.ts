import { Store } from 'redux'
import { appAnalytics } from '../../../src/app/common/usecases/analytics/app.analytics'
import { InMemoryMissionsRepository } from '../../../src/app/missions/adapters/gateways/inmemory/inMemory.missionsRepository'
import { MissionHeader } from '../../../src/app/missions/domain/entities/missionHeader'
import { MissionsListingState } from '../../../src/app/missions/domain/MissionsState.redux'
import * as missionsListingActions from '../../../src/app/missions/usecases/missionslisting/missionsListing.actions'
import { appEpics } from '../../../src/configuration/redux/rootEpicMiddleware.redux'
import {
    AppActionsType,
    getMissionsListing,
    getMissionsListingIsLoading,
    getMissionsListingIsRefreshing
} from '../../../src/configuration/redux/rootReducer.redux'
import { AppState } from '../../../src/configuration/redux/rootState.redux'
import { ReduxStore } from '../../../src/configuration/redux/rootStore.redux'
import { testAnalyticsDependencies, testEpicsDependencies } from '../../testMiddlewareDependencies.redux'
import { StubMissionHeaderBuilder } from '../stubMissionHeader.builder'

describe('Missions listing retriever', () => {
    let store: Store<AppState, AppActionsType>
    let initialState: AppState
    let inMemoryMissionsRepository: InMemoryMissionsRepository

    beforeEach(() => {
        const testDependencies = testEpicsDependencies
        store = new ReduxStore()
            .configure({
                reduxObservable: { epics: appEpics, dependencies: testDependencies },
                analyticsLogger: { analytics: appAnalytics, dependencies: testAnalyticsDependencies }
            })
        inMemoryMissionsRepository = testDependencies.dependencies.missionsRepository as InMemoryMissionsRepository
        initialState = store.getState()
    })

    describe('While fetching', () => {
        it('Inform that the retrieval of missions is on loading', done => {
            expect(getMissionsListingIsLoading(store.getState())).toBeFalsy()

            expectFinalMissionsState({
                ...initialState.domain.missions,
                isLoading: true
            }, null, 1, done)

            dispatchMissionsListingActions(missionsListingActions.Actions.fetchAllMissions())
        })

        it('Abort missions fetching', done => {
            expectFinalMissionsState({
                ...initialState.domain.missions
            }, null, 2, done)

            dispatchMissionsListingActions(missionsListingActions.Actions.fetchAllMissions())
            dispatchMissionsListingActions(missionsListingActions.Actions.abortAllMissionsFetching())
        })
    })

    describe('After fetching missions', () => {
        let mission: MissionHeader

        beforeEach(() => {
            mission = new StubMissionHeaderBuilder()
                .withId('1')
                .withName('Name')
                .withDescription('Lorem ipsum')
                .withAverageDailyRate(111)
                .build()
        })

        it('Fetch a list with zero mission', done => {
            expectFinalMissionsState({
                ...initialState.domain.missions,
                byId     : {},
                allIds   : [],
                isLoading: false
            }, null, 2, done)

            dispatchMissionsListingActions(missionsListingActions.Actions.fetchAllMissions())
            inMemoryMissionsRepository.populateAllMissions([])
        })

        it('Fetch a list with some missions', done => {
            const mission2: MissionHeader = new StubMissionHeaderBuilder()
                .withId('2')
                .withName('Name2')
                .withDescription('Lorem ipsum')
                .build()

            expectFinalMissionsState({
                ...initialState.domain.missions,
                byId     : {
                    [mission.id] : {
                        id              : mission.id,
                        name            : mission.name,
                        description     : mission.description,
                        averageDailyRate: mission.averageDailyRate,
                        authorId        : mission.authorId
                    },
                    [mission2.id]: {
                        id              : mission2.id,
                        name            : mission2.name,
                        description     : mission2.description,
                        averageDailyRate: mission2.averageDailyRate,
                        authorId        : mission2.authorId
                    }
                },
                allIds   : [mission.id, mission2.id],
                isLoading: false
            }, () => {
                expect(getMissionsListingIsLoading(store.getState())).toBeFalsy()
                expect(getMissionsListing(store.getState())).toEqual([{
                    id              : mission.id,
                    name            : mission.name,
                    description     : mission.description,
                    averageDailyRate: mission.averageDailyRate,
                    authorId        : mission.authorId
                }, {
                    id              : mission2.id,
                    name            : mission2.name,
                    description     : mission2.description,
                    averageDailyRate: mission2.averageDailyRate,
                    authorId        : mission2.authorId
                }])
            }, 3, done)

            dispatchMissionsListingActions(missionsListingActions.Actions.fetchAllMissions())
            inMemoryMissionsRepository.populateAllMissions([mission, mission2])
        })

        it('Remove a mission from the list', done => {
            const mission2: MissionHeader = new StubMissionHeaderBuilder()
                .withId('2')
                .withName('Name2')
                .withDescription('Lorem ipsum')
                .build()

            expectFinalMissionsState({
                ...initialState.domain.missions,
                byId     : {
                    [mission2.id]: {
                        id              : mission2.id,
                        name            : mission2.name,
                        description     : mission2.description,
                        averageDailyRate: mission2.averageDailyRate,
                        authorId        : mission2.authorId
                    }
                },
                allIds   : [mission2.id],
                isLoading: false
            }, () => {
                expect(getMissionsListingIsLoading(store.getState())).toBeFalsy()
                expect(getMissionsListing(store.getState())).toEqual([{
                    id              : mission2.id,
                    name            : mission2.name,
                    description     : mission2.description,
                    averageDailyRate: mission2.averageDailyRate,
                    authorId        : mission2.authorId
                }])
            }, 5, done)

            dispatchMissionsListingActions(missionsListingActions.Actions.fetchAllMissions())
            inMemoryMissionsRepository.populateAllMissions([mission, mission2])
            dispatchMissionsListingActions(missionsListingActions.Actions.removeMission(mission.id))
            inMemoryMissionsRepository.triggerMissionRemove()
        })
    })

    describe('While refreshing', () => {
        it('Inform that the refresh of missions is on progress', done => {
            expect(getMissionsListingIsRefreshing(store.getState())).toBeFalsy()

            expectFinalMissionsState({
                ...initialState.domain.missions,
                isRefreshing: true
            }, () => {
                expect(getMissionsListingIsRefreshing(store.getState())).toBeTruthy()
            }, 1, done)

            dispatchMissionsListingActions(missionsListingActions.Actions.refreshAllMissions())
        })

        it('Abort missions refreshing', done => {
            expectFinalMissionsState({
                ...initialState.domain.missions
            }, null, 3, done)

            dispatchMissionsListingActions(missionsListingActions.Actions.refreshAllMissions())
            dispatchMissionsListingActions(missionsListingActions.Actions.abortAllMissionsFetching())
        })
    })

    describe('After refreshing missions', () => {
        let mission: MissionHeader

        beforeEach(() => {
            mission = new StubMissionHeaderBuilder()
                .withId('1')
                .withName('Name')
                .withDescription('Lorem ipsum')
                .withAuthorId('authorId')
                .build()
        })

        it('Refresh the missions listing', done => {
            expectFinalMissionsState({
                ...initialState.domain.missions,
                byId        : {
                    [mission.id]: {
                        id              : mission.id,
                        name            : mission.name,
                        description     : mission.description,
                        averageDailyRate: mission.averageDailyRate,
                        authorId        : mission.authorId
                    }
                },
                allIds      : [mission.id],
                isRefreshing: false
            }, () => {
                expect(getMissionsListingIsRefreshing(store.getState())).toBeFalsy()
                expect(getMissionsListing(store.getState())).toEqual([{
                    id              : mission.id,
                    name            : mission.name,
                    description     : mission.description,
                    averageDailyRate: mission.averageDailyRate,
                    authorId        : mission.authorId
                }])
            }, 3, done)

            dispatchMissionsListingActions(missionsListingActions.Actions.refreshAllMissions())
            inMemoryMissionsRepository.populateAllMissions([mission])
        })
    })

    describe('Notify if there is an error', () => {
        it('During the fetching', done => {
            expectFinalMissionsState({
                ...initialState.domain.missions,
                error: 'error'
            }, null, 3, done)

            dispatchMissionsListingActions(missionsListingActions.Actions.fetchAllMissions())
            inMemoryMissionsRepository.allMissionsOnError('error')
        })

        it('During the refreshing', done => {
            expectFinalMissionsState({
                ...initialState.domain.missions,
                error: 'error'
            }, null, 2, done)

            dispatchMissionsListingActions(missionsListingActions.Actions.refreshAllMissions())
            inMemoryMissionsRepository.allMissionsOnError('error')
        })
    })

    function dispatchMissionsListingActions(actions: missionsListingActions.Actions) {
        store.dispatch<missionsListingActions.Actions>(actions)
    }

    function expectFinalMissionsState(expectedMissionsState: MissionsListingState,
                                      selectors: null | (() => void),
                                      eventCount: number,
                                      done: () => void) {
        let event = 0
        store.subscribe(() => {
            if (++event === eventCount) {
                expectState({
                    ...initialState,
                    domain: {
                        ...initialState.domain,
                        missions: expectedMissionsState
                    }
                })
                if (selectors)
                    selectors()
                done()
            }
        })
    }

    function expectState(expectedState: AppState) {
        expect(store.getState()).toEqual(expectedState)
    }
})
